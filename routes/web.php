<?php

Route::get('test',function () {
  dd(debug_backtrace());
});

Route::get('/','PageController@mainPage')->name('main');

Route::get('daily-draw','PageController@dailyDraw')->name('daily_draw');

// Route::get('moon', function () {
//     return view('moon/main');
// })->name('moon');
//
// Route::get('token', function () {
//     return view('token/main');
// })->name('token');

Route::get('draw-archive','PageController@drawArchive');

Route::get('help-center/{categoryId?}','PageController@helpCenter')->name('helpCenter');

Route::group(['middleware' => ['user-auth','2fa']], function () {
  Route::get('play-daily-draw','GameController@dailyDraw');
  Route::post('buyTicket','GameController@buyTicket');

  Route::get('refill-balance','BalanceController@refillBalance');
  Route::get('withdrawal','BalanceController@withdrawal');
  Route::post('withdrawal','BalanceController@saveWithdrawalRequest');

  Route::get('my-account','PageController@myAccount');
  Route::get('my-transactions','PageController@myTransactions');
  Route::get('resent-confirmation-mail','PageController@resentConfirmationMail');

  Route::get('security-center','SecurityController@securityCenter');
  Route::post('2fa/enable', 'SecurityController@enableTwoFactor');
  Route::get('2fa/disable', 'SecurityController@disableTwoFactor');
  Route::get('2fa/validate', 'TwoFAController@getValidateToken');
});

Route::group(['middleware' => ['admin']], function () {
  Route::get('admin/','AdminController@mainPage');
  Route::get('admin/withdrawal-requests','AdminController@withdrawalRequests');
  Route::post('admin/getWithdrawalStatusModalContent','AdminController@getWithdrawalStatusModalContent');
  Route::post('admin/setWithdrawalStatus','AdminController@setWithdrawalStatus');

});

Route::post('2fa/authenticate', ['middleware' => 'throttle:5', 'uses' => 'TwoFAController@authenticate2fa']);
Route::get('2fa-validation', 'PageController@twoFaValidationModal');
Route::post('send-support-message',['middleware' => 'throttle:5', 'uses' => 'SupportController@sendSupportMessage']);

Route::get('register/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'RegisterController@confirmUser'
]);
Route::get('login','PageController@loginModal');
Route::post('logout','LoginController@logout');

Route::post('showMoreDraws','PageController@showMoreDraws');
Route::post('register','RegisterController@register');
Route::post('login','LoginController@login');
