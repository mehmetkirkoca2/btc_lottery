<?php

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Transaction;
use App\Models\WaletAddress;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('push-btc', function (Request $request) {
  $requestDatas = $request->getContent();
  $requestDatas=json_decode($requestDatas);

  $requestKey=$requestDatas->request_key;

  $currency=$requestDatas->currency;
  $transactionId=$requestDatas->txid; //unique
  $amount=$requestDatas->amount;
  $amount=bcdiv($amount,100000000,8);
  $address=$requestDatas->address;
  $confirmations=$requestDatas->confirmations;
  $minConfirmations=$requestDatas->minConfirmations;

  if($address!='' and $amount!='' and $requestKey=="dk345gg45sdkds46ljfkasdf45jkk66ljdrrg566jgdfg357fgjfgj484"){

    $transaction=Transaction::where('currency',$currency)->where('address',$address)->where('tx_id',$transactionId)->first();
    $wallet=WaletAddress::where('address',$address)->where('currency',$currency)->first();

    if(is_null($transaction)) {

      if(is_null($wallet)){// wallet doesnt exist
        echo json_encode("no wallet");
        die();
      }else{
        $transaction= new Transaction;
        $transaction->user_id=$wallet->user_id;
        $transaction->currency=$currency;
        $transaction->amount=$amount;
        $transaction->address=$wallet->address;
        $transaction->tx_id=$transactionId;
        $transaction->status=0;
        $transaction->save();
      }

    }

    if($transaction->status==0 and $confirmations>=$minConfirmations){
      $transaction->status=1;
      $transaction->save();
      $wallet->balance=bcadd($wallet->balance,$amount,8);
      $wallet->save();
      echo json_encode("confirmed");
      die();
    }
    echo json_encode("passed");
    die();
  }else {
    echo json_encode("not passed");
    die();
  }

});
