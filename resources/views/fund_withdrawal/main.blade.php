@extends('layouts/app')

@section('title', 'Ana Sayfa')

@section('pageCss')
  <link rel="stylesheet" href="{!! asset('css/games/io6.css') !!}">
  <link rel="stylesheet" href="{!! asset('css/games/adaptive6.css') !!}">
  <link rel="stylesheet" href="{!! asset('admin_panel/plugins/sweet-alert/sweetalert2.min.css') !!}">
@endsection
@section('js-bottom')
  <script type="text/javascript" src="{!! asset('admin_panel/plugins/sweet-alert/sweetalert2.min.js') !!}"></script>
@endsection
@section('content')
  @php
    $userBalance=getUserBalance();
  @endphp
  <div class="content refill">
    <div class="heading_section with_tools">
      <h1>Fund withdrawal</h1>
    </div>
    <div class="widthdrawal_section" style="min-height: 818px;">
       <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24 xs-compensation">
          <h2>Send a request</h2>
          <form action="#" class="form-horizontal">
             <div class="form-group">
                <label for="widthdrawalWallet">Bitcoin-wallet address</label>
                <div class="col-sm-24">
                   <input type="text" id="widthdrawalWallet" class="form-control formControlWithdrawal" name="wallet" autocomplete="off">
                </div>
             </div>
             <div class="form-group">
                <label for="widthdrawalAmount">Amount for widthdrawal (from 0.001 BTC)</label>
                <div class="col-sm-24">
                   <input value="0" oninput="checkBalance('widthdrawalAmount');"  type="number" name="amount" id="widthdrawalAmount" step="0.001" class="form-control formControlWithdrawal" max="{{ bcsub($userBalance,0.001,8)}}" placeholder="0.002">
                </div>
             </div>
             <p class="text-muted">Available for withdrawal
                <a href="#" onclick="pushval('widthdrawalAmount',{{ bcsub($userBalance,0.001,8) }});">{{ $userBalance }} BTC</a>
             </p>
             <p class="text-muted">Service fee: <span id="fee">0.001</span> BTC</p>

             <div class="form-group noedit">
                <label for="widthdrawalTotal"><strong>Total</strong></label>
                <div class="col-sm-24">
                   <h3 id="widthdrawalTotal">0.001</h3>
                </div>
             </div>

             <div class="form-group">
                <label for="widthdrawalPass">Account's password</label>
                <div class="col-sm-24">
                   <input type="password" id="widthdrawalPass" class="form-control formControlWithdrawal" name="password" autocomplete="off">
                </div>
             </div>
             <button type="button" id="btnWithdrawalBTC" onclick="withdrawalBTC();" class="btn btn-orange btn-lg btn-strong">Send <span id="widthdrawalTotalBtn">0.000</span> BTC</button>
          </form>
          <p><a href="{{ url('my-transactions') }}" class="arrow-link">Transactions history</a></p>
       </div>
       <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24 text-center xs-compensation">
          <img src="{{ url('img/coins_slider.png') }}" alt="TrueFlip">
       </div>
    </div>
    @include('layouts/footer')
  </div>
  <script type="text/javascript">
    function withdrawalBTC() {
      $('#btnWithdrawalBTC').prop('disabled', true);
      var widthdrawalWallet=$('#widthdrawalWallet').val();
      var widthdrawalAmount=$('#widthdrawalAmount').val();
      var widthdrawalPass=$('#widthdrawalPass').val();
      if(widthdrawalWallet==''){
        swal('Required Field','Wallet address can not be empty','warning')
        $('#btnWithdrawalBTC').prop('disabled', false);
        return;
      }

      if(widthdrawalAmount==0){
        swal('Empty withdrawal amount','Withdrawal amount can not be zero','warning')
        $('#btnWithdrawalBTC').prop('disabled', false);
        return;
      }

      if(checkBalanceForWithdrawal(widthdrawalAmount)==false){
        swal('Insuffucent balance','Your blance is not enough for withdrawal amount','warning')
        $('#btnWithdrawalBTC').prop('disabled', false);
        return;
      }

      if(widthdrawalPass==''){
        swal('Password Field Required','Please type your password','warning')
        $('#btnWithdrawalBTC').prop('disabled', false);
        return;
      }

      $.ajax({
        type:'POST',
        data:{
          widthdrawalWallet:widthdrawalWallet,
          widthdrawalAmount:widthdrawalAmount,
          widthdrawalPass:widthdrawalPass
        },
        url:'withdrawal',
        success: function (result) {
          var result=JSON.parse(result);
          swal({
            allowOutsideClick:false,
            allowEscapeKey:false,
            title:result.title,
            text: result.message,
            type: result.status
            }).then(function() {
              if(result.status=='success'){
                window.location.href = "my-transactions";
              }else{
                $('#btnWithdrawalBTC').prop('disabled', false);
              }
          });
        }
      });
    }
  </script>
@endsection
