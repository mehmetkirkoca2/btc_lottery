<!-- Modal PRIZE TABLE Flips Star -->
<div class="modal fade" id="whatCanBeWonModal" tabindex="-1" role="dialog" aria-labelledby="whatCanBeWonModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="whatCanBeWonModalLabel">What can be won?</h4>
         </div>
         <div class="modal-body">
            <table class="table">
               <thead>
                  <tr>
                     <th>Match</th>
                     <th>Prizes / Winner Count</th>
                     <th>Probability</th>
                  </tr>
               </thead>
               @php
                 $getPrizes=getPrizes();
                 $prizes=$getPrizes['prizes'];
                 $totalPoolAmount=$getPrizes['totalPoolAmount'];
               @endphp
               <tbody>
                 @foreach ($prizes as $prize)
                   <tr>
                      <td>
                        @for ($i=0; $i < $prize['numbers']; $i++)
                          <i class="balls-icon"></i>
                        @endfor
                        @if($prize['plus_one']==1)
                         <i class="balls-icon balls-icon__orange"></i>
                        @endif
                      </td>
                      <td>
                        @if($prize['numbers']==5 and $prize['plus_one']==1)
                          <b>Jackpot : </b>({{ $prize['prize'] }} BTC + {{ $totalPoolAmount }})
                        @else
                          {{ $prize['prize'] }} BTC
                        @endif
                      </td>
                      <td>{{ $prize['probability'] }}</td>
                   </tr>
                 @endforeach
               </tbody>
            </table>
            <div style="color:red;">
              <small>
                <strong>
                  Note: These prizes are not final they will increase in every ticket purchasement.
                  If nobody guess the correct numbers. Showing prizes will add to the Jackpot prize. The +{{ $totalPoolAmount }} BTC showing you to added prizes from the previous draws
                </strong>
              </small>
            </div>
         </div>
      </div>
   </div>
</div>
