@extends('layouts/app')

@section('title', 'Ana Sayfa')

@section('pageCss')
  <link rel="stylesheet" href="{!! asset('css/games/io5.css') !!}">
  <link rel="stylesheet" href="{!! asset('css/games/adaptive5.css') !!}">
@endsection

@section('js-bottom')
  <script type="text/javascript" src="{{ asset('js/plugins/daily_draw.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/plugins/scrollbar.js') }}"></script>
@endsection

@section('content')
     <div class="heading_section">
        <h1>Buy tickets</h1>
     </div>
     <script type="text/javascript">
        var ticketControls = {
            'SELECT5':'Select 5 numbers',
            'SELECT1': 'And one more',
            'AUTO':'Auto',
            'CLEAR':'Clear'
        };
        var free_tickets = 0;
        window.multibet_enabled = false;
        var futureDraws = [];
     </script>
     <div class="play_section row-eq-height">
        <div class="col-lg-16 col-md-16 col-sm-24 col-xs-24">
           <div class="play_section__bets">
              <div class="row">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-24">
                    <h2>Tickets</h2>
                    <p>Draw №{{ $drawNo }}, {{ getDrawNumberDate() }} UTC</p>
                 </div>
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-24 text-right">
                    <a href="#" class="money-link" data-toggle="modal" data-target="#whatCanBeWonModal">What can be won?</a>
                 </div>
              </div>
           </div>
           <div class="play_section__tabs">
              <ul class="nav nav-tabs" role="tablist" id="betTabs">
                 <li role="presentation" class="active">
                   <a onclick="sgame.fn.multibets_disable();
                              $(this).tab('show');
                              return false;" href="#manual" aria-controls="manual" role="tab" data-toggle="tab">Manual</a>
                  </li>
                 <li role="presentation"><a onclick="sgame.fn.multibets_enable();
                                                     $(this).tab('show');
                                                     return false;" href="#multibet" aria-controls="multibet" role="tab" data-toggle"tab">Automatic multibet</a>
                  </li>
              </ul>
           </div>
           <div class="tab-content">
              <div role="tabpanel" class="tab-pane fade active" id="manual" >
                 <div class="play_section__bets">
                    <div class="tickets"></div>
                    <a href="#" class="btn btn-block btn-outline-dark btn-lg" onclick="sgame.fn.addTicket(2); return false;"><i class="ui-icons ui-icons__plus"></i> Add tickets</a>
                 </div>
              </div>
              <div role="tabpanel" class="tab-pane fade" id="multibet">
                 <div class="play_section__bets">
                    <div class="multibet__wrapper">
                       <div class="slider__item">
                          <label class="col-lg-6 col-md-8 col-sm-24 col-xs-24">Number of tickets</label>
                          <div class="col-lg-12 col-md-8 col-sm-12 col-xs-24">
                             <div class="slider__wrapper">
                                <span class="slider__from">1</span>
                                <input class="slider" id="ticketsSlider" data-slider-min="1" data-slider-max="100" data-slider-step="1" data-slider-value="2" type="text">
                                <span class="slider__to">100</span>
                             </div>
                          </div>
                          <div class="col-lg-6 col-md-8 col-sm-12 col-xs-24">
                             <input type="number" id="sgame_multy_tickets_qty" class="styled" min="1" max="100" step="1" value="2">
                          </div>
                       </div>
                       <div class="slider__item">
                          <label class="col-lg-6 col-md-8 col-sm-24 col-xs-24">Number of draws</label>
                          <div class="col-lg-12 col-md-8 col-sm-12 col-xs-24">
                             <div class="slider__wrapper">
                                <span class="slider__from">1</span>
                                <input class="slider" id="drawsSlider" data-slider-min="1" data-slider-max="15" data-slider-step="1" data-slider-value="1" type="text">
                                <span class="slider__to">15</span>
                             </div>
                          </div>
                          <div class="col-lg-6 col-md-8 col-sm-12 col-xs-24">
                             <input type="number" id="sgame_multy_draws_qty" class="styled" min="1" max="15" step="1" value="1">
                          </div>
                       </div>
                       <div class="slider__item">
                          <div class="col-lg-18 col-md-16 col-sm-16 col-xs-24">
                             <label>Same ticket for all draws</label>
                             <p class="text-muted">The same ticket for all draws. The equal numbers will be generated for each draw.  If disabled, each draw will have its combinations.</p>
                          </div>
                          <div class="col-lg-6 col-md-8 col-sm-8 col-xs-24">
                             <div class="switch">
                                <input type="checkbox" name="unique" checked>
                                <span class="slidersw round"></span>
                             </div>
                          </div>
                       </div>
                    </div>
                    <a href="#" class="btn btn-block btn-outline-dark btn-lg" onclick="$('.combo_list').toggle(300); sgame.fn.generateBets(); return false;"><i class="ui-icons ui-icons__combinations"></i> Show combinations</a>
                    <div class="combo_list" style="display:none;">
                       <div class="equal_combos">
                          <div class="cmb__title"> Draws <span id="equal_combos_number"></span></div>
                          <div class="cmb__list">
                          </div>
                          <div>
                             <a href="#" onclick="sgame.fn.generateBets(true); return false;" class="btn btn-block btn-outline-dark btn-lg">Regenerate all </a>
                          </div>
                       </div>
                       <div class="unique_combos">
                          <div class="cmb__list">
                             <div class="cmb_draw">
                                <a href="#" onclick="return false;" class="cmb_draw__refresh"></a>
                                <div class="cmb_draw__title"><span id="unique_combos_number_"></span></div>
                             </div>
                          </div>
                          <div>
                             <a href="#" onclick="sgame.fn.generateBets(true); return false;" class="btn btn-block btn-outline-dark btn-lg">Regenerate all</a>
                          </div>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-24 col-xs-24 play_section__form">
           <div class="discount_label discount_label__success hidden">
              - 15%
           </div>
           <h4>Make a bet</h4>
           <ul class="list-group">
              <li class="list-group-item dotted_line"><span>Filled-out tickets</span><span class="pull-right" id="form_tickets_count">0</span></li>
              <li class="multibet_form hidden list-group-item dotted_line"><span>Number of draws</span><span class="pull-right" id="form_draws_quantity_multibet">0</span></li>
              <li class="manual_form list-group-item inline"><span>Number of draws</span><span class="pull-right"><input type="number" class="styled" id="form_draws_quantity_manual" min="1" max="15" data-max="15" step="1" value="1"></span></li>
           </ul>
           <div class="total_block">
              <p>Total <strong class="pull-right" id="total_summ">0 <span>BTC</span></strong></p>
              {{-- <p class="discount_block hidden">Discount <span class="discount_small discount_small_green">15%</span> <s class="pull-right">0.006 BTC</s></p>
              <a href="#" data-toggle="modal" data-target="#discountModal" class="star-link star-link__success">How can I get an up to 30% off?</a>
              <a href="#" data-toggle="modal" data-target="#discountModal" class="star-link hidden star-link__danger">Good pick! Maximum winning odds.</a> --}}
           </div>
           <a href="#" id="paybtn" class="btn btn-block btn-lg btn-orange btn-strong">Pay using balance</a>
           <ul class="currency_list">
              <li><i class="cur-icon cur-icon-btc"></i></li>
              <li><i class="cur-icon cur-icon-ltc"></i></li>
              <li><i class="cur-icon cur-icon-eth"></i></li>
              <li><i class="cur-icon cur-icon-dash"></i></li>
              <li><i class="cur-icon cur-icon-doge"></i></li>
              <li><i class="cur-icon cur-icon-wallet"></i> <span>and other forms of refill</span></li>
           </ul>
           <div class="common_numbers">
              <p class="text-center"><strong>The most common numbers</strong></p>
              <div class="carousel_section">
                 <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                      @if(isset($combos))
                        @foreach ($combos as $key=> $combo)
                          @if($key==0)
                            <li data-target="#carousel-example-generic" data-slide-to="{{$key}}" class="active"></li>
                          @else
                            <li data-target="#carousel-example-generic" data-slide-to="{{$key}}"></li>
                          @endif
                        @endforeach
                      @endif
                    </ol>
                    <div class="carousel-inner" role="listbox">
                      @if(isset($combos))
                        @foreach ($combos as $key=> $combo)
                          <div class="item {{ $key==0? 'active':'' }}">
                             <ul class="combo_results">
                               @foreach ($combo as $number)
                                 <li>{{ $number }}</li>
                               @endforeach
                             </ul>
                             <a href="#" class="btn btn-outline-light" onclick="sgame.fn.pickCombo([{{ implode($combo,',') }}]); return false;">Pick these numbers</a>
                          </div>
                        @endforeach
                      @endif
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="" aria-hidden="true"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="" aria-hidden="true"></span>
                    </a>
                 </div>
              </div>
           </div>
        </div>
     </div>
     @include('layouts/footer')
   @include('play_daily_draw/modals')
@endsection
