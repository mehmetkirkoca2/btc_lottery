<!-- Modal Discount Flips Star -->
<div class="modal fade" id="discountModal" tabindex="-1" role="dialog" aria-labelledby="discountModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="discountModalLabel">More tickets, more chances! Get up to 30% discount!</h4>
         </div>
         <div class="modal-body">
            <div class="discountModal">
               <div class="discountModal__tickets discountModal__1"><strong>14 tickets</strong> or more</div>
               <div class="discountModal__tickets discountModal__2"><strong>28 tickets</strong> or more</div>
               <div class="discountModal__tickets discountModal__3"><strong>365 tickets</strong> or more</div>
               <span class="discountTT discountTT_1 discountTT__success">-5%</span>
               <span class="discountTT discountTT_2 discountTT__success">-15%</span>
               <span class="discountTT discountTT_3 discountTT__danger">-30%</span>
            </div>
            <p class="text-center">Don't miss this offer!</p>
            <button data-dismiss="modal" aria-label="Close" class="btn btn-block btn-orange btn-lg btn-shadow btn-strong ">Play now</button>
         </div>
      </div>
   </div>
</div>
<!-- Modal thank you -->
<div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog" aria-labelledby="thankyouModal">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title text-center" id="discountModalLabel">Thank you!</h4>
         </div>
         <div class="modal-body">
            <p class="text-center">
            <p class="text-center">Your request has been successfully submitted for processing.</p>
            <p><button data-dismiss="modal" aria-label="Close" data-href="/en/mygame" class="btn btn-block btn-orange btn-lg btn-shadow btn-strong ">Good luck</button></p>
         </div>
      </div>
   </div>
</div>

<!--noinex-->
<div class="modal fade " id="modalETHBlockChainWarning" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content ">
         <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Info</h4>
         </div>
         <div class="modal-body">
            <p>
               We found some issues related to syncing with ETH blockchain. Therefore, TFL and ETH balances cannot be updated now. After fixing the issues your ETH and TFL balances will be updated automatically. We are kindly asking you to wait until we finish. Sorry for temporary inconvenience.
            </p>
            <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-orange btn-lg">Ok</button>
         </div>
      </div>
   </div>
</div>
<!--/noindex-->
@include('modals/what_can_be_won')
<!-- Modal VIDEO -->
<div class="modal fade" id="fairvideoModal" tabindex="-1" role="dialog" aria-labelledby="...">
   <div class="modal-dialog" role="document">
      <div class="modal-content modal-md">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">How to check the results? </h4>
         </div>
         <iframe width="100%" height="315" src="https://www.youtube.com/embed/1aOt6OVs88I" frameborder="0" allowfullscreen></iframe>
      </div>
   </div>
</div>
