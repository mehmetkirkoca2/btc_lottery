@extends('layouts/app')
@section('title', 'Ana Sayfa')
@section('pageCss')
  <link rel="stylesheet" href="{!! asset('css/games/io2.css') !!}">
  <link rel="stylesheet" href="{!! asset('css/games/adaptive2.css') !!}">
@endsection
@section('js-bottom')
  <script type="text/javascript">
    function open2FaMocal() {
      $.ajax({
        type:'POST',
        data:{
        },
        url:'2fa/enable',
        success: function (data) {
          $( "#div2faModalContent" ).html( data );
          $('#twoFaModalProtection').modal('show');
        }
      });
    }
  </script>
@endsection
@section('content')
  <div class="security_section row-eq-height" style="min-height: 608px;">
   <div class="col-lg-16 col-md-16 col-sm-24 col-xs-24 left__content">
      <div class="section">
         <h6>1. E-mail verification</h6>
         @if (session()->has('message'))
           <div class="alert alert-success">
             {!! session('message') !!}
           </div>
         @endif
         @php
           $protection=0;
         @endphp
         @if(Auth::User()->status==0)
           <p>Confirm your email address, it will be used for sending confirmation codes and other important information.</p>
           <div>
              <div class="btn btn-default">
                 <i class="ui-icons ui-icons__delete"></i> Need confirmation
              </div>
              <a href="{{ url('resent-confirmation-mail') }}" class="btn btn-outline" data-action="resent_confemail">Resend confirmation E-mail</a>
           </div>
        @else
          @php
            $protection++;
          @endphp
          <div class="alert alert-success">Congratulation your account is verified.</div>
        @endif
      </div>
      <div class="section">
        <h6>2. Two-factor authentication</h6>
        <p>The highest level of security that will help to prevent unauthorized access to your account in case your password is stolen</p>

        <div>
          @if (Auth::user()->google2fa_secret)
            @php
              $protection++;
            @endphp
            <a href="{{ url('2fa/disable') }}" class="btn btn-warning">Disable 2FA</a>
          @else
            <div class="btn btn-default">
               <i class="ui-icons ui-icons__delete"></i> Not activated
            </div>
            <button class="btn btn-primary" onclick="open2FaMocal();">Activate 2FA</button>
          @endif
        </div>

      </div>
      <div class="section"></div>
   </div>
   <div class="col-lg-8 col-md-8 col-sm-24 col-xs-24 right__content">
      <div class="security_level">
         <h4>Current account security level</h4>
         <div class="sec-progress sec-progress-{{ $protection*50 }}">
           @if($protection==0)
             <label style="color:white"><span>Low<br> protection</span></label>
           @elseif ($protection==1)
            <span>Basic<br> protection</span>
           @elseif($protection==2)
             <span>Strong<br> protection</span>
           @endif
         </div>
      </div>
   </div>
</div>
@include('security_center/modals')
@include('layouts/footer')
@endsection
