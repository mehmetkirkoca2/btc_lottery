@extends('help_center/layout')
@section('content')
  @include('layouts/header')
  @include('layouts/left_nav_menu')
  <div class="layout__body">
    <div class="sidebar js-custom-scroll">
       <div class="sidebar__help">
          <h1>Help center</h1>
       </div>
       <ul class="sidebar__nav">
        @foreach ($categories as $category)
          <li><a href="{{ url('help-center/'.$category->id) }}" data-id="4" data-parent-id="1">{{ $category->name }}</a></li>
        @endforeach
       </ul>
    </div>
    <div class="content ">
       <div class="search_section">
          <h3>How may i assist you?</h3>
          <div class="col-lg-24">
             <form action="/" method="POST" onsubmit="helpcenter.fn.search(); return false;" id="searchform">
                <div class="input-group">
                   <input type="search" id="search_field" name="search" class="form-control" placeholder="">
                   <span class="input-group-btn">
                   <button class="btn" type="submit" onclick="helpcenter.fn.search(); return false;"></button>
                   </span>
                </div>
                <!-- /input-group -->
             </form>
             <p class="help_tips">For example, <a href="#" data-action="put_into_search_input">How can i change my password</a>?</p>
          </div>
       </div>
       <div class="search_results row-eq-height">
          <div class="col-lg-16 col-md-16 col-sm-24 col-xs-24 search_results__left">
             <ul class="help__breadcrumbs">
                <li><a href="/"><i class="ui-icons ui-icons__book"></i></a></li>
                <li>{{ $categoryName }}</li>
             </ul>
             <!--h3>The most common questions are</h3-->
             <ul class="questions__list">
               @foreach ($questions as $question)
                <li data-id="#q_{{ $question->id }}" data-action="scroll_to_answer"><a href="#q_{{ $question->id }}">{{ $question->question }}</a></li>
               @endforeach
             </ul>
             @foreach ($questions as $question)
               <div class="answer__item" id="q_{{ $question->id }}">
                  <p><strong>{{ $question->question }}</strong></p>
                  <p>{{ $question->answer }}</p>
                  <a href="#"><i class="ui-icons ui-icons__link"></i></a>
               </div>
             @endforeach

          </div>
          <div class="co-lg-8 col-md-8 col-sm-24 col-xs-24 search_results__right">
             <div class="help_right">
                <h4>Couldn't find the answer?</h4>
                <p>If you were unable to find an answer to your question using search box or navigating through the section of our help center, you can contact technical support using the form below.</p>
                <button onclick="toogleContactForm();" class="btn btn-warning">Contact technical support</button>
                <div class="support_form">
                   <form action="{{ url('send-support-message') }}" class="form" method="POST">
                      <div class="form-group">
                         <label for="name_surname">Intruduce yourself</label>
                         <div>
                            <input type="text" id="name_surname" name="nameSurname" class="form-control" required>
                         </div>
                      </div>
                      <div class="form-group">
                         <label for="email">E-mail</label>
                         <div>
                            <input type="email" id="email" name="email" class="form-control" required>
                         </div>
                      </div>
                      <div class="form-group">
                         <label for="subject">Subject</label>
                         <div>
                           <input type="text" id="subject" name="subject" class="form-control" required>
                         </div>
                      </div>
                      <div class="form-group">
                         <label for="message">Describe your problem or question in full details</label>
                         <div>
                            <textarea class="form-control" id="message" name="message" required></textarea>
                         </div>
                      </div>

                      <button class="btn btn-lg btn-block btn-orange btn-center" type="submit">Send a request</button>
                   </form>
                </div>
             </div>
          </div>
       </div>
       @include('layouts/footer')
    </div>
  </div>
  @include('layouts/modals')
@endsection
