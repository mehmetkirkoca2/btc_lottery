<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Account. Help Center. Daily Draw</title>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="description" content="Daily Draw is the first international anonymous fair games based on the blockchain technology with one-second payouts and 100% open source code">
      <meta property="og:title" content="Account. Help Center. Daily Draw">
      <meta property="og:description" content="Daily Draw is the first international anonymous fair games based on the blockchain technology with one-second payouts and 100% open source code">
      <meta property="og:url" content="">
      <meta name="keywords" content="">
      <meta property="og:site_name" content="">
      <meta property="og:type" content="website">
      <meta property="og:keywords" content="">
      <meta name="HandheldFriendly" content="True">
      <meta name="MobileOptimized" content="320">
      <meta http-equiv="cleartype" content="on">
      <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
      <link rel="stylesheet" href="{!! asset('help_center/css/layout.css') !!}">

      <link rel="stylesheet" href="{!! asset('help_center/css/io.css') !!}">
      <link rel="stylesheet" href="{!! asset('help_center/css/adaptive.css') !!}">

      <link rel="stylesheet" href="{!! asset('css/jquery.formstyler.css') !!}">
      <link rel="stylesheet" href="{!! asset('css/jquery.formstyler.theme.css') !!}">

   </head>
   <body>
    <div class="layout">
      @yield('content')
      <script type="text/javascript" src="{!! asset('js/jquery.min.js') !!}"></script>
      <script type="text/javascript" src="{!! asset('js/bootstrap.min.js') !!}"></script>
      <script type="text/javascript" src="{!! asset('js/plugins/jquery.formstyler.min.js') !!}"></script>
      <script type="text/javascript" src="{!! asset('js/plugins/jquert.mcustomscrollbar.min.js') !!}"></script>
      <script type="text/javascript" src="{!! asset('js/plugins/toastr-master/toastr.js') !!}"></script>

      <script type="text/javascript" src="{!! asset('help_center/js/app.js') !!}"></script>

    </div>
   </body>
</html>
