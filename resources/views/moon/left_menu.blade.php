<div class="sidebar js-custom-scroll mCustomScrollbar _mCS_2"><div id="mCSB_2" class="mCustomScrollBox mCS-orange mCSB_vertical mCSB_inside" tabindex="0" style="max-height: none;"><div id="mCSB_2_container" class="mCSB_container" style="position: relative; top: -13px; left: 0px;" dir="ltr">
    <div class="fs_sidebar__timer">
        <h2>Rapid to the Moon</h2>
        <div class="jp">
            <strong>Superprize</strong>
            <span>14.39 <sup>BTC</sup></span>
            <p>USD 204,160.02</p>
        </div>
                <a href="#signupModal" class="btn btn-orange btn-ticket btn-text btn-block hvr-pulse-grow" data-toggle="modal" data-target="#signupModal">Play now
            <span>Bets from 0.0001 BTC</span>
        </a>

        <!--div class="smoke">
                      <span class="s0"></span>
                        <span class="s1"></span>
                        <span class="s2"></span>
                        <span class="s3"></span>
                        <span class="s4"></span>
                        <span class="s5"></span>
                        <span class="s6"></span>
                        <span class="s7"></span>
                        <span class="s8"></span>
                        <span class="s9"></span>
                    </div>
        <div class="smoke smoke-right">
                      <span class="s0"></span>
                      <span class="s1"></span>
                      <span class="s2"></span>
                      <span class="s3"></span>
                      <span class="s4"></span>
                      <span class="s5"></span>
                      <span class="s6"></span>
                      <span class="s7"></span>
                      <span class="s8"></span>
                      <span class="s9"></span>
                  </div-->
    </div>
    <ul class="sidebar__nav">
        <li><a href="/" class="active">About the game</a></li>
                <li><a href="/en/how-to-play" class="">How to play</a></li>
        <li><a href="/en/archive" class="">Draw archive</a></li>
        <li><a href="/en/winners" class="">Winners</a></li>
        <li><a href="https://github.com/TrueFlip/prizecounter/tree/rapido" target="_blank">Source code on GitHub</a></li>
    </ul>
</div><div id="mCSB_2_scrollbar_vertical" class="mCSB_scrollTools mCSB_2_scrollbar mCS-orange mCSB_scrollTools_vertical" style="display: block;"><div class="mCSB_draggerContainer"><div id="mCSB_2_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 30px; display: block; height: 564px; max-height: 567px; top: 13px;"><div class="mCSB_dragger_bar" style="line-height: 30px;"></div></div><div class="mCSB_draggerRail"></div></div></div></div></div>
