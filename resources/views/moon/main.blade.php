@extends('layouts/app')

@section('title', 'Ana Sayfa')

@section('pageCss')
  <link rel="stylesheet" href="{!! asset('css/games/io3.css') !!}">
  <link rel="stylesheet" href="{!! asset('css/games/adaptive3.css') !!}">
@endsection

@section('content')
  <div class="content ">
    @include('moon/left_menu')
     <div class="jackpot_section">
        <div class="jackpot_section__content col-lg-8 col-md-12 pull-right">
           <h1>Rapid to the Moon</h1>
           <p>Fair international anonymous blockchain games with instant payouts, <a href="https://github.com/Daily Draw" target="_blank">open source code</a>, <a href="#">fair draw process</a> and <a href="https://blockchain.info/address/1FmQr2LMFQCp74Csyzbhq4JqzMDG7uMMnj" target="_blank">transparent prize fund</a></p>
           <div class="jackpot_pretty">
              <strong>Superprize</strong>
              <div class="jp_digits">
                 <span class="jackpot_digit">1</span>                <span class="jackpot_digit">4</span>
                 <span class="jackpot_dot"></span>
                 <span class="jackpot_digit">3</span>
                 <span class="jackpot_digit">9</span>
                 <span class="jackpot_digit digit_currency"></span>
              </div>
              <p>US$ 197,509</p>
           </div>
           <a href="#signupModal" class="btn btn-orange btn-stars btn-shadow btn-block" data-toggle="modal" data-target="#signupModal">Play now</a>
           <ul class="currency_list">
              <li><i class="cur-icon cur-icon-btc"></i></li>
              <li><i class="cur-icon cur-icon-ltc"></i></li>
              <li><i class="cur-icon cur-icon-eth"></i></li>
              <li><i class="cur-icon cur-icon-dash"></i></li>
              <li><i class="cur-icon cur-icon-doge"></i></li>
              <li><i class="cur-icon cur-icon-wallet"></i> <span>and other forms of adding funds</span></li>
           </ul>
        </div>
     </div>
     <div class="statistics_section">
        <div class="stat_block stat_block__results col-lg-8 col-md-8 col-sm-12 col-xs-24" style="height: 274px;">
           <p class="heading">Draw results №504324</p>
           <ul class="combo_results">
              <li>2</li>
              <li>14</li>
              <li>13</li>
              <li>4</li>
              <li>10</li>
              <li>5</li>
              <li>7</li>
              <li>3</li>
              <li>4</li>
           </ul>
           <div>
              <a href="#" data-action="rapid_timeline" data-drawid="504324" class="timeline-link">Timeline of the draw</a>
           </div>
           <div>
              <a href="/en/archive" class="arrow-link"><span>Draw archive</span></a>
           </div>
        </div>
        <div class="stat_block stat_block__won col-lg-8 col-md-8 col-sm-12 col-xs-24" style="height: 274px;">
           <p class="heading">Raffled since inception</p>
           <div class="raffled_block">
              16.175<sup>BTC</sup>
           </div>
           <div>
              <a href="/en/winners" class="arrow-link"><span>View history of winners and their stories</span></a>
           </div>
        </div>
        <div class="stat_block stat_block__transactions col-lg-8 col-md-8 col-sm-24 col-xs-24" style="height: 274px;">
           <p class="heading">Luckiest players <a href="#" data-action="refresh_mostlucky" class="btn pull-right"><i></i></a></p>
           <table class="table">
              <tbody>
                 <tr>
                    <td>ioa*****</td>
                    <td><span class="text-warning">0.02460 BTC</span></td>
                    <td class="hidden-md">14/01/2018</td>
                 </tr>
                 <tr>
                    <td>hac*****</td>
                    <td><span class="text-warning">0.02080 BTC</span></td>
                    <td class="hidden-md">15/01/2018</td>
                 </tr>
                 <tr>
                    <td>gab*****</td>
                    <td><span class="text-warning">0.01790 BTC</span></td>
                    <td class="hidden-md">11/01/2018</td>
                 </tr>
                 <tr>
                    <td>spj**</td>
                    <td><span class="text-warning">0.01730 BTC</span></td>
                    <td class="hidden-md">10/01/2018</td>
                 </tr>
                 <tr>
                    <td>erw*****</td>
                    <td><span class="text-warning">0.01700 BTC</span></td>
                    <td class="hidden-md">14/01/2018</td>
                 </tr>
              </tbody>
           </table>
        </div>
     </div>
     <div class="carousel_section">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
           <!-- Indicators -->
           <ol class="carousel-indicators">
              <li data-target="#carousel-example-generic" data-slide-to="0" class=""></li>
              <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
              <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
              <li data-target="#carousel-example-generic" data-slide-to="3" class=""></li>
              <li data-target="#carousel-example-generic" data-slide-to="4" class="active"></li>
           </ol>
           <!-- Wrapper for slides -->
           <div class="carousel-inner" role="listbox">
              <div class="item">
                 <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24">
                    <h4>Choose 8 numbers between 1 and 20 and at least one between 1 and 4</h4>
                    <p>Rapid To The Moon a fast numerical fair game, which takes place every 10-30 minutes. In this game, the main field is divided into two parts in each of which a player must choose numbers (eight in the first part and at least one in the second). In order to make an expanded bet, choose more than one number in the second part. Thus, you could increase chances of winning and the amount of possible winnings.</p>
                    <strong>For instance</strong>
                    <ul class="combo_results">
                       <li>1</li>
                       <li>2</li>
                       <li>3</li>
                       <li>4</li>
                       <li>5</li>
                       <li>6</li>
                       <li>7</li>
                       <li>2</li>
                    </ul>
                 </div>
                 <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24 text-center">
                    <img src="{!! asset('img/fs_mp_slide_1.jpg') !!}" alt="">
                 </div>
              </div>
              <div class="item">
                 <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24">
                    <h4>Fill out fair game tickets</h4>
                    <p>Choose the number of tickets that you would like to fill out and send to the draw. Fill them out manually or automatically (randomly by the system). If desired, the selected bet may not only play in the upcoming run but also play in several future ones.</p>
                    <strong>Single ticket price 0.0001 BTC (US$ 2.58)</strong>
                    <p>
                       <a class="btn btn-orange btn-lg btn-strong" href="#signupModal" data-toggle="modal" data-target="#signupModal">Fill out tickets now</a>
                    </p>
                 </div>
                 <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24 text-center">
                    <img src="{!! asset('img/fs_mp_slide_2.jpg') !!}" alt="">
                 </div>
              </div>
              <div class="item">
                 <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24">
                    <h4>Use the multiplier &amp; pay for participation via any preferable method</h4>
                    <p>Use the Multiplier to increase the amount of your prize in case you win. It will multiply any winnings including those won using an expanded bet. Then proceed to the payment methods. You can buy tickets for a draw right before it starts. The system will automatically convert your payment to bitcoin and transfer funds to the game's prize fund. The prize funds are transparent and always open for observation as we use the blockchain technology.</p>
                 </div>
                 <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24 text-center">
                    <img src="{!! asset('img/fs_mp_slide_3.jpg') !!}" alt="">
                 </div>
              </div>
              <div class="item">
                 <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24">
                    <h4>Wait for a run</h4>
                    <p>Rapid To The Moon draws take place each 10-30 minutes. The winning combination is determined by an algorithm based on the bitcoin blockchain. The algorithm is published on <a href="#" target="_blank">GitHub</a>. The draw is broadcasted on Daily Draw. Results are published in the Draw archive page.</p>
                    <p>
                       <a class="btn btn-orange btn-lg btn-strong" href="#signupModal" data-toggle="modal" data-target="#signupModal">Participate now</a>
                    </p>
                 </div>
                 <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24 text-center">
                    <img src="{!! asset('img/fs_mp_slide_4.jpg') !!}" alt="">
                 </div>
              </div>
              <div class="item active">
                 <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24">
                    <h4>In case of the match of numbers</h4>
                    <p>All winnings in Rapid to the Moon, except for the Jackpot are fixed. When you use an expanded bet, your earnings in any category increase, except for the first winning combination (4 + 1) and the Jackpot. The Jackpot can be won only by a participant who could guess 8 + 1 numbers. The Jackpot is an accumulated amount. In an event when nobody could guess the combination in the current draw, the accumulated fund will be used for the next draw.</p>
                    <p>
                       <a class="btn btn-orange btn-shadow btn-lg btn-strong" data-toggle="modal" data-target="#myModal" href="#myModal">What can be won?</a>
                    </p>
                 </div>
                 <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24 text-center">
                    <img src="{!! asset('img/fs_mp_slide_5.jpg') !!}" alt="">
                 </div>
              </div>
           </div>
           <!-- Controls -->
           <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
           <span class="" aria-hidden="true"></span>
           </a>
           <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
           <span class="" aria-hidden="true"></span>
           </a>
        </div>
     </div>
     <div class="advantages_section">
        <h4>Why Daily Draw</h4>
        <div class="row">
           <div class="advantage col-lg-8 col-md-8 col-sm-12 col-xs-24">
              <i class="adv-icon adv-icon__01"></i> <span>Instant payouts</span>
           </div>
           <div class="advantage col-lg-8 col-md-8 col-sm-12 col-xs-24">
              <i class="adv-icon adv-icon__02"></i> <span>Absolutely fair!</span>
           </div>
           <div class="advantage col-lg-8 col-md-8 col-sm-12 col-xs-24">
              <i class="adv-icon adv-icon__03"></i> <span>60% of the proceeds of the ticket sales join the prize fund</span>
           </div>
           <div class="advantage col-lg-8 col-md-8 col-sm-12 col-xs-24">
              <i class="adv-icon adv-icon__04"></i> <span>Multi-currency</span>
           </div>
           <div class="advantage col-lg-8 col-md-8 col-sm-12 col-xs-24">
              <i class="adv-icon adv-icon__05"></i> <span>Full transrarency</span>
           </div>
           <div class="advantage col-lg-8 col-md-8 col-sm-12 col-xs-24">
              <i class="adv-icon adv-icon__06"></i> <span>International</span>
           </div>
        </div>
     </div>
     <div class="faq_section">
        <div class="row">
           <div class="col-lg-16 col-md-14 col-sm-12 col-xs-24">
              <h4>Frequently asked questions</h4>
              <div class="faq_collapsed">
                 <ul class="list-group">
                    <li class="list-group-item faq__question open">Do you take any commission for withdrawal?<span class="pull-right"><i class="toggled_icon toggled_icon__plus"></i></span></li>
                    <li class="list-group-item faq__answer" style="display: block;">Yes we do. Service fee is 1%</li>
                    <li class="list-group-item faq__question">What's the winning probability in your lottery?<span class="pull-right"><i class="toggled_icon toggled_icon__plus"></i></span></li>
                    <li class="list-group-item faq__answer" style="display: none;">The chart of probabilities is given here {{url('/')}}newico/img/prize_table.jpg</li>
                    <li class="list-group-item faq__question">How much do I get if I hit the Jackpot?<span class="pull-right"><i class="toggled_icon toggled_icon__plus"></i></span></li>
                    <li class="list-group-item faq__answer" style="display: none;">You&amp;#39;ll get the whole amount except for 1% fee.</li>
                 </ul>
              </div>
              <a href="http://help Draw" class="btn btn-outline btn-arrow btn-lg">More answers in our Help Center</a>
           </div>
           <div class="col-lg-8 col-md-10 col-sm-12 col-xs-24">
              <div class="win_story">
                 <h4>People really win</h4>
                 <div class="win_story__body">
                    I played 10 times and won twice! One 0.025btc another 0.0025btc.Wow,I am so lucky!!!
                 </div>
                 <div class="win_story__user text-center">
                    eud*****
                    <strong>Won 0.00000 BTC</strong>
                 </div>
                 <a href="/en/winners" class="btn btn-block btn-outline btn-arrow btn-lg">Read more stories</a>
              </div>
           </div>
        </div>
     </div>
     @include('layouts/footer')
  </div>
@endsection
