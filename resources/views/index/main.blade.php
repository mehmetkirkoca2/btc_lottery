@extends('layouts/app')
@section('title', 'Main Page')
@section('pageCss')
  <link rel="stylesheet" href="{!! asset('css/games/io2.css') !!}">
  <link rel="stylesheet" href="{!! asset('css/games/adaptive2.css') !!}">
@endsection
@section('js-bottom')
  <script type="text/javascript" src="{!! asset('js/plugins/moment.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('js/plugins/moment-timezone.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('js/plugins/jquery.countdown.min.js') !!}"></script>
  <script type="text/javascript">
     var nextDrawTime = moment.tz("{{ getDrawNumberDate() }}", "UTC");
     $('#spanCountdown').countdown(nextDrawTime.toDate(), function(event) {
       $(this).html(event.strftime('%H:%M:%S'));
     });
     @if ($openModal==1)
       $('#signinModal').modal('show');
     @endif
     @if ($openModal==2)
       $('#modal2FAValidation').modal('show');
     @endif

  </script>
@endsection
@php
  $jackpotPrize=getJackPotPrize();
  $usdPrice=getBtcToUsd('USD');
  $calculatedJackpostPrizeUsd=bcmul($jackpotPrize,$usdPrice,3);
  $prizeDigits=str_split($jackpotPrize);
  $numbers=$lastDrawNumbers['numbers'];
  $plusOne=$lastDrawNumbers['plusOne'];
  $drawId=$lastDrawNumbers['drawId'];
@endphp
@section('content')
<div class="content no_left_nav">
   <div class="top_section">
      <div class="col-lg-8 col-md-12 col-sm-24 col-xs-24 top_section__text">
         <h1>The future of <span class="text-warning">fair games</span> is here</h1>
         <p>Take a journey to the world of blockchain games with instant payouts, open source code, fair draw and its transparent prize fund.</p>
      </div>
   </div>
   <div class="carousel_section">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
         <!-- Indicators -->
         <!-- Wrapper for slides -->
         <div class="carousel-inner" role="listbox">
            <div class="item active" id="MAIN_banner_1">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-24 left_ban">
                  <p class="text-head">The world's <span class="text-warning">largest</span></p>
                  <p>Fair international blockchain game</p>
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-24 right_ban">
                  <div class="jackpot_pretty">
                     <h5>Jackpot</h5>
                     <div class="jp_digits">
                        @foreach ($prizeDigits as $key=>$prizeDigit)
                          @if( $key <= 4 )
                            @if( $prizeDigit == '.' )
                              <span class="jackpot_dot"></span>
                            @else
                              <span class="jackpot_digit">{{ $prizeDigit }}</span>
                            @endif
                          @endif
                        @endforeach
                        <span class="jackpot_digit digit_currency"></span>
                     </div>
                     <p>US$ {{$calculatedJackpostPrizeUsd}}</p>
                  </div>
                  @if(Auth::User())
                    <a href="{{ url('play-daily-draw') }}" class="btn btn-orange btn-stars">Play now</a>
                  @else
                    <a href="#signupModal" class="btn btn-orange btn-stars" data-toggle="modal" data-target="#signupModal">Play now</a>
                  @endif
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="section_games row-eq-height">
      <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24 section_games__star" style="width:100%;">
         <div class="games__item">
            @if(! is_null($numbers))
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-24 responsive">
                 <h3>Daily Draw</h3>
                 <p class="desc">Similar to any ordinary game: guess 5 numbers between 1 and 49 and one between 1 and 26. The system then randomly draws 6 numbered balls.</p>
                 <!-- LINK TO PLAY STAR -->
                 <a href="#" class="arrow-link"><span>Learn more</span></a>
                 <p>Draw results <a href="#">№{{ $drawId }}</a></p>
                 <div>
                    <ul class="combo_results combo_results__star">
                       @foreach ($numbers as $number)
                         <li>{{ $number }}</li>
                       @endforeach
                       <li>{{ $plusOne }}</li>
                    </ul>
                 </div>
                 <!-- LINK TO STAR ATCHIVE -->
                 <p class="text-small"><a href="{{ url('draw-archive') }}">Draw archive</a></p>
              </div>
            @endif
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-24 games__item_jpblock">
               <div class="fs_sidebar__timer">
                  <div class="jp">
                     <strong>Jackpot</strong>
                     <span>{{ $jackpotPrize }} <sup>BTC</sup></span>
                     <p>USD {{ $calculatedJackpostPrizeUsd }}</p>
                  </div>
                  <!-- LINK TO PLAY STAR / SIGNUP AND PLAY STAR -->
                  @if(Auth::User())
                    <a href="{{ url('play-daily-draw') }}" class="btn btn-orange btn-ticket btn-text btn-block hvr-pulse-grow">Play now <span>Bets from 0.0002 BTC</span></a>
                  @else
                    <a href="#signupModal" class="btn btn-orange btn-ticket btn-text btn-block hvr-pulse-grow" data-toggle="modal" data-target="#signupModal">Play now <span>Bets from 0.0002 BTC</span></a>
                  @endif
                  <div class="flipstar_draw_timer">
                     <span class="reverce_timer" id="spanCountdown">00:00:00</span>
                     <span>Till the next<br>draw</span>
                  </div>
                  <p>Draws take place once a day</p>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="statistics_section row-eq-height">
      <div class="col-lg-8 col-md-12 col-sm-12 col-xs-24 statistics__block">
         <h4>Recent bets</h4>
         <ul class="nav nav-pills" role="tablist">
            <li role="presentation" class="active"><a href="#bets_star" aria-controls="bets_star" role="tab" data-toggle="tab">Daily Draw</a></li>
         </ul>
         <!-- Tab panes -->
         <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade active" id="bets_star">
               <table class="table">
                  <thead>
                     <tr>
                        <th>Player</th>
                        <th>Draw</th>
                        <th class="hidden-sm hidden-xs">Date</th>
                        <th>Numbers</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if(! is_null($recentBets))
                       @foreach ($recentBets as $recentBet)
                         <tr>
                            <td>{{ substr($recentBet->user->email,0,3) }}***</td>
                            <td>{{ $recentBet->draw_id }}</td>
                            <td class="hidden-sm hidden-xs">{{ $recentBet->created_at->format('m/d/Y') }}</td>
                            <td>
                               <ul>
                                  <li>{{ $recentBet->n1 }}</li>
                                  <li>{{ $recentBet->n2 }}</li>
                                  <li>{{ $recentBet->n3 }}</li>
                                  <li>{{ $recentBet->n4 }}</li>
                                  <li>{{ $recentBet->n5 }}</li>
                                  <li>{{ $recentBet->n6 }}</li>
                               </ul>
                            </td>
                         </tr>
                       @endforeach
                     @endif
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      <div class="col-lg-8 col-md-12 col-sm-12 col-xs-24 statistics__block">
         <h4>Recent wins</h4>
         <ul class="nav nav-pills" role="tablist">
            <li role="presentation" class="active"><a href="#wins_star" aria-controls="wins_star" role="tab" data-toggle="tab">Daily Draw</a></li>
         </ul>
         <!-- Tab panes -->
         <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade  active" id="wins_star">
               <table class="table">
                  <thead>
                     <tr>
                        <th>Player</th>
                        <th>Draw</th>
                        <th class="hidden-sm hidden-xs">Date</th>
                        <th>Winning</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if(! is_null($recentWins))
                       @foreach ($recentWins as $recentWin)
                         <tr>
                            <td>{{ substr($recentWin->user->email,0,3) }}***</td>
                            <td>{{ $recentWin->daily_draw_id }}</td>
                            <td class="hidden-sm hidden-xs">{{ $recentWin->created_at->format('m/d/Y') }}</td>
                            <td>{{ $recentWin->prize_amount }} BTC</td>
                         </tr>
                       @endforeach
                     @endif
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      <div class="col-lg-8 col-md-24 col-sm-24 col-xs-24 statistics__block">
         <ul class="nav nav-pills" role="tablist">
            <li role="presentation" class="active"><a href="#deposit_tab" aria-controls="deposit_tab" role="tab" data-toggle="tab">Deposit</a></li>
            <li role="presentation"><a href="#withdrawal_tab" aria-controls="withdrawal_tab" role="tab" data-toggle="tab">Withdrawal</a></li>
         </ul>
         <!-- Tab panes -->
         <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade active" id="deposit_tab">
               <table class="table">
                  <thead>
                     <tr>
                        <th>Transaction ID</th>
                        <th>User</th>
                        <th class="hidden-xs">Date</th>
                        <th>Amount</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if(! is_null($recentTransactions))
                       @foreach ($recentTransactions as $recentTransaction)
                         <tr>
                            <td><a href="https://blockchain.info/tx/{{ $recentTransaction->tx_id }}" target="_blank">{{ $recentTransaction->tx_id }}</a></td>
                            <td>{{ substr($recentTransaction->user->email,0,3) }}***</td>
                            <td class="hidden-xs">{{ $recentTransaction->created_at->format('m/d/Y') }}</td>
                            <td>
                               {{ $recentTransaction->amount }} {{ $recentTransaction->currency }}
                            </td>
                         </tr>
                       @endforeach
                     @endif
                  </tbody>
               </table>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="withdrawal_tab">
               <table class="table">
                  <thead>
                     <tr>
                        <th>Transaction ID</th>
                        <th>User</th>
                        <th class="hidden-xs">Date</th>
                        <th>Amount</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if(! is_null($recentWithdrawals))
                       @foreach ($recentWithdrawals as $recentWithdrawal)
                         <tr>
                            <td><a href="#" target="_blank">{{ $recentWithdrawal->tx_id }}</a></td>
                            <td>{{ substr($recentWithdrawal->user->email,0,3) }}***</td>
                            <td class="hidden-xs">{{ $recentWithdrawal->created_at->format('m/d/Y') }}</td>
                            <td>
                               {{ $recentWithdrawal->amount }} {{ $recentWithdrawal->currency }}
                            </td>
                         </tr>
                       @endforeach
                     @endif
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
   </div>
   <div class="clearfix"></div>
   <div class="numbers_section" style="padding-top:20px;">
      <h3><span>Frequency of numbers</span></h3>
      <ul class="nav nav-pills" role="tablist">
         <li role="presentation" class="active"><a href="#numbers_star" aria-controls="numbers_star" role="tab" data-toggle="tab">Daily Draw</a></li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
         <div role="tabpanel" class="tab-pane fade active" id="numbers_star">
            <div class="numbers_star__chart col-lg-24 col-md-24 col-sm-24 col-xs-24" id="numbersStar">
               @foreach ($frequencyOfNumbers as $frequencyOfNumber)
                 @if($frequencyOfNumber->frequency!=0)
                   <div class="bar">
                      <div class="title">{{ $frequencyOfNumber->number }}</div>
                      <div class="value" data-placement="top" title="%{{ $frequencyOfNumber->frequency }}" style="height: {{ $frequencyOfNumber->frequency*3 }}%;"></div>
                   </div>
                 @endif
               @endforeach
            </div>
         </div>
      </div>
   </div>
   <div class="investor_section">
      <div class="col-lg-24 col-md-24 col-sm-24 col-xs-24">
         <h4>The games of Daily Draw in figures</h4>
         <div class="investor__rates col-md-offset-3 col-lg-7 col-md-7 col-sm-12 col-xs-24">
            <span class="text-warning">{{ $ticketSoldLastMounth }}</span> Tickets sold last month
         </div>
         <div class="investor__rates col-lg-7 col-md-7 col-sm-12 col-xs-24">
            <span class="text-warning">{{ $winnersLastMounth }}</span> Winners in the last month
         </div>
         <div class="investor__rates col-lg-7 col-md-7 col-sm-12 col-xs-24">
            <span class="text-warning">{{ substr($paidToUserLastMounth,0,6) }}<sup>BTC</sup></span> Paid to users last month
         </div>
      </div>
   </div>
</div>
@include('layouts/footer')
@endsection
