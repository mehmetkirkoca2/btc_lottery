@extends('layouts/app')

@section('title', 'Ana Sayfa')

@section('pageCss')
  <link rel="stylesheet" href="{!! asset('css/games/io6.css') !!}">
  <link rel="stylesheet" href="{!! asset('css/games/adaptive6.css') !!}">
@endsection

@section('content')
     <div class="content refill">
        <div class="heading_section with_tools">
           <h1>Refill balance</h1>
        </div>
        <div class="refill_section" style="min-height: 886px;">
           <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#btc_tab" aria-controls="btc_tab" role="tab" data-toggle="tab"><i class="cur-icon cur-icon-btc-dark"></i></a></li>
           </ul>
           <div class="tab-content">
              <div role="tabpanel" class="col-lg-12 col-md-12 col-sm-24 col-xs-24 tab-pane fade active xs-compensation" id="btc_tab">
                 <h3>Bitcoin</h3>
                 <div class="form-group">
                    <label>Top-up amount</label>
                    <div class="col-sm-18 xs-compensation">
                       <div class="input-group">
                          <input type="number" id="btcAmount" class="form-control" step="0.001" min="0.001" placeholder="0.01" onkeyup="generateQrbarcode();">
                          <span class="input-group-addon">BTC</span>
                       </div>
                    </div>
                    <div class="col-sm-6 comission_block xs-compensation">
                       <strong>= $<span id="usdAmount"></span></strong>
                    </div>
                 </div>
                 <p>Top-up address</p>
                 @php
                   $userBtcAddress=getUserAddress()
                 @endphp
                 <p><a href="#" class="copy-link" data-cp="{{$userBtcAddress}}" data-message="Copied to clipboard!">{{$userBtcAddress}}</a></p>
                 <p><a href="#" class="btn btn-outline-dark" data-cp="{{$userBtcAddress}}" data-message="Copied to clipboard!">Copy to clipboard</a></p>
                 <p>Or scan the QR-code</p>
                 <p><img id="qrCode" src="https://chart.googleapis.com/chart?chs=195x195&cht=qr&chl=bitcoin:{{$userBtcAddress}}?amount=0"></p>
                 <p><a href="{{ url('rofile') }}" class="arrow-link">Transaction history</a></p>
              </div>
           </div>
        </div>
        @include('layouts/footer')
     </div>
@endsection
