@extends('layouts/app')
@section('title', 'Ana Sayfa')
@section('pageCss')
  <link rel="stylesheet" href="{!! asset('css/games/io2.css') !!}">
  <link rel="stylesheet" href="{!! asset('css/games/adaptive2.css') !!}">
@endsection
@section('js-bottom')

@endsection
@section('content')
  <div class="security_section row-eq-height" style="min-height: 608px;">
   <div class="col-lg-24 col-md-24 col-sm-24 col-xs-24 left__content">
      <div class="section">
         <h6>My Transactions</h6>
         <table class="table table-info">
           <tr>
             <th>Type</th>
             <th>Coin</th>
             <th>Amount</th>
             <th>Information</th>
             <th>Status</th>
           </tr>
           @foreach ($myTransactions as $myTransaction)
             <tr>
               <td>{{$myTransaction->type==1 ? 'Deposit':'Withdrawal'}}</td>
               <td>{{$myTransaction->currency}}</td>
               <td>{{$myTransaction->amount}}</td>
               <td>
                 <div><label>Address :</label>{{$myTransaction->address}}</div>
                 <div><label>Tx Id :</label>{{$myTransaction->tx_id}}</div>
               </td>
               <td>
                 @if($myTransaction->status==0)
                   Waiting
                 @elseif ($myTransaction->status==1)
                   Transfer Confirmed
                 @elseif ($myTransaction->status==2)
                   Approved for Transfer
                 @endif
               </td>
             </tr>
           @endforeach
         </table>
      </div>
      <div class="section"></div>
   </div>
</div>
@include('layouts/footer')
@endsection
