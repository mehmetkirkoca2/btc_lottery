@extends('layouts/app')

@section('title', 'Daily Draw')
@section('pageCss')
  <link rel="stylesheet" href="{!! asset('css/games/io.css') !!}">
  <link rel="stylesheet" href="{!! asset('css/games/adaptive.css') !!}">
@endsection
@section('js-bottom')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript">
    $( function() {
      $( "#accordion" ).accordion({
        active: 0
      });
    } );
  </script>
  <style media="screen">
    .ui-accordion .ui-accordion-header {
      display: block;
      cursor: pointer;
      position: relative;
      margin: 2px 0 0 0;
      padding: .5em .5em .5em .7em;
      font-size: 100%;
      margin-bottom: 5px;
      background-color: #EFB600;
      border-color: #FFB606;
    }
  </style>
@endsection
@php
  $jackpotPrize=getJackPotPrize();
  $lastDrawNumbers=getLastDrawNumbers();
  $usdPrice=getBtcToUsd('USD');
  $calculatedJackpostPrizeUsd=bcmul($jackpotPrize,$usdPrice,3);
  $prizeDigits=str_split($jackpotPrize);
  $numbers=$lastDrawNumbers['numbers'];
  $plusOne=$lastDrawNumbers['plusOne'];
  $drawId=$lastDrawNumbers['drawId'];
  $raffledPrize=getRaffledPrize();
@endphp
@section('content')
  <div class="jackpot_section">
     <div class="jackpot_section__content col-lg-8 col-md-12 pull-right">
        <h1>Daily Draw</h1>
        <p>Fair international blockchain games with instant payouts, <a href="https://github.com/Daily Draw" target="_blank">open source code</a>, <a href="#fairvideoModal" data-toggle="modal" data-target="#fairvideoModal">fair draw process</a> and <a href="https://blockchain.info/address/1FmQr2LMFQCp74Csyzbhq4JqzMDG7uMMnj" target="_blank">transparent prize fund</a></p>
        <div class="jackpot_pretty">
           <div class="jp_digits">
             @foreach ($prizeDigits as $key=>$prizeDigit)
               @if( $key <= 4 )
                 @if( $prizeDigit == '.' )
                   <span class="jackpot_dot"></span>
                 @else
                   <span class="jackpot_digit">{{ $prizeDigit }}</span>
                 @endif
               @endif
             @endforeach
              <span class="jackpot_digit digit_currency"></span>
           </div>
           <p>US$ {{ $calculatedJackpostPrizeUsd }}</p>
        </div>
        @if(Auth::User())
          <a href="{{ url('play-daily-draw') }}" class="btn btn-orange btn-stars btn-shadow btn-block">Play now</a>
        @else
          <a href="#signupModal" class="btn btn-orange btn-stars btn-shadow btn-block" data-toggle="modal" data-target="#signupModal">Play now</a>
        @endif
     </div>
  </div>
  <div class="statistics_section">
    @if(! is_null($numbers))
      <div class="stat_block stat_block__results col-lg-8 col-md-8 col-sm-12 col-xs-24" style="height:218px;">
         <p class="heading">Draw results №{{ $drawId }}</p>
         <ul class="combo_results">
           @foreach ($numbers as $number)
             <li>{{ $number }}</li>
           @endforeach
           <li>{{ $plusOne }}</li>
         </ul>
         <div>
            <a href="{{ url('draw-archive') }}" class="arrow-link"><span>Draw archive</span></a>
         </div>
      </div>
    @endif
     <div class="stat_block stat_block__won col-lg-8 col-md-8 col-sm-12 col-xs-24">
        <p class="heading">Raffled since inception</p>
        <div class="raffled_block">
           {{ $raffledPrize }}<sup>BTC</sup>
        </div>
     </div>
     <div class="stat_block stat_block__transactions col-lg-8 col-md-8 col-sm-24 col-xs-24" style="height:218px;">
        <p class="heading">Luckiest players </p>
        <table class="table">
           <tbody>
             @foreach ($luciestWinners as $luciestWinner)
               <tr>
                  <td>{{ substr($luciestWinner->user->email,0,3) }}***</td>
                  <td><span class="text-warning">{{ $luciestWinner->prize_amount }} BTC</span></td>
               </tr>
             @endforeach
           </tbody>
        </table>
     </div>
  </div>
  <div class="carousel_section">
     <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
           <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
           <li data-target="#carousel-example-generic" data-slide-to="1"></li>
           <li data-target="#carousel-example-generic" data-slide-to="2"></li>
           <li data-target="#carousel-example-generic" data-slide-to="3"></li>
           <li data-target="#carousel-example-generic" data-slide-to="4"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
           <div class="item active">
              <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24">
                 <h4>Guess 5 numbers from 1 to 49 and 1 number from 1 to 26</h4>
                 <p>The game is based on the PowerBall principle. However, we have decided to significantly increase the winning possibility by reducing the total amount of numbers from 69 to 49, which gives about a 1.3 time more significant chance to win than the one available in the PowerBall itself.</p>
                 <strong>For instance</strong>
                 <ul class="combo_results">
                    <li>1</li>
                    <li>2</li>
                    <li>3</li>
                    <li>4</li>
                    <li>5</li>
                    <li>1</li>
                 </ul>
              </div>
           </div>
           <div class="item">
              <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24">
                 <h4>Fill out fair games tickets</h4>
                 <p>Choose the number of tickets that you would like to fill out and send to the draw. Fill them out manually or automatically (randomly by the system). If desired, the selected bet may not only play in the upcoming run but also play in several future ones.</p>
                 <strong>One ticket price is 0.0002 BTC</strong>
                 @if(Auth::User())
                   <p><a href="{{ url('play-daily-draw') }}" class="btn btn-orange btn-shadow btn-lg btn-strong">Get a ticket</a></p>
                 @else
                   <p><a class="btn btn-orange btn-shadow btn-lg btn-strong" href="#signupModal" data-toggle="modal" data-target="#signupModal">Get a ticket</a></p>
                 @endif
              </div>
           </div>
           <div class="item">
              <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24">
                 <h4>Buy a ticket secure and safe</h4>
                 <p>We accept BTC. We will accept other coins soon.You can Fund deposit/withdrawal is instant and secured by 2FA and e-mail verification. All related transactions appear on the blockchain, so they are easy to check. We purpose 60% of the ticket sale earnings to join the Prize fund.</p>
                 @if(Auth::User())
                   <a href="{{ url('play-daily-draw') }}" class="btn btn-orange btn-shadow btn-lg btn-strong">Get a ticket</a>
                 @else
                   <a class="btn btn-orange btn-shadow btn-lg btn-strong" href="#signupModal" data-toggle="modal" data-target="#signupModal">Get a ticket</a>
                 @endif
              </div>
           </div>
           <div class="item">
              <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24">
                 <h4>Wait for a run</h4>
                 <p>Draws occur daily at (08:00 PM) 20:00 GMT. The system determines the winning set through an open-source randomizer. The code is published on GitHub. The results are posted on the website and in official social media channels and social networks.</p>
                 <div class="flipstar_draw_timer">
                    <span>Till the next draw</span>
                    <span class="reverce_timer">15:14:10</span>
                 </div>
              </div>
           </div>
           <div class="item">
              <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24">
                 <h4>If your numbers match</h4>
                 <p>Winning bets are the ones which ticket numbers match those of the winning combination set out in the table below. If some category loses in the current draw, then the accumulated amount joins the next draw as a part of the Jackpot.</p>
                 <p>
                    <a class="btn btn-orange btn-shadow btn-lg btn-strong" data-toggle="modal" data-target="#whatCanBeWonModal" href="#whatCanBeWonModal">What can be won?</a>
                 </p>
              </div>
           </div>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="" aria-hidden="true"></span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="" aria-hidden="true"></span>
        </a>
     </div>
  </div>
  <div class="advantages_section">
     <h4>Why Daily Draw</h4>
     <div class="row">
        <div class="advantage col-lg-8 col-md-8 col-sm-12 col-xs-24">
           <i class="adv-icon adv-icon__01"></i> <span>Instant payouts</span>
        </div>
        <div class="advantage col-lg-8 col-md-8 col-sm-12 col-xs-24">
           <i class="adv-icon adv-icon__02"></i> <span>Absolutely fair!</span>
        </div>
        <div class="advantage col-lg-8 col-md-8 col-sm-12 col-xs-24">
           <i class="adv-icon adv-icon__03"></i> <span>60% of the proceeds of the ticket sales join the prize fund</span>
        </div>
        <div class="advantage col-lg-8 col-md-8 col-sm-12 col-xs-24">
           <i class="adv-icon adv-icon__05"></i> <span>Full transrarency</span>
        </div>
        <div class="advantage col-lg-8 col-md-8 col-sm-12 col-xs-24">
           <i class="adv-icon adv-icon__06"></i> <span>International</span>
        </div>
     </div>
  </div>
  <div class="faq_section">
     <div class="row">
        <div class="col-lg-16 col-md-14 col-sm-24 col-xs-24">
           <h4>Frequently asked questions</h4>
           <div id="accordion">
            <h3>Do you take any commission for withdrawal?</h3>
            <div>
              <p>
                Yes we do. Service fee is 1%
              </p>
            </div>
            <h3>What is the winning probability in your lottery?</h3>
            <div>
              <p>
                The chart of probabilities is given here <a class="btn btn-orange btn-shadow" data-toggle="modal" data-target="#whatCanBeWonModal" href="#whatCanBeWonModal">What can be won?</a>
              </p>
            </div>
            <h3>How much do I get if I hit the Jackpot?</h3>
            <div>
              <p>
                You will get the whole amount except for 1% fee.
              </p>
            </div>
          </div>
         <a href="{{ url('help-center') }}" class="btn btn-outline btn-arrow btn-lg">More answers in our Help Center</a>
        </div>
        <div class="col-lg-8 col-md-10 col-sm-24 col-xs-24">
           <div class="win_story">
              <h4>People really win</h4>
              <div class="win_story__body">
                 I played 10 times and won twice! One 0.025btc another 0.0025btc.Wow,I am so lucky!!!
              </div>
              <div class="win_story__user text-center">
                 eud*****
                 <strong>Won 0.02750 BTC</strong>
              </div>
              <a href="en/winners.html" class="btn btn-block btn-outline btn-arrow btn-lg">Read more stories</a>
           </div>
        </div>
     </div>
  </div>
  @include('modals/what_can_be_won')
  @include('layouts/footer')
@endsection
