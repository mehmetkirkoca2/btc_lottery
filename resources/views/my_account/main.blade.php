@extends('layouts/app')
@section('title', 'Ana Sayfa')
@section('pageCss')
  <link rel="stylesheet" href="{!! asset('css/games/io2.css') !!}">
  <link rel="stylesheet" href="{!! asset('css/games/adaptive2.css') !!}">
@endsection
@section('js-bottom')

@endsection
@section('content')
  <div class="settings_section row-eq-height" style="min-height: 723px;">
     <div class="col-lg-16 col-md-16 col-sm-24 col-xs-24 left__content">
        <h3>Change password</h3>
        <form>
           <div class="form-group">
              <label for="old_password">Current password</label>
              <div class="col-sm-18">
                 <input type="password" id="old_password" name="old_password" class="form-control" placeholder="******">
              </div>
           </div>
           <div class="form-group">
              <label for="new_password">New password</label>
              <div class="col-sm-18">
                 <input data-actionkey="check_password" type="password" id="new_password" name="new_password" class="form-control" placeholder="******">
                 <div class="progress">
                    <div class="progress-bar" id="progressbar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                    </div>
                 </div>
                 <p id="ptextinfo"></p>
                 <p>Use at least 8 characters - a combination of letters, numbers and symbols</p>
              </div>
           </div>
           <div class="form-group">
              <label for="confirm_password">Repeat new password</label>
              <div class="col-sm-18">
                 <input type="password" id="confirm_password" name="confirm_password" class="form-control" placeholder="******">
              </div>
           </div>
           <div><button data-action="change_password" class="btn btn-orange btn-lg">Save</button> </div>
        </form>
     </div>
     <div class="col-lg-8 col-md-8 col-sm-24 col-xs-24 right__content">
        <h3>Emails and newsletters</h3>
        <form>
           <div>
              <label>
                 <div class="jq-checkbox styled checked">
                    <input type="checkbox" class="styled" checked="checked" name="email_service" value="1">
                    <div class="jq-checkbox__div"></div>
                 </div>
                 I want to receive service emails
              </label>
           </div>
           <div>
              <label>
                 <div class="jq-checkbox styled checked">
                    <input type="checkbox" class="styled" checked="checked" name="email_newsletter" value="1">
                    <div class="jq-checkbox__div"></div>
                 </div>
                 I want to receive newsletters
              </label>
           </div>
           <input type="hidden" name="action" value="save_email_settings">
           <button type="submit" data-action="save_email_settings" class="btn btn-orange btn-lg">Save</button>
        </form>
     </div>
  </div>
@include('layouts/footer')
@endsection
