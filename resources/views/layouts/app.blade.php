<!DOCTYPE html>
<html lang="en">
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <head>
      <title>Daily Draw</title>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="description" content="">
      <meta property="og:title" content="">
      <meta property="og:description" content="">
      <meta property="og:url" content="">
      <meta name="keywords" content="">
      <meta property="og:image" content="">
      <meta property="og:site_name" content="">
      <meta property="og:type" content="website">
      <meta property="og:keywords" content="">
      <meta name="HandheldFriendly" content="True">
      <meta name="MobileOptimized" content="320">
      <meta http-equiv="cleartype" content="on">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">

      <link rel="stylesheet" href="{!! asset('css/layout.css') !!}">
      @yield('pageCss')

      <!--toastr-->
      <link rel="stylesheet" href="{!! asset('js/plugins/toastr-master/toastr.css') !!}" type="text/css" />
      <link rel="icon" href="favicon.ico" type="image/x-icon">
      <link href="#" rel="apple-touch-icon" sizes="57x57">
      <link href="#" rel="apple-touch-icon" sizes="114x114">
      <link href="#" rel="apple-touch-icon" sizes="72x72">
      <link href="#" rel="apple-touch-icon" sizes="144x144">
      <link href="#" rel="apple-touch-icon" sizes="60x60">
      <link href="#" rel="apple-touch-icon" sizes="120x120">
      <link href="#" rel="apple-touch-icon" sizes="76x76">
      <link href="#" rel="apple-touch-icon" sizes="152x152">
      <link href="#" rel="apple-touch-icon" sizes="180x180">
      <link href="#" rel="icon" sizes="192x192" type="image/png">
      <link href="#" rel="icon" sizes="160x160" type="image/png">
      <link href="#" rel="icon" sizes="96x96" type="image/png">
      <link href="#" rel="icon" sizes="16x16" type="image/png">
      <link href="#" rel="icon" sizes="32x32" type="image/png">
      <meta content="#332f47" name="msapplication-TileColor">
      <meta content="#332f47" name="theme-color">
      <meta content="#332f47" name="msapplication-navbutton-color">
      <meta content="" name="msapplication-TileImage">
      <meta content="Daily Draw" name="application-name">
      <meta content="Daily Draw" name="apple-mobile-web-app-title">
      <link href="{!! asset('css/jquery.formstyler.css') !!}" rel="stylesheet">
      <link href="{!! asset('css/jquery.formstyler.theme.css') !!}" rel="stylesheet">
   </head>
    @yield('head-script-bottom')
   <body class="loading">
      @include('layouts/btc_value_script')
      <div class="layout">
        @include('layouts/btc_value_script')
        @include('layouts/header')
        @include('layouts/left_nav_menu')
        <div class="layout__body">
          @yield('content')
        </div>
      </div>
      @include('layouts/modals')
      @include('layouts/slide_menu')
      <script type="text/javascript" src="{!! asset('js/jquery.min.js') !!}"></script>
      <script type="text/javascript" src="{!! asset('js/bootstrap.min.js') !!}"></script>
      <script type="text/javascript" src="{!! asset('js/plugins/jquery.formstyler.min.js') !!}"></script>
      <script type="text/javascript" src="{!! asset('js/plugins/jquert.mcustomscrollbar.min.js') !!}"></script>
      <script type="text/javascript" src="{!! asset('js/plugins/toastr-master/toastr.js') !!}"></script>
      <script type="text/javascript" src="{!! asset('js/app.js') !!}"></script>
      @yield('js-bottom')
      <script type="text/javascript">
         $(function () { $.ajaxSetup({headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }});});
      </script>
   </body>
</html>
