<aside class="layout__navbar_left js-custom-scroll" role="navbar">
  <ul>
      <li><a href="{{ url('/') }}" class="{{isActiveRouteName('main','active','')}} ico nav-games"><span class="hidden-xs">Game of Daily Draw</span></a></li>
      <li><a href="{{ url('daily-draw') }}" class="{{isActiveRouteName('star','active','')}} ico nav-star"><span class="hidden-xs">Daily Draw</span></a></li>
      {{-- <li><a href="{{ url('moon') }}" class="{{isActiveRouteName('moon','active','')}} ico nav-rapid"><span class="hidden-xs">Rapid to the Moon</span></a></li> --}}
      {{-- <li><a href="{{ url('token') }}" class="{{isActiveRouteName('token','active','')}} ico nav-tfl"><span class="hidden-xs">TFL token</span></a></li> --}}
      <li><a href="{{ url('help-center') }}" class="{{isActiveRouteName('helpCenter','active','')}} ico nav-help"><span class="hidden-xs">Help Center</span></a></li>
  </ul>
</aside>
