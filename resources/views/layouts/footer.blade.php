<footer class="layout__footer">
   <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-24 col-xs-24">
         <p>&copy; Daily Draw Group 2018</p>
         <p class="text-small">Daily Draw and related rights belong to Daily Draw GROUP SOCIEDAD DE RESPONSABILIDAD LIMITADA incorporated in Costa Rica San Jose-Montes de Oca by the digital reg. number 40620000684636. Please note that gambling is permitted under the Costa Rica gaming law.</p>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-24">
         <p class="heading">Daily Draw</p>
         <ul>
            <li><a href="{{url('/')}}terms-of-use">Terms of use</a></li>
            <li><a href="{{url('/')}}privacy-policy">Privacy policy</a></li>
            <li><a href="{{url('/')}}contact-us">Contact us</a></li>
         </ul>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-24">
         <p class="heading">Games</p>
         <ul>
            <li><a href="#">Daily Draw</a></li>
            {{-- <li><a href="#/">Rapid to the Moon</a></li> --}}
         </ul>
      </div>
      <div class="col-lg-8 col-md-8 col-sm-24 col-xs-24">
         <p class="heading">Join us</p>
         <ul class="social-btns">
            <li><a href="#" target="_blank" class="btn"><i class="social social-fb-light"></i></a></li>
            <li><a href="#" target="_blank" class="btn"><i class="social social-telegram-light"></i></a></li>
            <li><a href="#" target="_blank" class="btn"><i class="social social-twitter-light"></i></a></li>
            <li><a href="#" target="_blank" class="btn"><i class="social social-youtube-light"></i></a></li>
            <li><a href="#" target="_blank" class="btn"><i class="social social-steemit-light"></i></a></li>
         </ul>
         <a href="#" class="btn btn-outline-light">Ask a question</a>
      </div>
   </div>
</footer>
