<header class="layout__navbar_top">
   <nav>
      <button class="hamboorger" type="button">
      <i class="line line-1"></i>
      <i class="line line-2"></i>
      <i class="line line-3"></i>
      </button>
      <a href="#" id="logo"><img src="{!! asset('img/logo_subpagesf687.png') !!}" alt="Daily Draw"></a>
      @if( Auth::User() )
        <ul class="user_nav user_nav__left">
           <li>
              <div class="dropdown headernav headernav__balance">
                 <div class="dropdown-toggle" id="dropdown__balance" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <span class="navtitle hidden-sm hidden-xs">{{ getUserBalance() }} BTC</span><span class="hidden-sm hidden-xs">Balance</span>
                    <span class="caret hidden-xs hidden-sm"></span>
                 </div>
                 <ul class="dropdown-menu" aria-labelledby="dropdown__balance">
                    <li class="visible-xs">Balance: <strong>{{ getUserBalance() }} <sup>BTC</sup></strong></li>
                    <li role="separarot" class="divider visible-xs"></li>
                    <li>
                       <p>Refill balance:</p>
                       @php
                          $userBtcAddress=getUserAddress();
                       @endphp
                       <div>BTC: <a href="#" data-cp="{{$userBtcAddress}}">{{$userBtcAddress}}</a></div>
                       <a href="{{url('profile')}}" class="btn btn-block">My transactions history</a>
                    </li>
                    <li>
                       <p>Current balance converted to another currencies:</p>
                       <span class="another_cur">USD: 0.00</span>
                       <span class="another_cur">EUR: 0.00</span>
                    </li>
                 </ul>
              </div>
           </li>
        </ul>
        <ul class="user_nav user_nav__left">
          <li class="hidden-xs">
             <div class="dropdown headernav headernav__mygame">
                <div class="dropdown-toggle">
                   <a href="{{url('play-daily-draw')}}">
                     <span class="navtitle hidden-md hidden-sm hidden-xs">Play Daily Draw</span>
                   </a>
                </div>
             </div>
          </li>
          <li>
            <div class="btnGroupAdress">
              <a href="{{url('refill-balance')}}" class="btn btn-default">Refill Balance</a>
              <a href="{{url('withdrawal')}}" class="btn btn-default">Withdrawal</a>
            </div>
          </li>
        </ul>
      @endif
      <ul class="user_nav user_nav__left">
         {{-- <li>
            <div class="dropdown headernav headernav__languages">
               <div class="dropdown-toggle" id="dropdown__languages" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                  <i class="langico langico-en"></i> <span class="hidden-sm hidden-xs">English</span>
               </div>
               <ul class="dropdown-menu" aria-labelledby="dropdown__languages">
                  <li><a href="#setlang" data-action="setlang" data-descriptor="ru"><i class="langico langico-ru"></i><span class="hidden-sm hidden-xs">Русский</span></a></li>
                  <li><a href="#setlang" data-action="setlang" data-descriptor="en"><i class="langico langico-en"></i><span class="hidden-sm hidden-xs">English</span></a></li>
                  <li><a href="#setlang" data-action="setlang" data-descriptor="es"><i class="langico langico-es"></i><span class="hidden-sm hidden-xs">Español</span></a></li>
               </ul>
            </div>
         </li> --}}
         @if(! Auth::User() )
           <li class="hidden-sm hidden-xs">
              <div class="headernav__social">
                 <ul class="social-btns">
                    <li><a href="#" target="_blank" class="btn"><i class="social social-fb-light"></i></a></li>
                    <li><a href="#" target="_blank" class="btn"><i class="social social-telegram-light"></i></a></li>
                    <li><a href="#" target="_blank" class="btn"><i class="social social-twitter-light"></i></a></li>
                    <li><a href="#" target="_blank" class="btn"><i class="social social-youtube-light"></i></a></li>
                 </ul>
              </div>
           </li>
        @endif
      </ul>
      <ul class="user_nav user_nav__right">
        @if(! Auth::User() )
         <li id="guest">
            <div class="headernav__guest">
               <a href="#signupModal" data-toggle="modal" data-target="#signupModal" data-modal="signupModal">Sign In / Up</a>
            </div>
         </li>
         @else
           <li>
              <div class="dropdown headernav headernav__user">
                 <div class="dropdown-toggle" id="dropdown__user" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <span class="userpic"><img src="{{asset('img/user-avatar-default.svg')}}" alt=""></span>
                    <span class="caret hidden-xs hidden-sm"></span>
                 </div>
                 <ul class="dropdown-menu" aria-labelledby="dropdown__user">
                    <li><a href="{{url('my-account')}}">My Account</a></li>
                    <li><a href="{{url('security-center')}}">Security center</a></li>
                    <li><a href="{{url('my-transactions')}}">My transactions</a></li>
                    {{-- <li><a href="{{url('referral')}}">Referral program</a></li> --}}
                    <li><a href="{{ url('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="navicons navicons-logout"></i>Logout</a></li>
                    <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: one;">
                        {{ csrf_field() }}
                    </form>
                 </ul>
              </div>
           </li>
        @endif
      </ul>
   </nav>
</header>
