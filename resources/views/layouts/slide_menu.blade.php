<div class="toggle-navbar" role="navbar">
   <div class="toggle-navbar__content js-custom-scroll">
      <div class="col-lg-12 col-md-10 col-sm-24 col-xs-24 toggle-navbar__games">
         <div class="toggle-navbar__header">
            <a href="{{url('/')}}" onclick="return false">Games of Daily Draw</a>
            <p>Home page</p>
         </div>
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-24 toggle-navbar__game games_flipsstar">
               <p>Daily Draw</p>
               <div class="jp">
                  <span>Jackpot</span>
                  403.55 BTC
               </div>
               <a href="#signupModal" data-toggle="modal" data-target="#signupModal" class="lbl lbl-warning">Play now <span>Bets from 0.0002 BTC</span></a>
               <ul>
                  <li><a href="#">About the game</a></li>
                  <li><a href="en/how-to-play.html">How to play</a></li>
                  <li><a href="en/archive.html">Draw archive</a></li>
                  <li><a href="en/winners.html">Winners</a></li>
                  <li><a href="en/check-transparency.html">Check the transparency</a></li>
                  <li><a href="https://github.com/Daily Draw" target="_blank">Source code on GitHub</a></li>
               </ul>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-24 toggle-navbar__game games_rapido">
               <p>Rapid to the Moon</p>
               <div class="jp">
                  <span>Super prize</span>
                  14.29 BTC
               </div>
               <a href="#signupModal" class="lbl lbl-warning" data-toggle="modal" data-target="#signupModal">Play now <span>Bets from 0.0001 BTC</span></a>
               <ul>
                  <li><a href="#/">About the game</a></li>
                  <li><a href="#/en/how-to-play">How to play</a></li>
                  <li><a href="#/en/archive">Draw archive</a></li>
                  <li><a href="#/en/winners">Winners</a></li>
                  <li><a href="https://github.com/Daily Draw" target="_blank">Source code on GitHub</a></li>
               </ul>
            </div>
         </div>
      </div>
      <div class="col-lg-12 col-md-14 col-sm-24 col-xs-24 toggle-navbar__additional">
         <div class="row">
            {{-- <div class="col-lg-8 col-md-8 col-sm-8 col-xs-24">
               <p>TFL token</p>
               <ul>
                  <li><a href="https://tfl.Daily Draw/">Dashboard</a></li>
                  <li><a href="https://tfl.Daily Draw/">Park tokens</a></li>
               </ul>
            </div> --}}
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-24">
               <p>Help Center</p>
               <ul>
                  <li><a href="https://help Draw/#/cat/7">Daily Draw</a></li>
                  <li><a href="https://help Draw/">Rapid to the Moon</a></li>
                  <li><a href="hhttps://help Draw/#/cat/15">Referral System</a></li>
                  <li><a href="hhttps://help Draw/#/cat/17">Tokens</a></li>
               </ul>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-24 col-xs-24">
               <p>Daily Draw</p>
               <ul>
                  <li><a href="{{url('/')}}terms-of-use">Terms of use</a></li>
                  <li><a href="{{url('/')}}privacy-policy">Privacy policy</a></li>
                  <li><a href="{{url('/')}}contact-us">Contact us</a></li>
               </ul>
            </div>
            <div class="col-lg-16 col-md-16 col-sm-24 col-xs-24">
               <p>Join us</p>
               <ul class="social-btns">
                  <li><a href="#" target="_blank" class="btn"><i class="social social-fb-light"></i></a></li>
                  <li><a href="#" target="_blank" class="btn"><i class="social social-telegram-light"></i></a></li>
                  <li><a href="#" target="_blank" class="btn"><i class="social social-twitter-light"></i></a></li>
                  <li><a href="#" target="_blank" class="btn"><i class="social social-youtube-light"></i></a></li>
                  <li><a href="#" target="_blank" class="btn"><i class="social social-steemit-light"></i></a></li>
               </ul>
               <a href="https://help Draw/" class="btn btn-outline">Ask a question</a>
            </div>
         </div>
      </div>
   </div>
</div>
