<!-- Modal Sign Up -->
<div class="modal fade" id="signupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Create your account / <a href="#signinModal" data-dismiss="modal" aria-label="Close"  data-toggle="modal" data-target="#signinModal">Sign In</a></h4>
         </div>
         <div class="modal-body">
            <form id="formRegister" class="form" action="{{ url('register') }}" method="POST">
              {{ csrf_field() }}
              <div class="alert alert-info" id="divRegisterMessage" style="display:none;line-height: 19px;"></div>
               <div class="form-group">
                  <input class="form-control" name="email" type="email" placeholder="Email" required>
               </div>
               <div class="form-group">
                  <input class="form-control" name="password" type="password" placeholder="Password" required>
               </div>
               <div style="padding-bottom:15px">
                 <input type="checkbox" name="age18" value="1" style="-webkit-appearance: checkbox;" required>
                  Im 18+ years old
               </div>
               <div style="padding-bottom:15px">
                 <input type="checkbox" name="iagree" value="1" style="-webkit-appearance: checkbox;" required>
                  I agree with <a href="{{url('/terms-of-use')}}" target="_blank">Terms of use</a>
                  and <a href="{{url('/privacy-policy')}}"  target="_blank">Privacy policy</a>
               </div>
               {!! Recaptcha::render() !!}
               <button type="button" class="btn btn-orange btn-strong btn-shadow btn-block btn-lg" id="btnSignUp">Sign Up</button>
            </form>
            <p class="text-center">
               <a href="#forgotModal" data-dismiss="modal" aria-label="Close"  data-toggle="modal" data-target="#forgotModal">Forgot your password?</a>
            </p>
         </div>
         <div class="bg-gray modal-prefooter" id="el_socialup">
            <div class="box-soc-autorisation">
               <p class="text-center">Log in with a social network account</p>
               <ul class="social-btns text-center">
                  <li><a href="#gp" class="btn" data-uloginbutton="googleplus"><i class="social social-gp-light"></i></a></li>
                  <li><a href="#fb" class="btn" data-uloginbutton="facebook"><i class="social social-fb-light"></i></a></li>
                  <li><a href="#tw" class="btn" data-uloginbutton="twitter"><i class="social social-twitter-light"></i></a></li>
               </ul>
            </div>
         </div>
         <!--div class="modal-footer text-center">
            </div-->
      </div>
   </div>
</div>
<!-- Modal Authenticate -->
<div class="modal fade" id="signinModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Sign In</h4>
         </div>
         <div class="modal-body">
            <form class="form" action="{{ url('login') }}" method="POST" id="formLogin">
              {{ csrf_field() }}
              @if (session()->has('errorLogin'))
                <div class="alert alert-danger">
                  <strong>Error !</strong> {!! session('errorLogin') !!}
                </div>
                <script type="text/javascript">
                  $(function(){
                    $('#signinModal').modal('show');
                  });
                </script>
              @endif
               <div class="form-group">
                  <input class="form-control" name="email" type="email" placeholder="Email" required>
               </div>
               <div class="form-group">
                  <input class="form-control" name="password" type="password" placeholder="Password" required data-submit="signin_user">
               </div>
               <div style="padding-bottom:15px">
                  <input type="checkbox" class="styled" name="i18yo" checked> Im 18+ years old
               </div>
               {!! Recaptcha::render() !!}
               <button type="submit" class="btn btn-orange btn-strong btn-shadow btn-block btn-lg">Sign In</button>
            </form>
            <p class="text-center">
               <a href="#forgotModal" data-dismiss="modal" aria-label="Close"  data-toggle="modal" data-target="#forgotModal">Forgot your password?</a>
            </p>
         </div>
         <div class="bg-gray modal-prefooter" id="el_social">
            <div class="box-soc-autorisation">
               <p class="text-center">Log in with a social network account</p>
               <ul class="social-btns text-center">
                  <li><a href="#gp" class="btn" data-uloginbutton="googleplus"><i class="social social-gp-light"></i></a></li>
                  <li><a href="#fb" class="btn" data-uloginbutton="facebook"><i class="social social-fb-light"></i></a></li>
                  <li><a href="#tw" class="btn" data-uloginbutton="twitter"><i class="social social-twitter-light"></i></a></li>
               </ul>
            </div>
         </div>
         <div class="modal-footer text-center">
            Not registered yet? <a href="#signupModal" data-dismiss="modal" aria-label="Close"  data-toggle="modal" data-target="#signupModal">Sign Up</a>
         </div>
      </div>
   </div>
</div>

<!-- Modal Forgot -->
<div class="modal fade" id="forgotModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Reset your password</h4>
         </div>
         <div class="modal-body">
            <form class="form" action="#" method="POST" onsubmit="return false;">
               <div class="form-group">
                  <input class="form-control" name="email" type="email" data-submit="resetpsw_user" placeholder="Email">
               </div>
               <button type="button" class="btn btn-orange btn-strong btn-shadow btn-block btn-lg">Reset password</button>
            </form>
         </div>
         <div class="modal-footer text-center">
            <a href="#signinModal" data-dismiss="modal" aria-label="Close"  data-toggle="modal" data-target="#signinModal">Sign In</a>
         </div>
      </div>
   </div>
</div>

<!-- Modal VIDEO -->
<div class="modal fade" id="fairvideoModal" tabindex="-1" role="dialog" aria-labelledby="...">
   <div class="modal-dialog" role="document">
      <div class="modal-content modal-md">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">How to check the results? </h4>
         </div>
         <iframe width="100%" height="315" src="" frameborder="0" allowfullscreen></iframe>
      </div>
   </div>
</div>

<!-- Modal What is it? -->
<div class="modal fade" id="fundsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title" id="myModalLabel">Token holder's fund</h4>
         </div>
         <div class="modal-body">
            <p>Token holder's fund</p>
            <p><strong>Current fund is formed from 01 August, 2017</strong></p>
         </div>
      </div>
   </div>
</div>
<!-- Modal park -->
<div class="modal fade" id="parkModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title" id="myModalLabel">Token holder information</h4>
         </div>
         <div class="modal-body">
            <p>To park your tokens, use the address below:</p>
            <p><strong></strong></p>
            <button data-cp="" class="btn btn-lg btn-outline btn-block">Copy to clipboard</button>
            <div class="alert alert-danger">
               Depositing tokens to this address other than TFL may result all your funds being lost.
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Modal withdraw -->
<div class="modal fade " id="modalTerms" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content ">
         <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title" id="myModalLabel">Terms and Conditions</h4>
         </div>
         <div class="modal-body">
            <div class="agreement_text js-custom-scroll mCustomScrollbar _mCS_3 mCS_no_scrollbar">
               <div id="mCSB_3" class="mCustomScrollBox mCS-orange mCSB_vertical mCSB_inside" tabindex="0" style="max-height: 300px;">
                  <div id="mCSB_3_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y" style="position:relative; top:0; left:0;" dir="ltr">
                     <div class="terms_text">
                        <h2>Terms of Use and Risk Notifications of the Daily Draw Platform </h2>
                        <p><strong>General</strong></p>
                        <p>1. These Terms and Conditions ("T&amp;Cs" or "Agreement") constitute and govern the contractual relationship between the parties, Blockchain Games N.V. hereinafter referred to as "Daily Draw" or "We", "Us"; or "Our"; and You, as the user and customer, hereinafter referred to as "User" or "You" or "Yours".</p>
                        <p>2. The Terms and Conditions stipulated below are effective as of December, 2017. Before using the website <a href="{{url('/')}}">{{url('/')}}</a>, please read Terms and Conditions carefully. The fact of using the website confirms your consent with the Terms and Conditions.</p>
                        <p>3. The web-site <a href="{{url('/')}}">{{url('/')}}</a> is licensed in Curacao and held by Blockchain Games N.V. a company incorporated under the laws of Curacao.</p>
                        <p>4. The User must read, agree with and accept all of the Terms and Conditions contained in this Agreement without modifications, which include those Terms and Conditions expressly set forth below and those incorporated by reference, before the User may become an active, registered User of <a href="{{url('/')}}">{{url('/')}}</a>. By continuing to access or use the Website, the User agrees to follow the Terms and Conditions of this Agreement. This Agreement shall also apply to all Services provided via mobile devices, including downloadable applications. All references to the use of the Website shall be construed as references to the use of our Services provided for mobile devices.</p>
                        <p>5. Blockchain Games N.V. may amend and/or assign this Agreement or any rights and/or obligations from time to time. The Terms and Conditions and the Privacy Policy are published on the Website and may be changed at any time. The new version of these Terms and Conditions will take effect immediately upon the next visit or login on the Website. By continuing to use the Website, the Services or the software offered by Blockchain Games N.V., the User agrees to be bound by the T&amp;Cs as well as by the latest modifications to it. If the User does not agree to be bound by the changes to these T&amp;C's, the User must not use or access Our Services, and inform Us in writing immediately. In order to avoid misunderstandings and disputes at a later stage, Users can print out these T&amp;C's. Blockchain Games N.V. may publish these General Terms and Conditions in a number of languages and shall reflect the same principles. This is done for information purposes and to help Users. It is however the English version that constitutes the legal basis of the relationship between You and Blockchain Games N.V. Should there be any discrepancy between the T&amp;Cs in the English version and the version in any other language, the English version shall prevail.</p>
                        <p>6. These General Terms and Conditions enter into force when you register and confirm Your registration details in the registration process on the Website. By registering an account with the Website You agree that you have read and understood these Terms and Conditions. By using the Website you signify that you have accepted and agreed to the contents contained therein. You must read and understand these Terms and Conditions fully before registering an account with Us. Should You not agree with any part of this Agreement, You must not use or continue to use the Website.</p>
                        <p>7. Blockchain Games N.V. reserves the right to refuse and/or cancel Services at its own discretion where the Services are illegal to use.</p>
                        <p><strong>Eligible users</strong></p>
                        <p>8. The website accepts Users only from those countries and geographic regions where online gambling is allowed by law. It is the User’s sole responsibility to inquire about the existing gambling laws and regulations of the given jurisdiction before placing bets on the website.</p>
                        <p>9. The User guarantees at all times not to be a resident of countries including the United States and its dependencies, and territories including but not limited to American Samoa, Guam, Marshall Islands, North Mariana Islands, Puerto Rico, and the Virgin Islands, Cuba, Iran, China, Hong Kong, Macau, Sudan, North Korea, United Kingdom, Syria, France or Spain.</p>
                        <p>10. The User assures Blockchain Games N.V. that he/she is over 18 years of age. No person under 18 years of age or other higher minimum legal age in User's jurisdiction may be registered as a User and any funds deposited or any money won by any such person shall be forfeited. We are entitled to require User to provide proof of age and to refuse a User's admission to the Service if it has reasons to believe that the minimum age requirement is not fulfilled.</p>
                        <p>11. The services are available to, and may only be used by individuals, who can enter into legally binding contracts under the law applicable to their country of domicile.</p>
                        <p><strong>Registration and account</strong></p>
                        <p>12. In order to use the Website, the User must complete the registration form and open a User account ("Account") on the Website. The User must fill in the registration form provided by Daily Draw which shall at least include the following details:
                           <br>a) The User's identity – The User warrants to, provide true, accurate, current and complete information regarding identity during the registration process. Any false information or impersonation of any person or entity, misrepresentation regarding any affiliation with another person, entity or association, use of false headers or other acts or omissions to conceal one's identity from Blockchain Games N.V. for any purpose will be prosecuted under the fullest extent of the law. A User accessing the Website may be asked to provide valid subsisting verification of identity and relevant information. The Company reserves the right to ask for the proof of age from the user and limit access to the website or suspend the user’s account in cs fail to meet this requirement.
                           <br>b) Date of Birth.
                           <br>c) The User's place of residence –Please note that the User may be asked to prove his/her place of residence by providing proof of identity such as copy of identity card, passport or driving license and/or utility bill;
                           <br>d) The User's valid e-mail address and if available, contact number (phone / mobile)
                           <br>e) Username – The username must be unique and clearly identifiable, personal and confidential. Offensive or indecent names are not allowed. Further prohibited are usernames that contain or allude to an Internet link or account names, which allude to certain rights of the User. We reserve the right to suspend accounts with an unacceptable username. Accounts can be reactivated as soon as suitable and an acceptable alternative username has been chosen.
                           <br>f) Password - The User is advised to choose a strong and non-predictable password for security reasons and is responsible for ensuring that this password is kept as highly confidential. In the event that there is concern that the secrecy of Username and Password is no longer the case, the User should notify Blockchain Games N.V. immediately.
                        </p>
                        <p>13. During and after the registration process the User may be required to provide Daily Draw with valid identification, including but not limited to the address, contact email and/or personal telephone number. In the event that Daily Draw has reason to believe that the information given is inaccurate, Daily Draw is entitled to suspend or terminate User's account and retain any winnings.</p>
                        <p>14. With the use of an Account on <a href="{{url('/')}}">{{url('/')}}</a> Website the User ensures at least the following:
                           <br>a) The User has completed the registration form truthfully and correctly.
                           <br>b) The User is not an individual under 18 years of age or other higher minimum legal age in User's jurisdiction.
                           <br>c) The User is neither under legal supervision nor restricted in his business activities.
                           <br>d) The User has registered personally on his/her behalf and not on the behalf of someone else.
                           <br>e) The User uses the account for personal use and has no commercial intentions.
                           <br>f) The User has no knowledge about any bet result of the underlying bet before placing it.
                           <br>g) The User does not have multiple accounts on <a href="{{url('/')}}">{{url('/')}}</a> Website.
                           <br>h) The User has read and accepts these General Terms and Conditions.
                        </p>
                        <p>15. We accept only one account per person. The use of more than one account per physical User is strictly forbidden. We retain the right to close a User's account at any time and to cancel all the transactions pertaining to any User that has registered more than one account in his own name or under different names, in the event that We have reasonable suspicion that the User has registered multiple accounts. It makes no difference whether the accounts have been used with the intent of defrauding or cheating, or otherwise.</p>
                        <p>16. It is prohibited for Users to sell, pledge, transfer and/or acquire accounts to/from other Users. Funds can only be remitted to the same account from where they originated at the deposit stage.</p>
                        <p>17. Blockchain Games N.V. accepts only natural persons with a single account as a User. Neither a legal entity nor corporate body is allowed to open or to have an account on <a href="{{url('/')}}">{{url('/')}}</a> Website.</p>
                        <p>18. We will treat as highly confidential the information the User entrusts to us, in accordance with the disclosures we provide during the registration process and in accordance with our Privacy Policy.</p>
                        <p>19. The User shall not treat Blockchain Games N.V. as a financial institution nor expect interest on his/her deposit/s.</p>
                        <p>20. Any amount credited erroneously to a User's Account should be reported immediately to Blockchain Games N.V. by the User. Any winnings and funds caused by an error are invalid regardless of how they occurred.</p>
                        <p>21. In the event of misconduct by a User, Blockchain Games N.V. is entitled to immediately terminate the Agreement with the User and exclude him from further use of the Website. Further, we are entitled to interrupt and terminate on-going activities, to block the User or the User's Account with or without prior notice and to retain the User's credit and winnings achieved through misconduct until clarification of the situation.</p>
                        <p>22. Any indication of fraud, manipulation, cash-back arbitrage, or other forms of deceitful or fraudulent activity which was the basis of a provision of a bonus or otherwise will render the Account inactive along with any and all profits or losses gathered.</p>
                        <p><strong>Fees and Taxes</strong></p>
                        <p>23. The User is fully responsible for paying all fees and taxes applied to their winnings according to the laws of the jurisdiction of User’s residence.</p>
                        <p>24. In countries where the report of winnings and losses is legally prescribed by legal authority or financial institution, the User is responsible for reporting his/her winnings and losses such authorities.</p>
                        <p><strong>Game Rules and conditions specific to lottery games</strong></p>
                        <p>25. In order to place a bet with an account on <a href="{{url('/')}}">{{url('/')}}</a>, the User must to purchase Tickets - record of participation in the System with a range of functionalities programmed via blockchain technologies which nobody controls and therefore everyone can trust.</p>
                        <p>26. The User confirms that he/ she knows and understands the rules of games offered by the <a href="{{url('/')}}">{{url('/')}}</a> Website. It is at User’s discretion to know the payout percentage of each game.</p>
                        <p>27. You hereby agree that Blockchain Games N.V. as the operator of <a href="{{url('/')}}">{{url('/')}}</a> is placing Game Bets on your behalf.</p>
                        <p>28. The User affects an instruction to the Blockchain Games N.V. as the operator of <a href="{{url('/')}}">{{url('/')}}</a> and bookmaker to place a bet on the outcome of a Daily Draws games event with a determinable outcome (hereinafter "Game Bet") on their behalf by filling out and submitting the electronic ticket displayed on the Website the compilation of which allows User to place bets on a of Daily Draws games of his choice on the <a href="{{url('/')}}">{{url('/')}}</a> Website. To settle the Game Bet amount, the User may either use funds available in his User Account or pay for the Game Bet/s by using alternative payment method.</p>
                        <p>29. Once any Game Bet has been submitted, it cannot be altered or cancelled.</p>
                        <p>30. Winnings on any Game Bet shall be credited to the User Account and the User is able to view any such Winnings on their User Account on the Website. In addition, the Blockchain Games N.V. may decide, but is not obliged, to inform the User with respect to Winnings by email, SMS/text message or other ways of communication.</p>
                        <p>31. Following the creation of Ticket by the Service, the Tickets will be used to User`s participation in the Daily Draw's games. The User understands that the User must keep his password safe and that the User may not share them with anybody.</p>
                        <p>32. Every payment for purchasing ticket in Flip Star game is distributed by the System according to the following proportion: 60 % of the ticket cost goes to the Jackpot fund; remaining 40% go to promotion (15-20%), prize fund for TFL Token holders game (10-15%) and management fee and referral rewards (5-10%).</p>
                        <p>33. Every day the Service raffles a draw among Users that bought tickets in this day. A Ticket wins when numbers of the ticket match numbers of the winning set according to the table set forth. Winning set for free tickets specified with table set forth below.</p>
                        <table class="table prize_table__fs">
                           <thead>
                              <tr>
                                 <th>Match</th>
                                 <th>Prize</th>
                                 <th>Probability</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <i class="balls-icon balls-icon__orange"></i>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon"></i>
                                 </td>
                                 <td>Jackpot</td>
                                 <td>1: 49 504 950</td>
                              </tr>
                              <tr>
                                 <td>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon"></i>
                                 </td>
                                 <td>8.0000 BTC</td>
                                 <td>1: 1 984 127</td>
                              </tr>
                              <tr>
                                 <td>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon balls-icon__orange"></i>
                                 </td>
                                 <td>2.0000 BTC</td>
                                 <td>1: 225 225</td>
                              </tr>
                              <tr>
                                 <td>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon"></i>
                                 </td>
                                 <td>0.0400 BTC</td>
                                 <td>1: 9 014</td>
                              </tr>
                              <tr>
                                 <td>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon balls-icon__orange"></i>
                                 </td>
                                 <td>0.0100 BTC</td>
                                 <td>1: 5 241</td>
                              </tr>
                              <tr>
                                 <td>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon balls-icon__orange"></i>
                                 </td>
                                 <td>0.0040 BTC</td>
                                 <td>1: 374</td>
                              </tr>
                              <tr>
                                 <td>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon"></i>
                                 </td>
                                 <td>0.0020 BTC</td>
                                 <td>1: 210</td>
                              </tr>
                              <tr>
                                 <td>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon balls-icon__orange"></i>
                                 </td>
                                 <td>0.0010 BTC</td>
                                 <td>1: 73</td>
                              </tr>
                              <tr>
                                 <td>
                                    <i class="balls-icon balls-icon__orange"></i>
                                 </td>
                                 <td>0.0006 BTC</td>
                                 <td>1: 46</td>
                              </tr>
                              <tr>
                                 <td>
                                    <i class="balls-icon"></i>
                                    <i class="balls-icon"></i>
                                 </td>
                                 <td>0.0002 BTC</td>
                                 <td>1: 15</td>
                              </tr>
                           </tbody>
                        </table>
                        <p>34. The process of determination of the winning is published on GitHub which is a development platform that provides hosting and reviewing code, managing projects, and building software alongside millions of other developers. The Daily Draw account is available at <a href="https://github.com/Daily Draw">https://github.com/Daily Draw</a></p>
                        <p><strong>Disclaimer of Liability</strong></p>
                        <p>35. The User is aware of the fact that gambling at the website may lead to losing money. The Company is not liable for any possible financial damage arising from the use of the website</p>
                        <p>36. The Company is taking effective measures to protect User’s private data from any unauthorized use and is only making it available to parties involved in providing of gambling services through the website. Notwithstanding this, the Company is not responsible for how the information is further treated by third parties, for example third party software providers or affiliates. Treatment of User’s private data by such parties is subject to terms and conditions of these parties, if any.</p>
                        <p>37. Blockchain Games N.V. is in no way responsible for any access to a User's account by a third person and will not be held responsible for any loss suffered due to the illicit use of a User's password by a third person, of unauthorized access, and/or for any transaction in which the name and password of a User was registered correctly.</p>
                        <p>38. The Company is not liable of any hardware or software, defects, unstable or lost Internet connections, or any other technical errors that may limit User’s access to the website or prevent User from an uninterrupted play.</p>
                        <p>39. Some circumstances may arise where a wager is confirmed, or a payment is performed, by us in error. In all these cases the Blockchain Games N.V. reserves the right to cancel all the wagers accepted containing such an error, or to correct the mistake made re-settling all the wagers at the correct prices/spreads/terms that should have been available at the time that the wager was placed in the absence of the error.</p>
                        <p>40. If we mistakenly credit your Account with winnings that do not belong to you, whether due to a technical, error in the pay-tables, or human error or otherwise, the amount will remain our property and will be deducted from your Account. If you have withdrawn funds that do not belong to you prior to us becoming aware of the error, the mistakenly paid amount will (without prejudice to other remedies and actions that may be available at law) constitute a debt owed by you to us. In the event of an incorrect crediting, you are obliged to notify us immediately by email.</p>
                        <p>41. The Blockchain Games N.V., its directors, employees, partners, service providers:
                           <br>a) do not warrant that the software or the Website is/are fit for their purpose;
                           <br>b) do not warrant that the software and Website are free from errors;
                           <br>c) do not warrant that the Website and/or Games will be accessible without interruptions;
                           <br>d) shall not be liable for any loss, costs, expenses or damages, whether direct, indirect, special, consequential, incidental or otherwise, arising in relation to Your use of the Website or Your participation in the Games.
                        </p>
                        <p>42. You hereby agree to fully indemnify and hold harmless the Blockchain Games N.V., its directors, employees, partners, and service providers for any cost, expense, loss, damages, claims and liabilities howsoever caused that may arise in relation to your use of the Website or participation in the Games.</p>
                        <p>43. You acknowledge that the Blockchain Games N.V. shall be the final decision-maker of whether you have violated the Blockchain Games N.V.’s rules, terms or conditions in a manner that results in your suspension or permanent barring from participation in our site.</p>
                        <p>44. The website can only be used for personal purposes and shall not be used for any type of commercial profit.</p>
                        <p><strong>Use of User’s Account</strong></p>
                        <p>45. Each User can create only one personal user account. Creating multiple user accounts by a User can lead to termination of the accounts. The User shall not provide access to his user account or allow using the website to any third party including but not limited to minors.</p>
                        <p>46. Also any returns, winnings or bonuses which you have gained or accrued during such time as the Duplicate Account was active will be forfeited by you and may be reclaimed by us, and you will return to us on demand any such funds which have been withdrawn from the Duplicate Account.</p>
                        <p>47. The website can only be used for personal purposes and shall not be used for any type of commercial profit.</p>
                        <p><strong>Responsible Gaming&nbsp;</strong></p>
                        <p>48. Daily Draw wants to ensure that its Users gamble in a responsible manner, but We also acknowledge that gambling can be addictive to some and affect their lives negatively. Since We care for Our Users, and want online gaming to be an enjoyable experience, We offer various measures which can be taken by a User to gamble in a responsible manner.</p>
                        <p>49. For those customers who wish to restrict their gambling, we provide voluntary self-exclusion tools, which enable you to close your account or restrict your ability to place bets. If you require any information relating to this facility please contact Customer service support@Daily Draw. We will endeavor to implement your request within a reasonable time period and to ensure compliance with your self-exclusion. However you accept that we have no responsibility or liability whatsoever if you continue gambling before your request has been implemented or you seek to use the Website and we fail to recognize or determine that. You acknowledge that exclusion and limits are set per account, and should you have accounts on more than one site owned by us, you must set the limits on each account.</p>
                        <p>50. You may contact any of the following organizations for consultation and support if you think that you start spending more money than you can afford, or in case gaming starts interfering with your normal daily routines: http://www.gamblersanonymous.org, http://www.gamcare.org.uk, https://www.gamblingtherapy.org.</p>
                        <p><strong>Anti-Fraud Policy</strong></p>
                        <p>51. The Company has a strict anti-fraud policy. If the User is suspected of fraudulent actions including, but not limited to:
                           <br>a) participating in any type of collusion with other Users;
                           <br>b) development of strategies aimed at gaining of unfair winnings;
                           <br>c) fraudulent actions against other online Blockchain Games N.V.s or payment providers;
                           <br>d) charge back transactions;
                           <br>e) creating two or more accounts;
                           <br>f) other types of cheating;
                           <br>g) or become a bankrupt in the country of his/her residence,
                           <br>the Company reserves the right to terminate the user account and suspend all payouts to the User. This decision is at sole discretion of the Company and the User will not be notified or informed about the reasons of such actions. The Company also reserves the right to inform the regulatory bodies of such fraudulent actions performed by the User.
                        </p>
                        <p><strong>Advantage play zero tolerance</strong></p>
                        <p>52. Blockchain Games N.V. have zero tolerance to advantage play. Customer, who will try to gain advantage of Blockchain Games N.V. welcome offers / other promotions agrees that Blockchain Games N.V. reserve the right to void bonuses and any winnings from such bonuses from customer:
                           <br>a) use of stolen cards or cryptocurrency wallets;
                           <br>b) chargebacks;
                           <br>c) creating more than one account in order to get advantage from Blockchain Games N.V. promotions;
                           <br>d) providing incorrect registration data;
                           <br>e) any other actions which may damage the Blockchain Games N.V..
                        </p>
                        <p>53. The Blockchain Games N.V. reserves the right to close Your Account and to refund to You the account balance, subject to the deduction of relevant withdrawal charges, at Blockchain Games N.V.’s absolute discretion and without any obligation to state a reason or give prior notice.</p>
                        <p>54. The Blockchain Games N.V. reserves the right to retain payments, if suspicion or evidence exists of manipulation of the Blockchain Games N.V. system. Criminal charges will be brought against any user or any other person(s), who has/have manipulated the Blockchain Games N.V. system or attempted to do so. The Blockchain Games N.V. reserves the right to terminate and/or, change any games or events being offered on the Website.</p>
                        <p>55. Should the user become aware of possible errors or incompleteness in the software, he/she agrees to refrain from taking advantage of them. Moreover, the user agrees to report any error or incompleteness immediately to the Blockchain Games N.V.. Should the user fail to fulfil the obligations stated in this clause, the Blockchain Games N.V. has a right to full compensation for all costs related to the error or incompleteness, including any costs incurred in association with the respective error/incompleteness and the failed notification by the user.</p>
                        <p>56. Also any returns, deposits, winnings or bonuses which you have gained or accrued during such time as the Duplicate Account was active will be forfeited by you and may be reclaimed by us, and you will return to us on demand any such funds which have been withdrawn from the Duplicate Account.</p>
                        <p><strong>Depositing</strong></p>
                        <p>57. Daily Draw offers a variety of payment methods. They include Bitcoin (BTC), Litecoin (LTC), Ethereum (ETH) and other supported cryptocurrencies. Please contact our support team at support@Daily Draw to inquire about the payment methods which are most favorable for your country of residence.</p>
                        <p>58. Please note that the minimal amount of deposit is $50 USD in BTC/ETH or any other supported cryptocurrency (or the equivalent in another currency on the Daily Draw Platform).</p>
                        <p><strong>Withdrawal/Refund Policy</strong></p>
                        <p>59. To withdraw funds, we require complete verification of your account with all the docs needed. The minimal amount for withdrawal is equivalent to $50 USD in BTC, ETH, LTC or any other supported cryptocurrency. The maximum amount for withdrawal depends on the payment method you decide to use. If the requested amount of withdrawal exceeds the limit of a particular payment system, the amount will be withdrawn in installments.</p>
                        <p>60. There is a 2% fee for withdrawals that is due to blockchain fees and transaction fees inside Daily Draw platform. </p>
                        <p>61. The Company reserves the right to check User’s identity prior to processing payouts and to hold withdrawals for the time needed to check the User’s identity. In case of false personal data provided by the Users, the withdrawal can be refused and the user account can be terminated. The User will be informed thereof by email.</p>
                        <p>62. The website supports payouts in BTC, ETH, LTC and other supported cryptocurrencies.</p>
                        <p>63. Please mind that the internal operating currency of the website is TFL Token. </p>
                        <p>64. Cryptocurrency withdrawals will be made to Your cryptocurrency wallet submitted at your account. To withdraw any cryptocurrency assets which have been deposited, we require the blockchain to give at minimum 3 confirmation of your last deposit, before a withdrawal can be made.</p>
                        <p>65. Users using no deposit bonuses for the game cannot participate in promotions and Blockchain Games N.V. tournaments.</p>
                        <p><strong>Dormant Accounts</strong></p>
                        <p>66. A Dormant Account is a User Account which a User has not logged into or logged out of for twelve consecutive months. If your User Account is deemed to be inactive, the Blockchain Games N.V. reserves the right to charge a monthly administrative fee equivalent to in cryptocurrency or the equivalent in another currency (or the current balance of your User Account, if less) as long as the balance of your User Account remains positive.</p>
                        <p>67. You authorize the Blockchain Games N.V. to debit this fee from your User Account on the beginning of the month following the day on which your User Account is deemed inactive, and on the beginning of every subsequent month that your User Account remains inactive. The Blockchain Games N.V. will stop deducting the fee if the account balance is zero or if the account is re-activated.</p>
                        <p><strong>Referral program</strong></p>
                        <p>68. For the purpose of attracting new Users the Service introduces Referral program (hereinafter the "Program") called "Friend`s winnings". The Program provides Users with the opportunity to refer other individuals to join the Daily Draw Service as new Users.</p>
                        <p>69. To be eligible to participate in the Referral Program as a referrer, a User must be registered on the Daily Draw Website and confirm his email. Under the Program, the System provides a User with a unique referral link that can be distributed to other individuals to become Referred Users. Referrers are provided with a personal "Refer-a-Friend" page ({{url('/')}}profile/referral) to check the status of his/her Referrals and to manage his/her rewards.</p>
                        <p>70. In order for a new User to be registered as a Referred player in the System, he must visit the Website using a referral link and create an account in the next 72 hours. Cookies must be enabled on the computer of a new User.</p>
                        <p>71. After a Referred User creates an account on the Website and confirms his email, the Referred User get a free ticket. Rules of using free tickets are set forth on the Website. The Referrer will receive 2% of the Referred Users’ winnings on an ongoing basis, including Jackpot&#8203;. Also Referrer will receive 3-7 % of tickets bought by all his/her Referral network. The percentage of bonus tickets received by Referrer is depend on amount of tickets bought by all his/her Referral network.</p>
                        <p>72. All Referral Program remunerations are added automatically to the User’s account and can be used at his discretion in accordance with present Terms and Conditions.</p>
                        <p>73. When distributing, promoting or communicating their Referral Code(s) the Registered Users must agree to the following restrictions:
                           <br>a) No spamming. The Registered Users agree that they will not "spam" anyone with invitations to join the Daily Draw System, and that at all timesthey will remain compliant with all applicable laws. The following specific activities are prohibited:
                           <br>&nbsp;a. Mass emailing, texting or messaging people they do not personally know;
                           <br>&nbsp;b. Use of automated systems or bots through any channel to distribute, post or respond to their Referral Code;
                           <br>&nbsp;c. Use of scripts, programed or automatic dialers to send invites or to communicate Referral Codes;
                           <br>&nbsp;d. Posting referral codes on event or venue pages without express permission from the owner.
                           <br>b) No misrepresentations.The Registered Users agree they you will not attempt to mislead anyone in connection with the Referral Program, either by affirmative representation, implication, or omission. In particular, they agree that they will not:
                           <br>&nbsp;a. Impersonate any person or entity;
                           <br>&nbsp;b. Create fake accounts, blogs, webpages, profiles, websites, links or messages;
                           <br>&nbsp;c. Misrepresent their relationship with Daily Draw or any other third party;
                           <br>&nbsp;d. Suggest that an affiliation or partnership exists with a third party where none exists. Make misrepresentations with respect to the characteristics or qualification requirements for any referral rewards.
                           <br>c) No prohibited content. The Registered Users agree that they will not use the Daily Draw brand in connection with:
                           <br>&nbsp;a. Disparaging or defamatory content concerning Daily Draw or third parties;
                           <br>&nbsp;b. Content which promotes racism, bigotry, hatred, discrimination or physical harm of any kind against any group or individual;
                           <br>&nbsp;c. Offensive, abusive, intimidating or harassing content;
                           <br>&nbsp;d. Content that is sexually explicit, obscene and/or contains nudity;
                           <br>&nbsp;e. Any political and/or religious statement;
                           <br>&nbsp;f. Content containing images or reference to drugs, alcohol, tobacco, weapons or firearms; Content that violates someone else’s privacy.
                           <br>d) Other restrictions. The Registered Users also agree that they will not:
                           <br>&nbsp;a. Open accounts with their affiliate links and with a single IP address;
                           <br>&nbsp;b. Create websites, domains, URLs, social media handles or email addresses containing the word "Daily Draw";
                           <br>&nbsp;c. Engage in fraudulent activity. The Registered Users agree that they and their referrals will not defraud or abuse (or attempt to defraud or abuse) Daily Draw, the terms of the Daily Draw Referral Program, or any invited users.
                        </p>
                        <p>74. Daily Draw System reserves the right to disqualify any Player and/or cancel any Reward(s) it finds to be violating these Terms and Conditions. Referrals generated by a script, macro or other automated means will be disqualified.</p>
                        <p><strong>Complaints</strong></p>
                        <p>75. You may contact our support@Daily Draw customer service according to the instructions located at the Website to give us any complaints regarding our services.</p>
                        <p>76. Complaints are handled in the support department and escalated in the organization of the Blockchain Games N.V. in the case that support personnel do not solve the case immediately. The User shall be informed about the state of the complaint to a reasonable level.</p>
                        <p>77. If the dispute is not resolved on the Blockchain Games N.V. management level, you can contact any independent body, gaming authority or licensor listed on the Blockchain Games N.V. website.</p>
                        <p>78. In the event of any dispute, you agree that the records of the server and blockchain shall act as the final authority in determining the outcome of any claim.</p>
                        <p>79. You agree that in the unlikely event of a disagreement between the result that appears on your screen and the game server, the result that appears on the game server and in blockchain will prevail, and you acknowledge and agree that our records will be the final authority in determining the terms and circumstances of your participation in the relevant online gaming activity and the results of this participation.</p>
                        <p>80. When we wish to contact you regarding such a dispute, we will do so by using any of Your contact details in your User account.</p>
                        <p>81. The User waives the right to participate in a class action lawsuit or a class wide arbitration against any entity or individual involved with the creation of Tickets on Daily Draw Platform.</p>
                        <p><strong>Notification about risks</strong></p>
                        <p>82. The User understands and accepts the risks in connection with transferring Bitcoins to the Daily Draw Platform and creating Ticket as exemplary set forth above and hereinafter. In particular, but not limited, the User understands the inherent risks listed hereinafter:
                           <br>a) Risk of software weaknesses: The User understands and accepts that the Blockchain System concept, the underlying software application and software platform is still in an early development stage and unproven, why there is no warranty that the process for creating tickets will be uninterrupted or error-free and why there is an inherent risk that the software could contain weaknesses, vulnerabilities or bugs causing, inter alia, the complete loss of Bitcoins.
                           <br>b) Regulatory risk: The User understands and accepts that the blockchain technology allows new forms of interaction and that it is possible that certain jurisdictions will apply existing regulations on, or introduce new regulations addressing blockchain technology based applications, which may be contrary to the current setup of the System and which may, inter alia, result in substantial modifications of the Service, including its termination and the loss of Bitcoins or tickets for the User.
                           <br>c) Risk of Loss of private key: Bitcoins in account wallet can only be accessed with a wallet seed or combination of private key and password. The private key is encrypted with a password. The User understands and accepts that if his wallet file or password respectively his private key got lost or stolen, the obtained Bitcoins associated with the User’s Wallet or password will be unrecoverable and will be permanently lost with the wallet seed.
                           <br>d) Risk of theft: The User understands and accepts that the Service concept, the underlying software application and software platform may be exposed to attacks by hackers or other individuals that that could result in theft or loss of bitcoins or tickets, impacting the ability to develop the Services.
                        </p>
                     </div>
                  </div>
                  <div id="mCSB_3_scrollbar_vertical" class="mCSB_scrollTools mCSB_3_scrollbar mCS-orange mCSB_scrollTools_vertical" style="display: none;">
                     <div class="mCSB_draggerContainer">
                        <div id="mCSB_3_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 30px; top: 0px;">
                           <div class="mCSB_dragger_bar" style="line-height: 30px;"></div>
                        </div>
                        <div class="mCSB_draggerRail"></div>
                     </div>
                  </div>
               </div>
            </div>
            <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-orange btn-lg">Ok</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade " id="modalReward" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content ">
         <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title" id="myModalLabel">How to get TFL reward</h4>
         </div>
         <div class="modal-body">
            <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24 rew_wr">
               <div class="reward_comics reward_comics_1">
                  <i class="text_1"></i>
               </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24 rew_wr">
               <div class="reward_comics reward_comics_2">
                  <i class="text_1"></i>
               </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24 rew_wr">
               <div class="reward_comics reward_comics_3">
                  <i class="text_1"></i>
                  <i class="text_2"></i>
               </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24 rew_wr">
               <div class="reward_comics reward_comics_4">
                  <i class="text_1"></i>
               </div>
            </div>
            <div class="col-lg-24 col-md-24 col-sm-24 col-xs-24 rew_wr">
               <div class="reward_comics reward_comics_5">
                  <i class="text_1"></i>
               </div>
            </div>
         </div>
         <div class="reward_separator">
            <h4>Step 1. Park TFL Tokens before December 28</h4>
            <p>— Log in to your existing account on Daily Draw or sign up a new one.</p>
            <p>— Choose «TFL token» on the left sidebar.</p>
            <p>— Find the blue tab in Dashboard and press «Park more».</p>
            <p>— Use the indicated address to transfer TFL from your wallet.</p>
            <h4>Step 2. Make sure you got Flip Coupons for the parked TFL</h4>
            <p>— Flip Coupons are distributed automatically to all TFL holders, on December 28.</p>
            <p>— We award a Flip Coupon per every 100 TFL you own.</p>
            <p>— Your reserved Flip Coupons appear on Daily Draw in 24 hours, right next to the Balance.</p>
            <h4>Step 3. Take part in the Survey, on December 26-28</h4>
            <p>Choose «Master Flip» in the Daily Draw sidebar. You will have to complete a survey prior to the game start. Feel free to share your opinion about Daily Draw development in 2018.</p>
            <h4>Step 4. Fill a ticket in Master Flip game before 21:00 UTC, December 28</h4>
            <p>Read the rules carefully, make your choice and confirm it.</p>
            <h4>Step 5. Get your Reward on December 29</h4>
            <p>Visit Daily Draw on December 29, check the draw results and get your rewards.</p>
         </div>
      </div>
   </div>
</div>
<!-- Modal Why do i need to park? -->
<div class="modal fade " id="whatForModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content ">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title" id="myModalLabel">Why do i need to park TFL?</h4>
         </div>
         <div class="modal-body">
            <p>Token provides its holder with extra rewards in accordance with the WP.</p>
            <ul class="green-check">
               <li>Participation in a in a special quarterly game for the Token Holders with a guaranteed prize in BTC;</li>
               <li>Тoken holders fund is: 7.94 BTC ;</li>
               <li>Participation in the development of the Daily Draw project;</li>
               <li>The first to learn about the news and take part in beta testing.</li>
            </ul>
         </div>
      </div>
   </div>
</div>

<!-- Modal 2FA Validation-->
<div class="modal fade" id="modal2FAValidation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Validate 2FA</h4>
         </div>
         <div class="modal-body">
            <form class="form" action="{{ url('2fa/authenticate') }}" method="POST" id="formLogin">
              {{ csrf_field() }}
              @if (session('error'))
                <div class="alert alert-danger">
                  <strong>Error !</strong> {!! session('error') !!}
                </div>
                @php
                  session()->forget('error');
                @endphp
                <script type="text/javascript">
                  $(function(){
                    $('#modal2FAValidation').modal('show');
                  });
                </script>
              @endif
               <div class="form-group">
                  <input class="form-control" name="secret" type="secret" placeholder="2FA Code" required>
               </div>
               <button type="submit" class="btn btn-orange btn-strong btn-shadow btn-block btn-lg">Validate</button>
            </form>
         </div>
      </div>
   </div>
</div>
