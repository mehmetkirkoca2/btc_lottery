<div class="modal-dialog modal-lg star_timeline">
   <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
         <h4 class="modal-title" id="myModalLabel">Draw results №298 31/01/2018</h4>
      </div>
      <div class="timeline__numbers">
         <ul>
            <li>1</li>
            <li>43</li>
            <li>48</li>
            <li>41</li>
            <li>35</li>
            <li>12</li>
         </ul>
      </div>
      <div class="timeline__hash">
         Hash: 0000000000000000003732c6d35fbf8bff6254dc5250361dc9546cb71690f563
      </div>
      <div class="timeline__table">
         <table class="table">
            <thead>
               <tr>
                  <th>Guessed numbers</th>
                  <th>Number of winners</th>
                  <th>Prize</th>
                  <th>Amount of payments</th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td>
                     <i class="balls-icon"></i>
                     <i class="balls-icon"></i>
                     <i class="balls-icon"></i>
                     <i class="balls-icon"></i>
                     <i class="balls-icon"></i>
                     <i class="balls-icon balls-icon__orange"></i>
                  </td>
                  <td>0</td>
                  <td>Jackpot</td>
                  <td>0</td>
               </tr>
               <tr>
                  <td>
                     <i class="balls-icon"></i>
                     <i class="balls-icon"></i>
                     <i class="balls-icon"></i>
                     <i class="balls-icon"></i>
                     <i class="balls-icon"></i>
                  </td>
                  <td>0</td>
                  <td>8.0000</td>
                  <td>0</td>
               </tr>
               <tr>
                  <td>
                     <i class="balls-icon"></i>
                     <i class="balls-icon"></i>
                     <i class="balls-icon"></i>
                     <i class="balls-icon"></i>
                     <i class="balls-icon balls-icon__orange"></i>
                  </td>
                  <td>0</td>
                  <td>2.0000</td>
                  <td>0</td>
               </tr>
               <tr>
                  <td>
                     <i class="balls-icon"></i>
                     <i class="balls-icon"></i>
                     <i class="balls-icon"></i>
                     <i class="balls-icon"></i>
                  </td>
                  <td>0</td>
                  <td>0.0400</td>
                  <td>0</td>
               </tr>
               <tr>
                  <td>
                     <i class="balls-icon"></i>
                     <i class="balls-icon"></i>
                     <i class="balls-icon"></i>
                     <i class="balls-icon balls-icon__orange"></i>
                  </td>
                  <td>0</td>
                  <td>0.0100</td>
                  <td>0</td>
               </tr>
               <tr>
                  <td>
                     <i class="balls-icon"></i>
                     <i class="balls-icon"></i>
                     <i class="balls-icon balls-icon__orange"></i>
                  </td>
                  <td>1</td>
                  <td>0.0040</td>
                  <td>0.004</td>
               </tr>
               <tr>
                  <td>
                     <i class="balls-icon"></i>
                     <i class="balls-icon"></i>
                     <i class="balls-icon"></i>
                  </td>
                  <td>2</td>
                  <td>0.0020</td>
                  <td>0.004</td>
               </tr>
               <tr>
                  <td>
                     <i class="balls-icon"></i>
                     <i class="balls-icon balls-icon__orange"></i>
                  </td>
                  <td>6</td>
                  <td>0.0010</td>
                  <td>0.006</td>
               </tr>
               <tr>
                  <td>
                     <i class="balls-icon balls-icon__orange"></i>
                  </td>
                  <td>13</td>
                  <td>0.0006</td>
                  <td>0.0078</td>
               </tr>
               <tr>
                  <td>
                     <i class="balls-icon"></i>
                     <i class="balls-icon"></i>
                  </td>
                  <td>27</td>
                  <td>0.0002</td>
                  <td>0.0054</td>
               </tr>
            </tbody>
         </table>
      </div>
      <div class="timeline__general">
         <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
               <strong>231</strong>
               <p>Users participated</p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
               <strong>436</strong>
               <p>Tickets played</p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
               <strong>0.02720 <sup>BTC</sup></strong>
               <p>Total payouts</p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
               <strong>0.09293 <sup>BTC</sup></strong>
               <p>Jackpot increased</p>
            </div>
         </div>
      </div>
      <div class="timeline__howto">
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-24">
               <div class="timeline__howtocheck" data-action="flipstar_howtocheck">
                  <p><strong>How to check the results</strong></p>
                  <p>Draw verification guide</p>
               </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-24">
               <a href="/en/check-transparency" class="btn btn-orange btn-md pull-left">Learn more</a>
               <p>about winning combination generation.</p>
            </div>
         </div>
      </div>
      <div class="timeline__download">
         <strong>Download fair game files</strong>
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-24">
               <div class="ticketsModal__CSV">
                  <p><a href="/tickets_archive/tickets-298.csv">CSV file</a> <span>(12 КБ)</span></p>
                  <p>List of tickets and combinations involved in the selected draw</p>
               </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-24">
               <div class="ticketsModal__ASC">
                  <p><a href="/tickets_archive/tickets-298.csv.asc">ASC file</a> <span>(10 КБ)</span></p>
                  <p>Electronic signature using which you can verify the reliability of the data</p>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
