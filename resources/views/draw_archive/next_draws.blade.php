@foreach ($dailyDraws as $dailyDraw)
  <tr>
     <td><a href="#flipstar_timeline" data-action="flipstar_timeline" data-drawid="{{$dailyDraw->id}}" class="timeline-link">{{$dailyDraw->id}}</a></td>
     <td>{{ intervalToDateTime($dailyDraw->hash->time) }}</td>
     <td>
        <ul class="draw_results">
           <li>{{$dailyDraw->n1}}</li>
           <li>{{$dailyDraw->n2}}</li>
           <li>{{$dailyDraw->n3}}</li>
           <li>{{$dailyDraw->n4}}</li>
           <li>{{$dailyDraw->n5}}</li>
           <li class="last">{{$dailyDraw->n6}}</li>
        </ul>
     </td>
     <td>
        {{$dailyDraw->total_won}} BTC
     </td>
     <td>{{$dailyDraw->jackpot_prize}} BTC </td>
  </tr>
@endforeach
