@extends('layouts/app')

@section('title', 'Ana Sayfa')

@section('pageCss')
  <link rel="stylesheet" href="{!! asset('css/daterangepicker.css') !!}">
  <link rel="stylesheet" href="{!! asset('css/games/io7.css') !!}">
  <link rel="stylesheet" href="{!! asset('css/games/adaptive7.css') !!}">
@endsection
@section('js-bottom')
  <script type="text/javascript" src="{!! asset('js/plugins/moment.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('js/plugins/daterangepicker.js') !!}"></script>
  <script type="text/javascript">
    $('.dateRangePicker').daterangepicker({
        locale: {
          format: 'YYYY-MM-DD'
        },
        startDate: '2013-01-01',
        endDate: '2013-12-31'
    },
    function(start, end, label) {
      // alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
    });
  </script>
@endsection
@section('content')
  <div class="content ">
     <div class="heading_section">
        <h1>Draw archive</h1>
     </div>
     <div class="archive_section row-eq-height" style="min-height: 2061px;">
        <div class="col-lg-16 col-md-16 col-sm-24 col-xs-24 archive_section__left">
           <h2>
              By <span class="filter_switcher active" data-action="filter_date"> date </span> or
              <span class="filter_switcher" data-action="filter_draw">draw</span>
           </h2>
           <div id="filter_date" class="pull-right filter__date form-inline active">
              <input type="text" class="form-control dateRangePicker" value="2017/11/11 - 2017/11/12">
              <a href="#filter_by_date" class="btn btn-orange" data-action="filter_by_date">SHOW</a>
           </div>
           <div id="filter_draw" class="pull-right filter__draw form-inline">
              № <input id="arch_from" type="number" class="form-control" name="start"> - <input id="arch_to" type="number" class="form-control" name="end">
              <a href="#filter_by_draw" class="btn btn-orange" data-action="filter_by_draw">SHOW</a>
           </div>
           <div class="archive_table">
              <table id="drawArchiveTable" class="table">
                 <thead>
                    <tr>
                       <th>Draw</th>
                       <th>Date</th>
                       <th>Winning numbers</th>
                       <th>Won</th>
                       <th>Jackpot</th>
                    </tr>
                 </thead>
                 <tbody>
                    <tr class="archive__current_draw">
                       <td>
                         <a href="#ticketsCSVModal" data-toggle="modal" data-target="#ticketsCSVModal" class="archive_link">
                         <i class="archive-icon archive-icon__download"></i> {{ getNextDrawNumber() }}</a>
                       </td>
                       <td>Today</td>
                       <td colspan="3">Waiting for the draw...</td>
                    </tr>
                    @foreach ($dailyDraws as $key=>$dailyDraw)
                      @php
                        if($key+1==count($dailyDraws)){
                          $dailyDrawMaxId=$dailyDraw->id;
                        }
                      @endphp
                      <tr>
                         <td><a href="#flipstar_timeline" data-action="flipstar_timeline" data-drawid="{{$dailyDraw->id}}" class="timeline-link">{{$dailyDraw->id}}</a></td>
                         <td>{{ intervalToDateTime($dailyDraw->hash->time) }}</td>
                         <td>
                            <ul class="draw_results">
                               <li>{{$dailyDraw->n1}}</li>
                               <li>{{$dailyDraw->n2}}</li>
                               <li>{{$dailyDraw->n3}}</li>
                               <li>{{$dailyDraw->n4}}</li>
                               <li>{{$dailyDraw->n5}}</li>
                               <li class="last">{{$dailyDraw->n6}}</li>
                            </ul>
                         </td>
                         <td>
                            {{$dailyDraw->total_won}} BTC
                         </td>
                         <td>{{$dailyDraw->jackpot_prize}} BTC </td>
                      </tr>
                    @endforeach
                 </tbody>
              </table>
           </div>
           <div class="archive_showmore">
             <button onclick="showMoreDraws();" class="btn btn-lg btn-outline-dark btn-block">Show more</button>
           </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-24 col-xs-24 archive_section__right">
           <div class="raffled_since_block">
              <h4>Raffled since inception</h4>
              <p>74.786<sup>BTC</sup></p>
           </div>
        </div>
     </div>
     <div class="modal" id="ticketsCSVModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

     </div>
     @include('layouts/footer')
  </div>
  <script type="text/javascript">
    function showMoreDraws() {
      $.ajax({
        type:'POST',
        url:'showMoreDraws',
        success: function (data) {
          $('#drawArchiveTable').append(data);
        }
      });
    }
  </script>
@endsection
