@extends('layouts/app')

@section('title', 'Ana Sayfa')

@section('pageCss')
  <link rel="stylesheet" href="{!! asset('css/games/io4.css') !!}">
  <link rel="stylesheet" href="{!! asset('css/games/adaptive4.css') !!}">
@endsection

@section('content')
  <div class="content no_left_nav">
   <div class="top_section">
      <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24 top_section__text">
         <h1>Daily Draw Token <span class="text-warning">(TFL)</span></h1>
         <p>Become a member of Daily Draw token holders community. Learn more about Daily Draw Token (TFL), its advantages and extra rewards which TFL provides to its holder.</p>
         <div class="top_section_exchange">
            <div class="col-lg-8 col-md-8 col-sm-24 col-xs-24">
               <p class="heading-1">TFL price</p>
               <p class="amount-1">$2.13
                  <span class="text-danger"><i class="rate-icon--down"></i> -5.92%</span>
               </p>
               <p class="text-small">0.00015 BTC</p>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-24 col-xs-24">
               <p class="heading-1">Market Cap</p>
               <p class="amount-1">$13,304,055.00</p>
               <p class="text-small">967.000 BTC</p>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-24 col-xs-24">
               <p class="heading-1">Volume (24h)</p>
               <p class="amount-1">$241,271.00</p>
               <p class="text-small">17.5 BTC</p>
            </div>
         </div>
         <a href="https://etherdelta.com/#TFL-BTC" target="_blank" class="btn btn-orange btn-lg btn-uppercase">Buy TFL</a>
         <a href="#signupModal" data-toggle="modal" data-target="#signupModal" class="btn btn-outline btn-lg btn-uppercase">Park TFL</a>
      </div>
   </div>
   <div class="flipcast_live_section">
      <h3 class="text-center">Flipсast #Q4 2017  <span class="badge badge-danger">• LIVE</span></h3>
      <div class="video_wrapper">
         <div class="video_control">
            <a href="#" class="play-btn" onclick="$(this).parent().hide(); $('.video_embed').removeClass('hidden'); return false"></a>
            <p>Watch now</p>
         </div>
         <div class="hidden video_embed">
            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/BT_jhFX48Oo" frameborder="0" allowfullscreen=""></iframe>
         </div>
      </div>
   </div>
   <div class="text_section row-eq-height">
      <div class="col-lg-16 col-md-16 col-sm-24 col-xs-24 left__content">
         <h2>Daily Draw token (TFL)</h2>
         <p>Daily Draw has conducted a <a href="#" data-toggle="modal" data-target="#tokenSaleModal">successful crowdsale</a> on 28.06 – 28.07.2017, gaining over 2000 in Bitcoin equivalent (BTC, ETH, LTC and DASH were accepted). The company initially offered up to 21 000 000 tokens at the price of 0,0005 BTC per TFL. </p>
         <p>Over 3000 participants acquired the total of 6 247 267 TFLs, while the rest of the initial amount was «burned» to proportionally increase each token holder’s percentage in the overall token amount. The final overall amount of existing TFL after burning.</p>
         <p>Daily Draw tokens (TFL) are listed on several cryptocurrency exchanges, which makes them an attractive investment asset. Also, the token provides its holder with extra rewards in accordance with the White Paper.</p>
         <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#rewards" aria-controls="rewards" role="tab" data-toggle="tab">Rewards</a></li>
            <li role="presentation"><a href="#erc20" aria-controls="erc20" role="tab" data-toggle="tab">ERC-20</a></li>
         </ul>
         <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade active" id="rewards">
               <p>Daily Draw tokens allow their owners to participate in a special quarterly game. Its prize fund is formed by 10-15% payments from each sold ticket according to the following scheme:</p>
               <div class="scheme_tfl">
                  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-24">
                     <i class="tfl-icons tfl-icons__money"></i>
                     <p>Payments are assigned to the fund daily after each draw; </p>
                  </div>
                  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-24">
                     <i class="tfl-icons tfl-icons__charts"></i>
                     <p>The amount of funds assigned to the fund (within 10-15% of income) is determined by the Token holders voting; </p>
                  </div>
                  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-24">
                     <i class="tfl-icons tfl-icons__date"></i>
                     <p>The Token owners game is held 4 times a year; </p>
                  </div>
                  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-24">
                     <i class="tfl-icons tfl-icons__list"></i>
                     <p>To participate in the game, TFL Token holders must verify their ownership through a smart contract; </p>
                  </div>
                  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-24">
                     <i class="tfl-icons tfl-icons__bell"></i>
                     <p>The owners of the Tokens are notified in advance of the date of the game;</p>
                  </div>
                  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-24">
                     <i class="tfl-icons tfl-icons__btc"></i>
                     <p>TFL club prize fund will be formed in accordance with the following proportion.</p>
                  </div>
               </div>
               <p>According to the basic forecast, at the initial stage Daily Draw can seize up to 0.4% of the global online lotteries market (or 0.03% of the world online gambling market), which corresponds to a daily turnover within the system at 15 BTC. In this case, the potential Token holders game winnings relative to the value of the purchased tokens will be approximately at 5.2-7.8% per year in BTC, depending on the share of the ticket sales revenue assigned for the TFL holders game. </p>
            </div>
            <div role="tabpanel" class="tab-pane fade " id="erc20">
               <p>Daily Draw’s TFL token is based on the Ethereum ERC-20 standard. ERC here means "Ethereum Request for Comments”, while “20” is the unique proposal ID number. In general, it defines a list of rules that such tokens have to implement, to be able functioning in the Ethereum ecosystem. These days ERC-20 becomes an industrial standard for token creation within a crowdsale procedure.</p>
               <p>For our business, this results in enhanced transparency, best possible connectivity and clear structure - to “explain” the coin for partners and blockchain community.</p>
            </div>
         </div>
      </div>
      <div class="col-lg-8 col-md-8 col-sm-24 col-xs-24 right__content">
         <div class="right_side_block">
            <div class="right_stat">
               <p class="heading-1">TFL exchange price</p>
               <p class="amount-1">$2.13
                  <span class="text-danger"><i class="rate-icon--down"></i> -5.92%</span>
               </p>
               <p class="text-small">0.00015 BTC</p>
            </div>
            <div class="right_stat">
               <p class="heading-1">Market Cap</p>
               <p class="amount-1">US$13,304,055.00</p>
               <p class="text-small">967.000 BTC</p>
            </div>
            <div class="right_stat">
               <p class="heading-1">Volume (24h)</p>
               <p class="amount-1">US$241,271.00</p>
               <p class="text-small">17.5 BTC</p>
            </div>
            <div class="row">
               <div class="col-lg-12 col-md-24 col-sm-24 col-xs-24">
                  <a href="https://etherdelta.com/#TFL-BTC" target="_blank" class="btn btn-block btn-lg btn-orange btn-uppercase btn-mbottom">Buy TFL</a>
               </div>
               <div class="col-lg-12 col-md-24 col-sm-24 col-xs-24">
                  <a href="#signupModal" data-toggle="modal" data-target="#signupModal" class="btn btn-block  btn-outline btn-lg">Park TFL</a>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="general_stat_section">
      <div class="general_stat_header">
         <div class="col-lg-8 col-md-8 col-sm-24 col-xs-24">
            <h3>General statistics</h3>
         </div>
         <div class="col-lg-8 col-md-8 col-sm-24 col-xs-24 text-center">
            <ul class="nav nav-pills" role="tablist">
               <li role="presentation" class="active"><a href="#week" aria-controls="week" role="tab" data-toggle="tab">Week</a></li>
               <li role="presentation"><a href="#month" aria-controls="month" role="tab" data-toggle="tab">Month</a></li>
               <li role="presentation"><a href="#quarter" aria-controls="quarter" role="tab" data-toggle="tab">Quarter</a></li>
            </ul>
         </div>
         <div class="col-lg-8 col-md-8 col-sm-24 col-xs-24">
         </div>
      </div>
      <div class="tab-content">
         <div role="tabpanel" class="tab-pane fade active" id="week">
            <div class="sales_stat row-eq-height">
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-24">
                  <div class="sales_stat_wrapper">
                     <p class="heading-1">11317</p>
                     <p>Total tickets sold last week</p>
                     <a href="#" class="popover" onclick="return false" title="" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-html="true" data-content="<strong>5433</strong> - Daily Draw<br><strong>5884</strong> - Rapid to the Moon" data-original-title="Total tickets sold last week"></a>
                  </div>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-24">
                  <div class="sales_stat_wrapper">
                     <p class="heading-1">562</p>
                     <p>Users participated</p>
                     <a href="#" class="popover" onclick="return false" title="" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-html="true" data-content="<strong>314</strong> - Daily Draw<br><strong>248</strong> - Rapid to the Moon" data-original-title="Users participated"></a>
                  </div>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-24">
                  <div class="sales_stat_wrapper">
                     <p class="heading-1">1568</p>
                     <p>Winners in the last week</p>
                     <a href="#" class="popover" onclick="return false" title="" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-html="true" data-content="<strong>472</strong> - Daily Draw<br><strong>1096</strong> - Rapid to the Moon" data-original-title="Winners in the last week"></a>
                  </div>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-24">
                  <div class="sales_stat_wrapper">
                     <p class="heading-1">0.1089<sup>BTC</sup></p>
                     <p>TFL holders fund increased on</p>
                  </div>
               </div>
            </div>
            <div class="stat_graphic">
               <div id="graphic" data-highcharts-chart="0">
                  <div id="highcharts-awpca4c-0" class="highcharts-container " style="position: relative; overflow: hidden; width: 1183px; height: 400px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                     <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="1183" height="400" viewBox="0 0 1183 400">
                        <desc>Created with Highcharts 5.0.12</desc>
                        <defs>
                           <clipPath id="highcharts-awpca4c-1">
                              <rect x="0" y="0" width="1047" height="343" fill="none"></rect>
                           </clipPath>
                        </defs>
                        <rect fill="transparent" class="highcharts-background" x="0" y="0" width="1183" height="400" rx="0" ry="0"></rect>
                        <rect fill="none" class="highcharts-plot-background" x="71" y="20" width="1047" height="343"></rect>
                        <g class="highcharts-grid highcharts-xaxis-grid ">
                           <path fill="none" class="highcharts-grid-line" d="M 201.5 20 L 201.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 332.5 20 L 332.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 463.5 20 L 463.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 594.5 20 L 594.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 724.5 20 L 724.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 855.5 20 L 855.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 986.5 20 L 986.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 1117.5 20 L 1117.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 70.5 20 L 70.5 363" opacity="1"></path>
                        </g>
                        <g class="highcharts-grid highcharts-yaxis-grid ">
                           <path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 71 363.5 L 1118 363.5" opacity="1"></path>
                           <path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 71 294.5 L 1118 294.5" opacity="1"></path>
                           <path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 71 226.5 L 1118 226.5" opacity="1"></path>
                           <path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 71 157.5 L 1118 157.5" opacity="1"></path>
                           <path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 71 89.5 L 1118 89.5" opacity="1"></path>
                           <path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 71 19.5 L 1118 19.5" opacity="1"></path>
                        </g>
                        <g class="highcharts-grid highcharts-yaxis-grid ">
                           <path fill="none" class="highcharts-grid-line" d="M 71 363.5 L 1118 363.5" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 71 294.5 L 1118 294.5" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 71 226.5 L 1118 226.5" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 71 157.5 L 1118 157.5" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 71 89.5 L 1118 89.5" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 71 20.5 L 1118 20.5" opacity="1"></path>
                        </g>
                        <g class="highcharts-grid highcharts-yaxis-grid "></g>
                        <rect fill="none" class="highcharts-plot-border" x="71" y="20" width="1047" height="343"></rect>
                        <g class="highcharts-axis highcharts-xaxis ">
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 201.5 363 L 201.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 332.5 363 L 332.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 463.5 363 L 463.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 594.5 363 L 594.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 724.5 363 L 724.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 855.5 363 L 855.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 986.5 363 L 986.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 1118.5 363 L 1118.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 70.5 363 L 70.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" d="M 71 363.5 L 1118 363.5"></path>
                        </g>
                        <g class="highcharts-axis highcharts-yaxis ">
                           <text x="1130" text-anchor="start" transform="translate(0,0)" class="highcharts-axis-title" style="color:#000;font-family:MuseoSansCyrl;;font-size:16px;font-weight:700;fill:#000;" y="20"></text>
                           <path fill="none" class="highcharts-axis-line" d="M 1118 20 L 1118 363"></path>
                        </g>
                        <g class="highcharts-axis highcharts-yaxis ">
                           <path fill="none" class="highcharts-axis-line" d="M 71 20 L 71 363"></path>
                        </g>
                        <g class="highcharts-axis highcharts-yaxis ">
                           <text x="10.453125" text-anchor="middle" transform="translate(0,0) rotate(270 10.453125 191.5)" class="highcharts-axis-title" style="color:#999;fill:#999;" y="191.5"></text>
                           <path fill="none" class="highcharts-axis-line" d="M 20 20 L 20 363"></path>
                        </g>
                        <g class="highcharts-series-group">
                           <g class="highcharts-series highcharts-series-0 highcharts-column-series highcharts-color-undefined highcharts-tracker " transform="translate(71,20) scale(1 1)" clip-path="url(#highcharts-awpca4c-1)">
                              <rect x="34" y="111" width="63" height="134" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="165" y="134" width="63" height="130" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="296" y="112" width="63" height="97" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="427" y="184" width="63" height="104" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="558" y="155" width="63" height="111" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="688" y="107" width="63" height="144" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="819" y="66" width="63" height="139" fill="#aa95d3" class="highcharts-point "></rect>
                              <rect x="950" y="189" width="63" height="90" fill="#aa95d3" class="highcharts-point"></rect>
                           </g>
                           <g class="highcharts-markers highcharts-series-0 highcharts-column-series highcharts-color-undefined " transform="translate(71,20) scale(1 1)" clip-path="none"></g>
                           <g class="highcharts-series highcharts-series-1 highcharts-column-series highcharts-color-undefined highcharts-tracker" transform="translate(71,20) scale(1 1)" clip-path="url(#highcharts-awpca4c-1)">
                              <rect x="34" y="245" width="63" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="165" y="264" width="63" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="296" y="209" width="63" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="427" y="288" width="63" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="558" y="266" width="63" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="688" y="251" width="63" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="819" y="205" width="63" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="950" y="279" width="63" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                           </g>
                           <g class="highcharts-markers highcharts-series-1 highcharts-column-series highcharts-color-undefined " transform="translate(71,20) scale(1 1)" clip-path="none"></g>
                           <g class="highcharts-series highcharts-series-2 highcharts-column-series highcharts-color-undefined highcharts-tracker " transform="translate(71,20) scale(1 1)" clip-path="url(#highcharts-awpca4c-1)">
                              <rect x="34" y="245" width="63" height="99" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="165" y="264" width="63" height="80" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="296" y="209" width="63" height="135" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="427" y="288" width="63" height="56" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="558" y="266" width="63" height="78" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="688" y="251" width="63" height="93" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="819" y="205" width="63" height="139" fill="#855fa8" class="highcharts-point "></rect>
                              <rect x="950" y="279" width="63" height="65" fill="#855fa8" class="highcharts-point"></rect>
                           </g>
                           <g class="highcharts-markers highcharts-series-2 highcharts-column-series highcharts-color-undefined " transform="translate(71,20) scale(1 1)" clip-path="none"></g>
                           <g class="highcharts-series highcharts-series-3 highcharts-spline-series highcharts-color-undefined " transform="translate(71,20) scale(1 1)" clip-path="url(#highcharts-awpca4c-1)">
                              <path fill="none" d="M 65.4375 288.12 C 65.4375 288.12 143.9625 277.144 196.3125 260.68 C 248.6625 244.216 274.8375 222.26399999999998 327.1875 205.79999999999998 C 379.5375 189.33599999999998 405.7125 189.336 458.0625 178.36 C 510.4125 167.38400000000001 536.5875 161.896 588.9375 150.92 C 641.2875 139.944 667.4625 139.94400000000002 719.8125 123.47999999999999 C 772.1625 107.01599999999999 798.3375 85.06399999999996 850.6875 68.59999999999997 C 903.0375 52.13599999999997 981.5625 41.160000000000025 981.5625 41.160000000000025" class="highcharts-graph" stroke="#3cb878" stroke-width="2" stroke-linejoin="round" stroke-linecap="round"></path>
                              <path fill="none" d="M 55.4375 288.12 L 65.4375 288.12 C 65.4375 288.12 143.9625 277.144 196.3125 260.68 C 248.6625 244.216 274.8375 222.26399999999998 327.1875 205.79999999999998 C 379.5375 189.33599999999998 405.7125 189.336 458.0625 178.36 C 510.4125 167.38400000000001 536.5875 161.896 588.9375 150.92 C 641.2875 139.944 667.4625 139.94400000000002 719.8125 123.47999999999999 C 772.1625 107.01599999999999 798.3375 85.06399999999996 850.6875 68.59999999999997 C 903.0375 52.13599999999997 981.5625 41.160000000000025 981.5625 41.160000000000025 L 991.5625 41.160000000000025" stroke-linejoin="round" visibility="visible" stroke="rgba(192,192,192,0.0001)" stroke-width="22" class="highcharts-tracker"></path>
                           </g>
                           <g class="highcharts-markers highcharts-series-3 highcharts-spline-series highcharts-color-undefined highcharts-tracker " transform="translate(71,20) scale(1 1)">
                              <path fill="#3cb878" d="M 850 68.59999999999997 A 0 0 0 1 1 850 68.59999999999997 Z" class="highcharts-halo highcharts-color-undefined" fill-opacity="0.25"></path>
                              <path fill="#3cb878" d="M 856 69 A 6 6 0 1 1 855.9999970000002 68.994000001 Z" stroke="#ffffff" stroke-width="1" visibility="hidden"></path>
                           </g>
                        </g>
                        <text x="592" text-anchor="middle" class="highcharts-title" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                        <text x="592" text-anchor="middle" class="highcharts-subtitle" style="color:#666666;fill:#666666;" y="24"></text>
                        <g class="highcharts-axis-labels highcharts-xaxis-labels ">
                           <text x="136.4375" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">08/01/2018</text>
                           <text x="267.3125" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">09/01/2018</text>
                           <text x="398.1875" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">10/01/2018</text>
                           <text x="529.0625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">11/01/2018</text>
                           <text x="659.9375" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">12/01/2018</text>
                           <text x="790.8125" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">13/01/2018</text>
                           <text x="921.6875" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">14/01/2018</text>
                           <text x="1052.5625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">15/01/2018</text>
                        </g>
                        <g class="highcharts-axis-labels highcharts-yaxis-labels ">
                           <text x="1133" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="start" transform="translate(0,0)" y="367" opacity="1">
                              <tspan>0</tspan>
                           </text>
                           <text x="1133" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="start" transform="translate(0,0)" y="298" opacity="1">
                              <tspan>0.025</tspan>
                           </text>
                           <text x="1133" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="start" transform="translate(0,0)" y="229" opacity="1">
                              <tspan>0.05</tspan>
                           </text>
                           <text x="1133" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="start" transform="translate(0,0)" y="161" opacity="1">
                              <tspan>0.075</tspan>
                           </text>
                           <text x="1133" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="start" transform="translate(0,0)" y="92" opacity="1">
                              <tspan>0.1</tspan>
                           </text>
                           <text x="1133" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="start" transform="translate(0,0)" y="24" opacity="1">
                              <tspan>0.125</tspan>
                           </text>
                        </g>
                        <g class="highcharts-axis-labels highcharts-yaxis-labels ">
                           <text x="56" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="end" transform="translate(0,0)" y="367" opacity="1">
                              <tspan>0</tspan>
                           </text>
                           <text x="56" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="end" transform="translate(0,0)" y="298" opacity="1">
                              <tspan>500</tspan>
                           </text>
                           <text x="56" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="end" transform="translate(0,0)" y="229" opacity="1">
                              <tspan>1000</tspan>
                           </text>
                           <text x="56" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="end" transform="translate(0,0)" y="161" opacity="1">
                              <tspan>1500</tspan>
                           </text>
                           <text x="56" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="end" transform="translate(0,0)" y="92" opacity="1">
                              <tspan>2000</tspan>
                           </text>
                           <text x="56" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="end" transform="translate(0,0)" y="24" opacity="1">
                              <tspan>2500</tspan>
                           </text>
                        </g>
                        <g class="highcharts-axis-labels highcharts-yaxis-labels "></g>
                        <g class="highcharts-label highcharts-tooltip highcharts-color-undefined" style="cursor:default;pointer-events:none;white-space:nowrap;" transform="translate(832,-9999)" opacity="0" visibility="visible">
                           <path fill="none" class="highcharts-label-box highcharts-tooltip-box" d="M 10.5 0.5 L 169.5 0.5 C 179.5 0.5 179.5 0.5 179.5 10.5 L 179.5 37.5 C 179.5 47.5 179.5 47.5 169.5 47.5 L 95.5 47.5 89.5 53.5 83.5 47.5 10.5 47.5 C 0.5 47.5 0.5 47.5 0.5 37.5 L 0.5 10.5 C 0.5 0.5 0.5 0.5 10.5 0.5" isShadow="true" stroke="#000000" stroke-opacity="0.049999999999999996" stroke-width="5" transform="translate(1, 1)"></path>
                           <path fill="none" class="highcharts-label-box highcharts-tooltip-box" d="M 10.5 0.5 L 169.5 0.5 C 179.5 0.5 179.5 0.5 179.5 10.5 L 179.5 37.5 C 179.5 47.5 179.5 47.5 169.5 47.5 L 95.5 47.5 89.5 53.5 83.5 47.5 10.5 47.5 C 0.5 47.5 0.5 47.5 0.5 37.5 L 0.5 10.5 C 0.5 0.5 0.5 0.5 10.5 0.5" isShadow="true" stroke="#000000" stroke-opacity="0.09999999999999999" stroke-width="3" transform="translate(1, 1)"></path>
                           <path fill="none" class="highcharts-label-box highcharts-tooltip-box" d="M 10.5 0.5 L 169.5 0.5 C 179.5 0.5 179.5 0.5 179.5 10.5 L 179.5 37.5 C 179.5 47.5 179.5 47.5 169.5 47.5 L 95.5 47.5 89.5 53.5 83.5 47.5 10.5 47.5 C 0.5 47.5 0.5 47.5 0.5 37.5 L 0.5 10.5 C 0.5 0.5 0.5 0.5 10.5 0.5" isShadow="true" stroke="#000000" stroke-opacity="0.15" stroke-width="1" transform="translate(1, 1)"></path>
                           <path fill="#fff" class="highcharts-label-box highcharts-tooltip-box" d="M 10.5 0.5 L 169.5 0.5 C 179.5 0.5 179.5 0.5 179.5 10.5 L 179.5 37.5 C 179.5 47.5 179.5 47.5 169.5 47.5 L 95.5 47.5 89.5 53.5 83.5 47.5 10.5 47.5 C 0.5 47.5 0.5 47.5 0.5 37.5 L 0.5 10.5 C 0.5 0.5 0.5 0.5 10.5 0.5" stroke="#5d4c84" stroke-width="3"></path>
                           <text x="8" style="font-size:12px;color:#333333;fill:#333333;" y="20">
                              <tspan style="font-size: 10px">14/01/2018</tspan>
                              <tspan style="fill:#855fa8" x="8" dy="15">●</tspan>
                              <tspan dx="0"> Rapid to the Moon: </tspan>
                              <tspan style="font-weight:bold" dx="0">1 015</tspan>
                           </text>
                        </g>
                     </svg>
                  </div>
               </div>
               <div class="legend">
                  <span><i class="balls-icon balls-icon__fs"></i> Daily Draw</span>
                  <span><i class="balls-icon balls-icon__free"></i> Free tickets</span>
                  <span><i class="balls-icon balls-icon__rapid"></i> Rapid to the Moon</span>
                  <span><i class="balls-icon balls-icon__success"></i> Token holder's fund, BTC</span>
               </div>
               <p>20/12/2017 - added an additional option to buy tickets with TFL</p>
            </div>
            <script type="text/javascript">
               var TAB_WEEK_INFO = {
                   categories: ["08/01/2018","09/01/2018","10/01/2018","11/01/2018","12/01/2018","13/01/2018","14/01/2018","15/01/2018"],
                   star: [ 976,948,708,763,814,1044,1012,654],
                   free: [ 0,0,0,0,0,0,0,0],
                   fund: [ 0.02,0.03,0.05,0.06,0.07,0.08,0.1,0.11],
                   rapid: [ 723,580,982,405,566,680,1015,473]
               };
            </script>
         </div>
         <div role="tabpanel" class="tab-pane fade" id="month" style="">
            <div class="sales_stat row-eq-height">
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-24">
                  <div class="sales_stat_wrapper">
                     <p class="heading-1">64218</p>
                     <p>Total tickets sold last month</p>
                     <a href="#" class="popover" onclick="return false" title="" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-html="true" data-content="<strong>25435</strong> - Daily Draw<br><strong>38783</strong> - Rapid to the Moon" data-original-title="Total tickets sold last month"></a>
                  </div>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-24">
                  <div class="sales_stat_wrapper">
                     <p class="heading-1">1592</p>
                     <p>Users participated</p>
                     <a href="#" class="popover" onclick="return false" title="" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-html="true" data-content="<strong>836</strong> - Daily Draw<br><strong>756</strong> - Rapid to the Moon" data-original-title="Users participated"></a>
                  </div>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-24">
                  <div class="sales_stat_wrapper">
                     <p class="heading-1">9967</p>
                     <p>Winners in the last month</p>
                     <a href="#" class="popover" onclick="return false" title="" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-html="true" data-content="<strong>2629</strong> - Daily Draw<br><strong>7338</strong> - Rapid to the Moon" data-original-title="Winners in the last month"></a>
                  </div>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-24">
                  <div class="sales_stat_wrapper">
                     <p class="heading-1">0.4775<sup>BTC</sup></p>
                     <p>TFL holders fund increased on</p>
                  </div>
               </div>
            </div>
            <div class="stat_graphic">
               <div id="graphic_month" data-highcharts-chart="1">
                  <div id="highcharts-awpca4c-4" class="highcharts-container " style="position: relative; overflow: hidden; width: 1183px; height: 400px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                     <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="1183" height="400" viewBox="0 0 1183 400">
                        <desc>Created with Highcharts 5.0.12</desc>
                        <defs>
                           <clipPath id="highcharts-awpca4c-5">
                              <rect x="0" y="0" width="1065" height="343" fill="none"></rect>
                           </clipPath>
                        </defs>
                        <rect fill="transparent" class="highcharts-background" x="0" y="0" width="1183" height="400" rx="0" ry="0"></rect>
                        <rect fill="none" class="highcharts-plot-background" x="71" y="20" width="1065" height="343"></rect>
                        <g class="highcharts-grid highcharts-xaxis-grid ">
                           <path fill="none" class="highcharts-grid-line" d="M 103.5 20 L 103.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 137.5 20 L 137.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 170.5 20 L 170.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 203.5 20 L 203.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 236.5 20 L 236.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 270.5 20 L 270.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 303.5 20 L 303.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 336.5 20 L 336.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 370.5 20 L 370.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 403.5 20 L 403.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 436.5 20 L 436.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 469.5 20 L 469.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 503.5 20 L 503.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 536.5 20 L 536.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 569.5 20 L 569.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 603.5 20 L 603.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 636.5 20 L 636.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 669.5 20 L 669.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 702.5 20 L 702.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 736.5 20 L 736.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 769.5 20 L 769.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 802.5 20 L 802.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 835.5 20 L 835.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 869.5 20 L 869.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 902.5 20 L 902.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 935.5 20 L 935.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 969.5 20 L 969.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 1002.5 20 L 1002.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 1035.5 20 L 1035.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 1068.5 20 L 1068.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 1102.5 20 L 1102.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 1135.5 20 L 1135.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 70.5 20 L 70.5 363" opacity="1"></path>
                        </g>
                        <g class="highcharts-grid highcharts-yaxis-grid ">
                           <path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 71 363.5 L 1136 363.5" opacity="1"></path>
                           <path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 71 294.5 L 1136 294.5" opacity="1"></path>
                           <path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 71 226.5 L 1136 226.5" opacity="1"></path>
                           <path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 71 157.5 L 1136 157.5" opacity="1"></path>
                           <path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 71 89.5 L 1136 89.5" opacity="1"></path>
                           <path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 71 19.5 L 1136 19.5" opacity="1"></path>
                        </g>
                        <g class="highcharts-grid highcharts-yaxis-grid ">
                           <path fill="none" class="highcharts-grid-line" d="M 71 363.5 L 1136 363.5" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 71 294.5 L 1136 294.5" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 71 226.5 L 1136 226.5" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 71 157.5 L 1136 157.5" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 71 89.5 L 1136 89.5" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 71 20.5 L 1136 20.5" opacity="1"></path>
                        </g>
                        <g class="highcharts-grid highcharts-yaxis-grid "></g>
                        <rect fill="none" class="highcharts-plot-border" x="71" y="20" width="1065" height="343"></rect>
                        <g class="highcharts-axis highcharts-xaxis ">
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 103.5 363 L 103.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 137.5 363 L 137.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 170.5 363 L 170.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 203.5 363 L 203.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 236.5 363 L 236.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 270.5 363 L 270.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 303.5 363 L 303.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 336.5 363 L 336.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 370.5 363 L 370.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 403.5 363 L 403.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 436.5 363 L 436.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 469.5 363 L 469.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 503.5 363 L 503.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 536.5 363 L 536.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 569.5 363 L 569.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 603.5 363 L 603.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 636.5 363 L 636.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 669.5 363 L 669.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 702.5 363 L 702.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 736.5 363 L 736.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 769.5 363 L 769.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 802.5 363 L 802.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 835.5 363 L 835.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 869.5 363 L 869.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 902.5 363 L 902.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 935.5 363 L 935.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 969.5 363 L 969.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 1002.5 363 L 1002.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 1035.5 363 L 1035.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 1068.5 363 L 1068.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 1102.5 363 L 1102.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 1136.5 363 L 1136.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 70.5 363 L 70.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" d="M 71 363.5 L 1136 363.5"></path>
                        </g>
                        <g class="highcharts-axis highcharts-yaxis ">
                           <text x="1148" text-anchor="start" transform="translate(0,0)" class="highcharts-axis-title" style="color:#000;font-family:MuseoSansCyrl;;font-size:16px;font-weight:700;fill:#000;" y="20"></text>
                           <path fill="none" class="highcharts-axis-line" d="M 1136 20 L 1136 363"></path>
                        </g>
                        <g class="highcharts-axis highcharts-yaxis ">
                           <path fill="none" class="highcharts-axis-line" d="M 71 20 L 71 363"></path>
                        </g>
                        <g class="highcharts-axis highcharts-yaxis ">
                           <text x="10.453125" text-anchor="middle" transform="translate(0,0) rotate(270 10.453125 191.5)" class="highcharts-axis-title" style="color:#999;fill:#999;" y="191.5"></text>
                           <path fill="none" class="highcharts-axis-line" d="M 20 20 L 20 363"></path>
                        </g>
                        <g class="highcharts-series-group">
                           <g class="highcharts-series highcharts-series-0 highcharts-column-series highcharts-color-undefined highcharts-tracker" transform="translate(71,20) scale(1 1)" clip-path="url(#highcharts-awpca4c-5)">
                              <rect x="9" y="322" width="16" height="13" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="42" y="319" width="16" height="14" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="75" y="313" width="16" height="23" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="108" y="314" width="16" height="21" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="142" y="19" width="16" height="20" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="175" y="291" width="16" height="24" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="208" y="292" width="16" height="19" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="242" y="286" width="16" height="23" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="275" y="277" width="16" height="36" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="308" y="257" width="16" height="33" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="341" y="278" width="16" height="26" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="375" y="306" width="16" height="24" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="408" y="290" width="16" height="25" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="441" y="315" width="16" height="23" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="475" y="239" width="16" height="50" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="508" y="187" width="16" height="113" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="541" y="232" width="16" height="70" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="574" y="181" width="16" height="37" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="608" y="77" width="16" height="36" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="641" y="187" width="16" height="57" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="674" y="242" width="16" height="50" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="708" y="267" width="16" height="52" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="741" y="291" width="16" height="31" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="774" y="221" width="16" height="38" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="807" y="266" width="16" height="45" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="841" y="274" width="16" height="43" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="874" y="267" width="16" height="32" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="907" y="291" width="16" height="34" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="941" y="281" width="16" height="37" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="974" y="265" width="16" height="48" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="1007" y="251" width="16" height="47" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="1040" y="292" width="16" height="30" fill="#aa95d3" class="highcharts-point"></rect>
                           </g>
                           <g class="highcharts-markers highcharts-series-0 highcharts-column-series highcharts-color-undefined " transform="translate(71,20) scale(1 1)" clip-path="none"></g>
                           <g class="highcharts-series highcharts-series-1 highcharts-column-series highcharts-color-undefined highcharts-tracker" transform="translate(71,20) scale(1 1)" clip-path="url(#highcharts-awpca4c-5)">
                              <rect x="9" y="335" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="42" y="333" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="75" y="336" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="108" y="335" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="142" y="39" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="175" y="315" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="208" y="311" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="242" y="309" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="275" y="313" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="308" y="290" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="341" y="304" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="375" y="330" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="408" y="315" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="441" y="338" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="475" y="289" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="508" y="300" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="541" y="302" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="574" y="218" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="608" y="113" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="641" y="244" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="674" y="292" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="708" y="319" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="741" y="322" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="774" y="259" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="807" y="311" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="841" y="317" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="874" y="299" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="907" y="325" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="941" y="318" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="974" y="313" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="1007" y="298" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="1040" y="322" width="16" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                           </g>
                           <g class="highcharts-markers highcharts-series-1 highcharts-column-series highcharts-color-undefined " transform="translate(71,20) scale(1 1)" clip-path="none"></g>
                           <g class="highcharts-series highcharts-series-2 highcharts-column-series highcharts-color-undefined highcharts-tracker" transform="translate(71,20) scale(1 1)" clip-path="url(#highcharts-awpca4c-5)">
                              <rect x="9" y="335" width="16" height="9" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="42" y="333" width="16" height="11" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="75" y="336" width="16" height="8" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="108" y="335" width="16" height="9" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="142" y="39" width="16" height="305" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="175" y="315" width="16" height="29" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="208" y="311" width="16" height="33" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="242" y="309" width="16" height="35" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="275" y="313" width="16" height="31" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="308" y="290" width="16" height="54" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="341" y="304" width="16" height="40" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="375" y="330" width="16" height="14" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="408" y="315" width="16" height="29" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="441" y="338" width="16" height="6" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="475" y="289" width="16" height="55" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="508" y="300" width="16" height="44" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="541" y="302" width="16" height="42" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="574" y="218" width="16" height="126" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="608" y="113" width="16" height="231" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="641" y="244" width="16" height="100" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="674" y="292" width="16" height="52" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="708" y="319" width="16" height="25" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="741" y="322" width="16" height="22" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="774" y="259" width="16" height="85" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="807" y="311" width="16" height="33" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="841" y="317" width="16" height="27" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="874" y="299" width="16" height="45" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="907" y="325" width="16" height="19" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="941" y="318" width="16" height="26" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="974" y="313" width="16" height="31" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="1007" y="298" width="16" height="46" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="1040" y="322" width="16" height="22" fill="#855fa8" class="highcharts-point"></rect>
                           </g>
                           <g class="highcharts-markers highcharts-series-2 highcharts-column-series highcharts-color-undefined " transform="translate(71,20) scale(1 1)" clip-path="none"></g>
                           <g class="highcharts-series highcharts-series-3 highcharts-spline-series highcharts-color-undefined " transform="translate(71,20) scale(1 1)" clip-path="url(#highcharts-awpca4c-5)">
                              <path fill="none" d="M 16.640625 336.14 C 16.640625 336.14 36.609375 332.024 49.921875 329.28 C 63.234375 326.53599999999994 69.890625 326.536 83.203125 322.42 C 96.515625 318.304 103.171875 312.816 116.484375 308.7 C 129.796875 304.584 136.453125 305.956 149.765625 301.84000000000003 C 163.078125 297.72400000000005 169.734375 292.236 183.046875 288.12 C 196.359375 284.004 203.015625 284.004 216.328125 281.26 C 229.640625 278.51599999999996 236.296875 277.14399999999995 249.609375 274.4 C 262.921875 271.656 269.578125 271.656 282.890625 267.54 C 296.203125 263.42400000000004 302.859375 257.936 316.171875 253.82 C 329.484375 249.704 336.140625 249.70399999999998 349.453125 246.95999999999998 C 362.765625 244.21599999999995 369.421875 242.844 382.734375 240.10000000000002 C 396.046875 237.35600000000005 402.703125 235.984 416.015625 233.24 C 429.328125 230.496 435.984375 229.12399999999997 449.296875 226.38 C 462.609375 223.63600000000002 469.265625 226.38 482.578125 219.52 C 495.890625 212.66000000000003 502.546875 201.684 515.859375 192.08 C 529.171875 182.47600000000003 535.828125 178.36 549.140625 171.5 C 562.453125 164.64 569.109375 163.268 582.421875 157.78 C 595.734375 152.292 602.390625 149.548 615.703125 144.06 C 629.015625 138.572 635.671875 134.45600000000002 648.984375 130.34 C 662.296875 126.224 668.953125 126.224 682.265625 123.47999999999999 C 695.578125 120.73599999999999 702.234375 120.73599999999998 715.546875 116.61999999999998 C 728.859375 112.50399999999996 735.515625 107.016 748.828125 102.9 C 762.140625 98.78400000000002 768.796875 100.15600000000002 782.109375 96.04000000000002 C 795.421875 91.924 802.078125 86.43599999999999 815.390625 82.32 C 828.703125 78.20399999999998 835.359375 79.57599999999998 848.671875 75.45999999999998 C 861.984375 71.344 868.640625 65.856 881.953125 61.74000000000001 C 895.265625 57.62400000000001 901.921875 57.624 915.234375 54.879999999999995 C 928.546875 52.13599999999999 935.203125 50.763999999999974 948.515625 48.01999999999998 C 961.828125 45.27599999999998 968.484375 45.276000000000025 981.796875 41.160000000000025 C 995.109375 37.044000000000025 1001.765625 31.555999999999994 1015.078125 27.439999999999998 C 1028.390625 23.323999999999998 1048.359375 20.58000000000004 1048.359375 20.58000000000004" class="highcharts-graph" stroke="#3cb878" stroke-width="2" stroke-linejoin="round" stroke-linecap="round"></path>
                              <path fill="none" d="M 6.640625 336.14 L 16.640625 336.14 C 16.640625 336.14 36.609375 332.024 49.921875 329.28 C 63.234375 326.53599999999994 69.890625 326.536 83.203125 322.42 C 96.515625 318.304 103.171875 312.816 116.484375 308.7 C 129.796875 304.584 136.453125 305.956 149.765625 301.84000000000003 C 163.078125 297.72400000000005 169.734375 292.236 183.046875 288.12 C 196.359375 284.004 203.015625 284.004 216.328125 281.26 C 229.640625 278.51599999999996 236.296875 277.14399999999995 249.609375 274.4 C 262.921875 271.656 269.578125 271.656 282.890625 267.54 C 296.203125 263.42400000000004 302.859375 257.936 316.171875 253.82 C 329.484375 249.704 336.140625 249.70399999999998 349.453125 246.95999999999998 C 362.765625 244.21599999999995 369.421875 242.844 382.734375 240.10000000000002 C 396.046875 237.35600000000005 402.703125 235.984 416.015625 233.24 C 429.328125 230.496 435.984375 229.12399999999997 449.296875 226.38 C 462.609375 223.63600000000002 469.265625 226.38 482.578125 219.52 C 495.890625 212.66000000000003 502.546875 201.684 515.859375 192.08 C 529.171875 182.47600000000003 535.828125 178.36 549.140625 171.5 C 562.453125 164.64 569.109375 163.268 582.421875 157.78 C 595.734375 152.292 602.390625 149.548 615.703125 144.06 C 629.015625 138.572 635.671875 134.45600000000002 648.984375 130.34 C 662.296875 126.224 668.953125 126.224 682.265625 123.47999999999999 C 695.578125 120.73599999999999 702.234375 120.73599999999998 715.546875 116.61999999999998 C 728.859375 112.50399999999996 735.515625 107.016 748.828125 102.9 C 762.140625 98.78400000000002 768.796875 100.15600000000002 782.109375 96.04000000000002 C 795.421875 91.924 802.078125 86.43599999999999 815.390625 82.32 C 828.703125 78.20399999999998 835.359375 79.57599999999998 848.671875 75.45999999999998 C 861.984375 71.344 868.640625 65.856 881.953125 61.74000000000001 C 895.265625 57.62400000000001 901.921875 57.624 915.234375 54.879999999999995 C 928.546875 52.13599999999999 935.203125 50.763999999999974 948.515625 48.01999999999998 C 961.828125 45.27599999999998 968.484375 45.276000000000025 981.796875 41.160000000000025 C 995.109375 37.044000000000025 1001.765625 31.555999999999994 1015.078125 27.439999999999998 C 1028.390625 23.323999999999998 1048.359375 20.58000000000004 1048.359375 20.58000000000004 L 1058.359375 20.58000000000004" stroke-linejoin="round" visibility="visible" stroke="rgba(192,192,192,0.0001)" stroke-width="22" class="highcharts-tracker"></path>
                           </g>
                           <g class="highcharts-markers highcharts-series-3 highcharts-spline-series highcharts-color-undefined highcharts-tracker" transform="translate(71,20) scale(1 1)"></g>
                        </g>
                        <text x="592" text-anchor="middle" class="highcharts-title" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                        <text x="592" text-anchor="middle" class="highcharts-subtitle" style="color:#666666;fill:#666666;" y="24"></text>
                        <g class="highcharts-axis-labels highcharts-xaxis-labels ">
                           <text x="87.640625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>15/…</tspan>
                              <title>15/12/2017</title>
                           </text>
                           <text x="120.921875" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>16/…</tspan>
                              <title>16/12/2017</title>
                           </text>
                           <text x="154.203125" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>17/…</tspan>
                              <title>17/12/2017</title>
                           </text>
                           <text x="187.484375" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>18/…</tspan>
                              <title>18/12/2017</title>
                           </text>
                           <text x="220.765625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>19/…</tspan>
                              <title>19/12/2017</title>
                           </text>
                           <text x="254.046875" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>20/…</tspan>
                              <title>20/12/2017</title>
                           </text>
                           <text x="287.328125" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>21/…</tspan>
                              <title>21/12/2017</title>
                           </text>
                           <text x="320.609375" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>22/…</tspan>
                              <title>22/12/2017</title>
                           </text>
                           <text x="353.890625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>23/…</tspan>
                              <title>23/12/2017</title>
                           </text>
                           <text x="387.171875" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>24/…</tspan>
                              <title>24/12/2017</title>
                           </text>
                           <text x="420.453125" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>25/…</tspan>
                              <title>25/12/2017</title>
                           </text>
                           <text x="453.734375" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>26/…</tspan>
                              <title>26/12/2017</title>
                           </text>
                           <text x="487.015625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>27/…</tspan>
                              <title>27/12/2017</title>
                           </text>
                           <text x="520.296875" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>28/…</tspan>
                              <title>28/12/2017</title>
                           </text>
                           <text x="553.578125" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>29/…</tspan>
                              <title>29/12/2017</title>
                           </text>
                           <text x="586.859375" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>30/…</tspan>
                              <title>30/12/2017</title>
                           </text>
                           <text x="620.140625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>31/…</tspan>
                              <title>31/12/2017</title>
                           </text>
                           <text x="653.421875" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>01/…</tspan>
                              <title>01/01/2018</title>
                           </text>
                           <text x="686.703125" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>02/…</tspan>
                              <title>02/01/2018</title>
                           </text>
                           <text x="719.984375" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>03/…</tspan>
                              <title>03/01/2018</title>
                           </text>
                           <text x="753.265625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>04/…</tspan>
                              <title>04/01/2018</title>
                           </text>
                           <text x="786.546875" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>05/…</tspan>
                              <title>05/01/2018</title>
                           </text>
                           <text x="819.828125" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>06/…</tspan>
                              <title>06/01/2018</title>
                           </text>
                           <text x="853.109375" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>07/…</tspan>
                              <title>07/01/2018</title>
                           </text>
                           <text x="886.390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>08/…</tspan>
                              <title>08/01/2018</title>
                           </text>
                           <text x="919.671875" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>09/…</tspan>
                              <title>09/01/2018</title>
                           </text>
                           <text x="952.953125" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>10/…</tspan>
                              <title>10/01/2018</title>
                           </text>
                           <text x="986.234375" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>11/…</tspan>
                              <title>11/01/2018</title>
                           </text>
                           <text x="1019.515625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>12/…</tspan>
                              <title>12/01/2018</title>
                           </text>
                           <text x="1052.796875" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>13/…</tspan>
                              <title>13/01/2018</title>
                           </text>
                           <text x="1086.078125" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>14/…</tspan>
                              <title>14/01/2018</title>
                           </text>
                           <text x="1119.359375" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan>15/…</tspan>
                              <title>15/01/2018</title>
                           </text>
                        </g>
                        <g class="highcharts-axis-labels highcharts-yaxis-labels ">
                           <text x="1151" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="start" transform="translate(0,0)" y="367" opacity="1">
                              <tspan>0</tspan>
                           </text>
                           <text x="1151" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="start" transform="translate(0,0)" y="298" opacity="1">
                              <tspan>0.1</tspan>
                           </text>
                           <text x="1151" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="start" transform="translate(0,0)" y="229" opacity="1">
                              <tspan>0.2</tspan>
                           </text>
                           <text x="1151" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="start" transform="translate(0,0)" y="161" opacity="1">
                              <tspan>0.3</tspan>
                           </text>
                           <text x="1151" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="start" transform="translate(0,0)" y="92" opacity="1">
                              <tspan>0.4</tspan>
                           </text>
                           <text x="1151" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="start" transform="translate(0,0)" y="24" opacity="1">
                              <tspan>0.5</tspan>
                           </text>
                        </g>
                        <g class="highcharts-axis-labels highcharts-yaxis-labels ">
                           <text x="56" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="end" transform="translate(0,0)" y="367" opacity="1">
                              <tspan>0</tspan>
                           </text>
                           <text x="56" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="end" transform="translate(0,0)" y="298" opacity="1">
                              <tspan>1500</tspan>
                           </text>
                           <text x="56" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="end" transform="translate(0,0)" y="229" opacity="1">
                              <tspan>3000</tspan>
                           </text>
                           <text x="56" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="end" transform="translate(0,0)" y="161" opacity="1">
                              <tspan>4500</tspan>
                           </text>
                           <text x="56" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="end" transform="translate(0,0)" y="92" opacity="1">
                              <tspan>6000</tspan>
                           </text>
                           <text x="56" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="end" transform="translate(0,0)" y="24" opacity="1">
                              <tspan>7500</tspan>
                           </text>
                        </g>
                        <g class="highcharts-axis-labels highcharts-yaxis-labels "></g>
                     </svg>
                  </div>
               </div>
               <div class="legend">
                  <span><i class="balls-icon balls-icon__fs"></i> Daily Draw</span>
                  <span><i class="balls-icon balls-icon__free"></i> Free tickets</span>
                  <span><i class="balls-icon balls-icon__rapid"></i> Rapid to the Moon</span>
                  <span><i class="balls-icon balls-icon__success"></i> Token holder's fund, BTC</span>
               </div>
               <p>20/12/2017 - added an additional option to buy tickets with TFL</p>
            </div>
            <script type="text/javascript">
               var TAB_MONTH_INFO = {
                   categories: ["15/12/2017","16/12/2017","17/12/2017","18/12/2017","19/12/2017","20/12/2017","21/12/2017","22/12/2017","23/12/2017","24/12/2017","25/12/2017","26/12/2017","27/12/2017","28/12/2017","29/12/2017","30/12/2017","31/12/2017","01/01/2018","02/01/2018","03/01/2018","04/01/2018","05/01/2018","06/01/2018","07/01/2018","08/01/2018","09/01/2018","10/01/2018","11/01/2018","12/01/2018","13/01/2018","14/01/2018","15/01/2018"],
                   star: [ 271,309,517,449,425,529,417,501,785,725,568,513,546,499,1093,2471,1542,796,784,1237,1098,1135,686,828,976,948,708,763,814,1044,1012,654],
                   free: [ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                   fund: [ 0.01,0.02,0.03,0.05,0.06,0.08,0.09,0.1,0.11,0.13,0.14,0.15,0.16,0.17,0.18,0.22,0.25,0.27,0.29,0.31,0.32,0.33,0.35,0.36,0.38,0.39,0.41,0.42,0.43,0.44,0.46,0.47],
                   rapid: [ 206,238,167,201,6680,639,716,771,670,1184,870,311,632,137,1207,952,917,2758,5053,2189,1142,554,475,1854,723,580,982,405,566,680,1015,473]
               };
            </script>
         </div>
         <div role="tabpanel" class="tab-pane fade" id="quarter" style="">
            <div class="sales_stat row-eq-height">
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-24">
                  <div class="sales_stat_wrapper">
                     <p class="heading-1">158660</p>
                     <p>Total tickets sold last quarter</p>
                     <a href="#" class="popover" onclick="return false" title="" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-html="true" data-content="<strong>50506</strong> - Daily Draw<br><strong>108154</strong> - Rapid to the Moon" data-original-title="Total tickets sold last quarter"></a>
                  </div>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-24">
                  <div class="sales_stat_wrapper">
                     <p class="heading-1">4040</p>
                     <p>Users participated</p>
                     <a href="#" class="popover" onclick="return false" title="" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-html="true" data-content="<strong>2878</strong> - Daily Draw<br><strong>1162</strong> - Rapid to the Moon" data-original-title="Users participated"></a>
                  </div>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-24">
                  <div class="sales_stat_wrapper">
                     <p class="heading-1">26995</p>
                     <p>Winners in the last quarter</p>
                     <a href="#" class="popover" onclick="return false" title="" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-html="true" data-content="<strong>5396</strong> - Daily Draw<br><strong>21599</strong> - Rapid to the Moon" data-original-title="Winners in the last quarter"></a>
                  </div>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-24">
                  <div class="sales_stat_wrapper">
                     <p class="heading-1">2.0047<sup>BTC</sup></p>
                     <p>TFL holders fund increased on</p>
                  </div>
               </div>
            </div>
            <div class="stat_graphic">
               <div id="graphic_quarter" data-highcharts-chart="2">
                  <div id="highcharts-awpca4c-8" class="highcharts-container " style="position: relative; overflow: hidden; width: 1183px; height: 400px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                     <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="1183" height="400" viewBox="0 0 1183 400">
                        <desc>Created with Highcharts 5.0.12</desc>
                        <defs>
                           <clipPath id="highcharts-awpca4c-9">
                              <rect x="0" y="0" width="1057" height="343" fill="none"></rect>
                           </clipPath>
                        </defs>
                        <rect fill="transparent" class="highcharts-background" x="0" y="0" width="1183" height="400" rx="0" ry="0"></rect>
                        <rect fill="none" class="highcharts-plot-background" x="79" y="20" width="1057" height="343"></rect>
                        <g class="highcharts-grid highcharts-xaxis-grid ">
                           <path fill="none" class="highcharts-grid-line" d="M 90.5 20 L 90.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 101.5 20 L 101.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 113.5 20 L 113.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 124.5 20 L 124.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 136.5 20 L 136.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 148.5 20 L 148.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 159.5 20 L 159.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 171.5 20 L 171.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 183.5 20 L 183.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 194.5 20 L 194.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 206.5 20 L 206.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 217.5 20 L 217.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 229.5 20 L 229.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 241.5 20 L 241.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 252.5 20 L 252.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 264.5 20 L 264.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 275.5 20 L 275.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 287.5 20 L 287.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 299.5 20 L 299.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 310.5 20 L 310.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 322.5 20 L 322.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 334.5 20 L 334.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 345.5 20 L 345.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 357.5 20 L 357.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 368.5 20 L 368.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 380.5 20 L 380.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 392.5 20 L 392.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 403.5 20 L 403.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 415.5 20 L 415.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 426.5 20 L 426.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 438.5 20 L 438.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 450.5 20 L 450.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 461.5 20 L 461.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 473.5 20 L 473.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 485.5 20 L 485.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 496.5 20 L 496.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 508.5 20 L 508.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 519.5 20 L 519.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 531.5 20 L 531.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 543.5 20 L 543.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 554.5 20 L 554.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 566.5 20 L 566.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 577.5 20 L 577.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 589.5 20 L 589.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 601.5 20 L 601.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 612.5 20 L 612.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 624.5 20 L 624.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 636.5 20 L 636.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 647.5 20 L 647.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 659.5 20 L 659.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 670.5 20 L 670.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 682.5 20 L 682.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 694.5 20 L 694.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 705.5 20 L 705.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 717.5 20 L 717.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 728.5 20 L 728.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 740.5 20 L 740.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 752.5 20 L 752.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 763.5 20 L 763.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 775.5 20 L 775.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 787.5 20 L 787.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 798.5 20 L 798.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 810.5 20 L 810.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 821.5 20 L 821.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 833.5 20 L 833.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 845.5 20 L 845.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 856.5 20 L 856.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 868.5 20 L 868.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 879.5 20 L 879.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 891.5 20 L 891.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 903.5 20 L 903.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 914.5 20 L 914.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 926.5 20 L 926.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 938.5 20 L 938.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 949.5 20 L 949.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 961.5 20 L 961.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 972.5 20 L 972.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 984.5 20 L 984.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 996.5 20 L 996.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 1007.5 20 L 1007.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 1019.5 20 L 1019.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 1030.5 20 L 1030.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 1042.5 20 L 1042.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 1054.5 20 L 1054.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 1065.5 20 L 1065.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 1077.5 20 L 1077.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 1089.5 20 L 1089.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 1100.5 20 L 1100.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 1112.5 20 L 1112.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 1123.5 20 L 1123.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 1135.5 20 L 1135.5 363" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 78.5 20 L 78.5 363" opacity="1"></path>
                        </g>
                        <g class="highcharts-grid highcharts-yaxis-grid ">
                           <path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 79 363.5 L 1136 363.5" opacity="1"></path>
                           <path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 79 294.5 L 1136 294.5" opacity="1"></path>
                           <path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 79 226.5 L 1136 226.5" opacity="1"></path>
                           <path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 79 157.5 L 1136 157.5" opacity="1"></path>
                           <path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 79 89.5 L 1136 89.5" opacity="1"></path>
                           <path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 79 19.5 L 1136 19.5" opacity="1"></path>
                        </g>
                        <g class="highcharts-grid highcharts-yaxis-grid ">
                           <path fill="none" class="highcharts-grid-line" d="M 79 363.5 L 1136 363.5" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 79 294.5 L 1136 294.5" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 79 226.5 L 1136 226.5" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 79 157.5 L 1136 157.5" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 79 89.5 L 1136 89.5" opacity="1"></path>
                           <path fill="none" class="highcharts-grid-line" d="M 79 20.5 L 1136 20.5" opacity="1"></path>
                        </g>
                        <g class="highcharts-grid highcharts-yaxis-grid "></g>
                        <rect fill="none" class="highcharts-plot-border" x="79" y="20" width="1057" height="343"></rect>
                        <g class="highcharts-axis highcharts-xaxis ">
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 90.5 363 L 90.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 101.5 363 L 101.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 113.5 363 L 113.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 124.5 363 L 124.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 136.5 363 L 136.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 148.5 363 L 148.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 159.5 363 L 159.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 171.5 363 L 171.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 183.5 363 L 183.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 194.5 363 L 194.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 206.5 363 L 206.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 217.5 363 L 217.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 229.5 363 L 229.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 241.5 363 L 241.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 252.5 363 L 252.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 264.5 363 L 264.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 275.5 363 L 275.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 287.5 363 L 287.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 299.5 363 L 299.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 310.5 363 L 310.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 322.5 363 L 322.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 334.5 363 L 334.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 345.5 363 L 345.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 357.5 363 L 357.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 368.5 363 L 368.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 380.5 363 L 380.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 392.5 363 L 392.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 403.5 363 L 403.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 415.5 363 L 415.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 426.5 363 L 426.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 438.5 363 L 438.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 450.5 363 L 450.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 461.5 363 L 461.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 473.5 363 L 473.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 485.5 363 L 485.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 496.5 363 L 496.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 508.5 363 L 508.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 519.5 363 L 519.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 531.5 363 L 531.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 543.5 363 L 543.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 554.5 363 L 554.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 566.5 363 L 566.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 577.5 363 L 577.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 589.5 363 L 589.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 601.5 363 L 601.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 612.5 363 L 612.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 624.5 363 L 624.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 636.5 363 L 636.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 647.5 363 L 647.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 659.5 363 L 659.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 670.5 363 L 670.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 682.5 363 L 682.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 694.5 363 L 694.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 705.5 363 L 705.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 717.5 363 L 717.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 728.5 363 L 728.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 740.5 363 L 740.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 752.5 363 L 752.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 763.5 363 L 763.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 775.5 363 L 775.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 787.5 363 L 787.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 798.5 363 L 798.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 810.5 363 L 810.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 821.5 363 L 821.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 833.5 363 L 833.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 845.5 363 L 845.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 856.5 363 L 856.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 868.5 363 L 868.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 879.5 363 L 879.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 891.5 363 L 891.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 903.5 363 L 903.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 914.5 363 L 914.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 926.5 363 L 926.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 938.5 363 L 938.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 949.5 363 L 949.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 961.5 363 L 961.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 972.5 363 L 972.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 984.5 363 L 984.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 996.5 363 L 996.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 1007.5 363 L 1007.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 1019.5 363 L 1019.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 1030.5 363 L 1030.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 1042.5 363 L 1042.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 1054.5 363 L 1054.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 1065.5 363 L 1065.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 1077.5 363 L 1077.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 1089.5 363 L 1089.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 1100.5 363 L 1100.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 1112.5 363 L 1112.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 1123.5 363 L 1123.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 1136.5 363 L 1136.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-tick" stroke="tranasparent" stroke-width="1" d="M 78.5 363 L 78.5 373" opacity="1"></path>
                           <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" d="M 79 363.5 L 1136 363.5"></path>
                        </g>
                        <g class="highcharts-axis highcharts-yaxis ">
                           <text x="1148" text-anchor="start" transform="translate(0,0)" class="highcharts-axis-title" style="color:#000;font-family:MuseoSansCyrl;;font-size:16px;font-weight:700;fill:#000;" y="20"></text>
                           <path fill="none" class="highcharts-axis-line" d="M 1136 20 L 1136 363"></path>
                        </g>
                        <g class="highcharts-axis highcharts-yaxis ">
                           <path fill="none" class="highcharts-axis-line" d="M 79 20 L 79 363"></path>
                        </g>
                        <g class="highcharts-axis highcharts-yaxis ">
                           <text x="9.59375" text-anchor="middle" transform="translate(0,0) rotate(270 9.59375 191.5)" class="highcharts-axis-title" style="color:#999;fill:#999;" y="191.5"></text>
                           <path fill="none" class="highcharts-axis-line" d="M 20 20 L 20 363"></path>
                        </g>
                        <g class="highcharts-series-group">
                           <g class="highcharts-series highcharts-series-0 highcharts-column-series highcharts-color-undefined highcharts-tracker" transform="translate(79,20) scale(1 1)" clip-path="url(#highcharts-awpca4c-9)">
                              <rect x="3" y="324" width="6" height="18" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="15" y="329" width="6" height="14" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="26" y="325" width="6" height="18" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="38" y="321" width="6" height="22" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="49" y="318" width="6" height="25" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="61" y="323" width="6" height="19" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="73" y="322" width="6" height="19" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="84" y="319" width="6" height="19" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="96" y="282" width="6" height="17" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="108" y="137" width="6" height="17" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="119" y="86" width="6" height="22" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="131" y="172" width="6" height="22" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="142" y="68" width="6" height="21" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="154" y="220" width="6" height="20" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="166" y="290" width="6" height="20" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="177" y="306" width="6" height="19" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="189" y="178" width="6" height="17" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="200" y="213" width="6" height="21" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="212" y="297" width="6" height="15" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="224" y="280" width="6" height="17" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="235" y="290" width="6" height="14" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="247" y="259" width="6" height="12" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="259" y="237" width="6" height="11" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="270" y="223" width="6" height="17" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="282" y="296" width="6" height="14" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="293" y="296" width="6" height="15" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="305" y="306" width="6" height="12" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="317" y="289" width="6" height="12" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="328" y="307" width="6" height="11" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="340" y="310" width="6" height="12" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="351" y="321" width="6" height="10" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="363" y="310" width="6" height="12" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="375" y="312" width="6" height="10" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="386" y="306" width="6" height="13" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="398" y="303" width="6" height="11" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="410" y="298" width="6" height="11" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="421" y="313" width="6" height="11" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="433" y="308" width="6" height="12" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="444" y="317" width="6" height="11" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="456" y="324" width="6" height="5" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="468" y="324" width="6" height="8" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="479" y="324" width="6" height="8" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="491" y="324" width="6" height="10" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="502" y="327" width="6" height="7" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="514" y="325" width="6" height="8" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="526" y="329" width="6" height="6" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="537" y="323" width="6" height="7" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="549" y="324" width="6" height="9" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="561" y="325" width="6" height="8" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="572" y="324" width="6" height="8" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="584" y="328" width="6" height="8" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="595" y="325" width="6" height="11" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="607" y="326" width="6" height="9" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="619" y="325" width="6" height="8" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="630" y="329" width="6" height="7" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="642" y="329" width="6" height="8" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="653" y="319" width="6" height="13" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="665" y="322" width="6" height="13" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="677" y="324" width="6" height="10" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="688" y="328" width="6" height="9" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="700" y="325" width="6" height="11" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="712" y="321" width="6" height="17" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="723" y="322" width="6" height="15" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="735" y="100" width="6" height="15" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="746" y="304" width="6" height="18" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="758" y="305" width="6" height="14" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="770" y="300" width="6" height="18" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="781" y="294" width="6" height="27" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="793" y="279" width="6" height="24" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="804" y="295" width="6" height="19" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="816" y="316" width="6" height="17" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="828" y="304" width="6" height="18" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="839" y="322" width="6" height="17" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="851" y="265" width="6" height="38" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="863" y="227" width="6" height="84" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="874" y="260" width="6" height="53" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="886" y="222" width="6" height="27" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="897" y="144" width="6" height="27" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="909" y="226" width="6" height="43" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="921" y="267" width="6" height="38" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="932" y="286" width="6" height="39" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="944" y="304" width="6" height="24" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="955" y="252" width="6" height="28" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="967" y="286" width="6" height="33" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="979" y="292" width="6" height="32" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="990" y="286" width="6" height="24" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="1002" y="304" width="6" height="26" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="1014" y="297" width="6" height="28" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="1025" y="285" width="6" height="36" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="1037" y="274" width="6" height="35" fill="#aa95d3" class="highcharts-point"></rect>
                              <rect x="1048" y="305" width="6" height="23" fill="#aa95d3" class="highcharts-point"></rect>
                           </g>
                           <g class="highcharts-markers highcharts-series-0 highcharts-column-series highcharts-color-undefined " transform="translate(79,20) scale(1 1)" clip-path="none"></g>
                           <g class="highcharts-series highcharts-series-1 highcharts-column-series highcharts-color-undefined highcharts-tracker" transform="translate(79,20) scale(1 1)" clip-path="url(#highcharts-awpca4c-9)">
                              <rect x="3" y="342" width="6" height="2" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="15" y="343" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="26" y="343" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="38" y="343" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="49" y="343" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="61" y="342" width="6" height="2" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="73" y="341" width="6" height="2" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="84" y="338" width="6" height="2" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="96" y="299" width="6" height="2" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="108" y="154" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="119" y="108" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="131" y="194" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="142" y="89" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="154" y="240" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="166" y="310" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="177" y="325" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="189" y="195" width="6" height="2" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="200" y="234" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="212" y="312" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="224" y="297" width="6" height="3" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="235" y="304" width="6" height="3" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="247" y="271" width="6" height="2" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="259" y="248" width="6" height="2" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="270" y="240" width="6" height="2" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="282" y="310" width="6" height="2" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="293" y="311" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="305" y="318" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="317" y="301" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="328" y="318" width="6" height="2" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="340" y="322" width="6" height="2" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="351" y="331" width="6" height="2" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="363" y="322" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="375" y="322" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="386" y="319" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="398" y="314" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="410" y="309" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="421" y="324" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="433" y="320" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="444" y="328" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="456" y="329" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="468" y="332" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="479" y="332" width="6" height="2" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="491" y="334" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="502" y="334" width="6" height="2" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="514" y="333" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="526" y="335" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="537" y="330" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="549" y="333" width="6" height="1" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="561" y="333" width="6" height="3" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="572" y="332" width="6" height="2" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="584" y="336" width="6" height="2" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="595" y="336" width="6" height="2" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="607" y="335" width="6" height="2" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="619" y="333" width="6" height="2" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="630" y="336" width="6" height="2" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="642" y="337" width="6" height="2" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="653" y="332" width="6" height="3" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="665" y="335" width="6" height="4" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="677" y="334" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="688" y="337" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="700" y="336" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="712" y="338" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="723" y="337" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="735" y="115" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="746" y="322" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="758" y="319" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="770" y="318" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="781" y="321" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="793" y="303" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="804" y="314" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="816" y="333" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="828" y="322" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="839" y="339" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="851" y="303" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="863" y="311" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="874" y="313" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="886" y="249" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="897" y="171" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="909" y="269" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="921" y="305" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="932" y="325" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="944" y="328" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="955" y="280" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="967" y="319" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="979" y="324" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="990" y="310" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="1002" y="330" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="1014" y="325" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="1025" y="321" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="1037" y="309" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                              <rect x="1048" y="328" width="6" height="0" fill="#d0c4e7" class="highcharts-point"></rect>
                           </g>
                           <g class="highcharts-markers highcharts-series-1 highcharts-column-series highcharts-color-undefined " transform="translate(79,20) scale(1 1)" clip-path="none"></g>
                           <g class="highcharts-series highcharts-series-2 highcharts-column-series highcharts-color-undefined highcharts-tracker" transform="translate(79,20) scale(1 1)" clip-path="url(#highcharts-awpca4c-9)">
                              <rect x="3" y="344" width="6" height="0" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="15" y="344" width="6" height="0" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="26" y="344" width="6" height="0" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="38" y="344" width="6" height="0" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="49" y="344" width="6" height="0" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="61" y="344" width="6" height="0" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="73" y="343" width="6" height="1" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="84" y="340" width="6" height="4" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="96" y="301" width="6" height="43" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="108" y="154" width="6" height="190" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="119" y="109" width="6" height="235" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="131" y="195" width="6" height="149" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="142" y="90" width="6" height="254" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="154" y="241" width="6" height="103" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="166" y="311" width="6" height="33" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="177" y="326" width="6" height="18" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="189" y="197" width="6" height="147" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="200" y="235" width="6" height="109" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="212" y="313" width="6" height="31" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="224" y="300" width="6" height="44" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="235" y="307" width="6" height="37" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="247" y="273" width="6" height="71" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="259" y="250" width="6" height="94" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="270" y="242" width="6" height="102" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="282" y="312" width="6" height="32" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="293" y="312" width="6" height="32" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="305" y="319" width="6" height="25" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="317" y="302" width="6" height="42" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="328" y="320" width="6" height="24" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="340" y="324" width="6" height="20" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="351" y="333" width="6" height="11" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="363" y="323" width="6" height="21" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="375" y="323" width="6" height="21" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="386" y="320" width="6" height="24" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="398" y="315" width="6" height="29" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="410" y="310" width="6" height="34" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="421" y="325" width="6" height="19" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="433" y="321" width="6" height="23" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="444" y="329" width="6" height="15" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="456" y="329" width="6" height="15" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="468" y="333" width="6" height="11" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="479" y="334" width="6" height="10" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="491" y="335" width="6" height="9" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="502" y="336" width="6" height="8" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="514" y="334" width="6" height="10" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="526" y="336" width="6" height="8" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="537" y="331" width="6" height="13" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="549" y="334" width="6" height="10" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="561" y="336" width="6" height="8" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="572" y="334" width="6" height="10" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="584" y="338" width="6" height="6" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="595" y="338" width="6" height="6" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="607" y="337" width="6" height="7" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="619" y="335" width="6" height="9" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="630" y="338" width="6" height="6" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="642" y="339" width="6" height="5" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="653" y="335" width="6" height="9" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="665" y="339" width="6" height="5" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="677" y="334" width="6" height="10" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="688" y="337" width="6" height="7" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="700" y="336" width="6" height="8" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="712" y="338" width="6" height="6" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="723" y="337" width="6" height="7" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="735" y="115" width="6" height="229" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="746" y="322" width="6" height="22" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="758" y="319" width="6" height="25" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="770" y="318" width="6" height="26" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="781" y="321" width="6" height="23" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="793" y="303" width="6" height="41" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="804" y="314" width="6" height="30" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="816" y="333" width="6" height="11" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="828" y="322" width="6" height="22" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="839" y="339" width="6" height="5" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="851" y="303" width="6" height="41" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="863" y="311" width="6" height="33" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="874" y="313" width="6" height="31" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="886" y="249" width="6" height="95" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="897" y="171" width="6" height="173" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="909" y="269" width="6" height="75" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="921" y="305" width="6" height="39" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="932" y="325" width="6" height="19" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="944" y="328" width="6" height="16" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="955" y="280" width="6" height="64" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="967" y="319" width="6" height="25" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="979" y="324" width="6" height="20" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="990" y="310" width="6" height="34" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="1002" y="330" width="6" height="14" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="1014" y="325" width="6" height="19" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="1025" y="321" width="6" height="23" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="1037" y="309" width="6" height="35" fill="#855fa8" class="highcharts-point"></rect>
                              <rect x="1048" y="328" width="6" height="16" fill="#855fa8" class="highcharts-point"></rect>
                           </g>
                           <g class="highcharts-markers highcharts-series-2 highcharts-column-series highcharts-color-undefined " transform="translate(79,20) scale(1 1)" clip-path="none"></g>
                           <g class="highcharts-series highcharts-series-3 highcharts-spline-series highcharts-color-undefined " transform="translate(79,20) scale(1 1)" clip-path="url(#highcharts-awpca4c-9)">
                              <path fill="none" d="M 5.8076923076923 338.884 C 5.8076923076923 338.884 12.77692307692312 336.14 17.423076923077 334.76800000000003 C 22.069230769231 333.396 24.392307692308 333.39600000000013 29.038461538462 332.024 C 33.6846153846156 330.65200000000004 36.0076923076924 329.82879999999994 40.653846153846 327.908 C 45.3 325.9872 47.623076923076994 325.4384000000002 52.269230769231 322.42 C 56.915384615384596 319.40160000000014 59.2384615384614 315.5599999999999 63.884615384615 312.81600000000003 C 68.530769230769 310.0719999999999 70.853846153846 310.6208 75.5 308.7 C 80.146153846154 306.77919999999995 82.469230769231 305.13280000000003 87.115384615385 303.212 C 91.7615384615386 301.29120000000006 94.0846153846154 300.7424000000002 98.730769230769 299.096 C 103.37692307692141 297.44960000000026 105.69999999999759 296.90079999999926 110.34615384615 294.98 C 114.99230769230601 293.0591999999993 117.315384615384 291.687200000001 121.96153846154 289.492 C 126.60769230769202 287.296800000001 128.930769230768 285.9247999999992 133.57692307692 284.004 C 138.223076923076 282.0831999999992 140.546153846154 281.8088000000009 145.19230769231 279.88800000000003 C 149.838461538462 277.96720000000084 152.161538461538 276.59519999999907 156.80769230769 274.4 C 161.453846153846 272.204799999999 163.776923076924 270.8328000000009 168.42307692308 268.91200000000003 C 173.069230769232 266.99120000000084 175.39230769230798 266.7167999999992 180.03846153846 264.796 C 184.684615384616 262.87519999999915 187.007692307694 261.50320000000096 191.65384615385 259.308 C 196.300000000002 257.11280000000096 198.623076923078 255.74079999999918 203.26923076923 253.82 C 207.915384615386 251.89919999999918 210.23846153846398 251.35040000000072 214.88461538462 249.704 C 219.53076923077197 248.05760000000075 221.853846153848 247.23440000000002 226.5 245.58800000000002 C 231.146153846152 243.94160000000005 233.46923076922803 243.39279999999917 238.11538461538 241.472 C 242.76153846153602 239.55119999999917 245.084615384614 237.6304000000007 249.73076923077 235.984 C 254.376923076922 234.33760000000072 256.699999999998 234.61199999999943 261.34615384615 233.24 C 265.992307692306 231.86799999999943 268.315384615384 231.04480000000083 272.96153846154 229.12400000000002 C 277.607692307692 227.20320000000086 279.930769230768 225.2823999999993 284.57692307692 223.63600000000002 C 289.22307692307606 221.98959999999929 291.54615384615397 222.81280000000083 296.19230769231 220.892 C 300.83846153846196 218.97120000000086 303.16153846153804 215.95279999999917 307.80769230769 214.032 C 312.45384615384603 212.11119999999917 314.77692307692394 212.3856000000005 319.42307692308 211.288 C 324.069230769232 210.1904000000005 326.392307692308 209.64159999999956 331.03846153846 208.544 C 335.684615384616 207.44639999999953 338.007692307694 207.1720000000006 342.65384615385 205.8 C 347.300000000002 204.4280000000006 349.623076923078 203.05599999999941 354.26923076923 201.684 C 358.91538461538596 200.31199999999941 361.238461538464 200.03760000000045 365.88461538462 198.94 C 370.530769230772 197.84240000000048 372.853846153848 197.2936 377.5 196.196 C 382.146153846152 195.0984 384.469230769228 194.54959999999951 389.11538461538 193.452 C 393.761538461536 192.35439999999952 396.08461538461404 192.08000000000058 400.73076923077 190.708 C 405.376923076922 189.33600000000058 407.699999999998 187.96399999999943 412.34615384615 186.592 C 416.992307692306 185.21999999999943 419.315384615384 184.94560000000047 423.96153846154 183.848 C 428.607692307692 182.7504000000005 430.93076923076796 182.20159999999953 435.57692307692 181.104 C 440.22307692307606 180.00639999999953 442.5461538461541 179.18320000000037 447.19230769231 178.36 C 451.83846153846207 177.53680000000037 454.16153846153793 176.98800000000003 458.80769230769 176.98800000000003 C 463.4538461538459 176.98800000000003 465.77692307692394 176.98800000000003 470.42307692308 176.98800000000003 C 475.06923076923204 176.98800000000003 477.392307692308 175.34159999999954 482.03846153846 174.24400000000003 C 486.684615384616 173.14639999999955 489.007692307694 172.32320000000033 493.65384615385 171.5 C 498.300000000002 170.67680000000036 500.623076923078 170.6767999999998 505.26923076923 170.12800000000001 C 509.915384615386 169.57919999999976 512.238461538464 169.30480000000026 516.88461538462 168.756 C 521.5307692307721 168.20720000000023 523.853846153848 167.93280000000001 528.5 167.38400000000001 C 533.146153846152 166.83520000000004 535.4692307692279 166.56079999999977 540.11538461538 166.012 C 544.7615384615359 165.46319999999974 547.0846153846139 165.46320000000037 551.73076923077 164.64000000000001 C 556.3769230769219 163.81680000000037 558.699999999998 162.99359999999953 563.34615384615 161.89600000000002 C 567.992307692306 160.79839999999953 570.315384615384 159.97520000000037 574.96153846154 159.15200000000002 C 579.607692307692 158.32880000000037 581.9307692307681 158.32879999999975 586.57692307692 157.78 C 591.2230769230761 157.23119999999975 593.5461538461541 157.23120000000037 598.19230769231 156.40800000000002 C 602.8384615384621 155.58480000000037 605.1615384615379 154.7615999999996 609.80769230769 153.66400000000004 C 614.4538461538459 152.56639999999956 616.7769230769239 151.7432000000004 621.42307692308 150.92000000000002 C 626.0692307692319 150.09680000000037 628.392307692308 150.09679999999977 633.03846153846 149.54800000000003 C 637.684615384616 148.99919999999977 640.007692307694 148.72480000000027 644.65384615385 148.17600000000002 C 649.300000000002 147.62720000000027 651.6230769230781 147.3527999999998 656.26923076923 146.80400000000003 C 660.9153846153861 146.25519999999977 663.2384615384641 146.25520000000037 667.88461538462 145.43200000000002 C 672.5307692307721 144.60880000000037 674.853846153848 143.5112 679.5 142.68800000000002 C 684.146153846152 141.8648 686.4692307692279 141.86479999999978 691.11538461538 141.31600000000003 C 695.7615384615359 140.7671999999998 698.0846153846139 140.49280000000027 702.73076923077 139.94400000000002 C 707.3769230769219 139.39520000000024 709.699999999998 139.39519999999968 714.34615384615 138.57200000000003 C 718.992307692306 137.74879999999965 721.315384615384 136.6512000000004 725.96153846154 135.828 C 730.607692307692 135.00480000000036 732.9307692307681 135.27919999999966 737.57692307692 134.45600000000002 C 742.2230769230761 133.63279999999966 744.5461538461541 132.53520000000037 749.19230769231 131.71200000000002 C 753.8384615384621 130.88880000000037 756.1615384615379 130.88879999999978 760.80769230769 130.34 C 765.4538461538459 129.79119999999978 767.7769230769239 129.51680000000027 772.42307692308 128.96800000000002 C 777.0692307692319 128.41920000000025 779.392307692308 128.41919999999965 784.03846153846 127.596 C 788.684615384616 126.77279999999963 791.007692307694 125.67520000000036 795.65384615385 124.852 C 800.300000000002 124.02880000000037 802.6230769230781 124.02879999999976 807.26923076923 123.48000000000002 C 811.9153846153861 122.93119999999978 814.2384615384641 122.65680000000025 818.88461538462 122.108 C 823.5307692307721 121.55920000000023 825.8538461538479 121.28479999999998 830.5 120.73599999999999 C 835.1461538461521 120.18719999999999 837.4692307692279 119.91279999999979 842.11538461538 119.36400000000003 C 846.7615384615359 118.8151999999998 849.0846153846139 119.36400000000003 853.73076923077 117.99200000000002 C 858.3769230769219 116.62 860.699999999998 114.42479999999917 865.34615384615 112.50400000000002 C 869.992307692306 110.58319999999917 872.315384615384 109.76000000000063 876.96153846154 108.38800000000003 C 881.607692307692 107.01600000000064 883.9307692307681 106.74159999999955 888.57692307692 105.64400000000003 C 893.2230769230761 104.54639999999955 895.5461538461541 103.9976000000005 900.19230769231 102.90000000000003 C 904.8384615384621 101.80240000000049 907.1615384615379 100.97919999999966 911.80769230769 100.156 C 916.4538461538459 99.33279999999965 918.7769230769239 99.33280000000026 923.42307692308 98.78400000000002 C 928.0692307692319 98.23520000000026 930.392307692308 98.23519999999964 935.03846153846 97.412 C 939.684615384616 96.58879999999964 942.007692307694 95.49120000000038 946.65384615385 94.668 C 951.300000000002 93.84480000000038 953.6230769230781 94.11919999999965 958.26923076923 93.29600000000002 C 962.9153846153861 92.47279999999967 965.2384615384641 91.37520000000036 969.88461538462 90.55200000000002 C 974.5307692307721 89.72880000000036 976.8538461538479 90.0032 981.5 89.18 C 986.1461538461521 88.35679999999999 988.4692307692279 87.25919999999857 993.11538461538 86.43599999999998 C 997.761538461548 85.61279999999857 1000.084615384632 85.61280000000049 1004.7307692308 85.06400000000002 C 1009.37692307696 84.5152000000005 1011.7000000000401 84.24080000000235 1016.3461538462 83.69200000000001 C 1020.99230769232 83.14320000000237 1023.31538461538 83.14319999999651 1027.9615384615 82.32000000000005 C 1032.6076923076598 81.49679999999651 1034.9307692307398 80.39920000000005 1039.5769230769 79.57600000000002 C 1044.2230769230598 78.75280000000004 1051.1923076923 78.20400000000001 1051.1923076923 78.20400000000001" class="highcharts-graph" stroke="#3cb878" stroke-width="2" stroke-linejoin="round" stroke-linecap="round"></path>
                              <path fill="none" d="M -4.1923076923077 338.884 L 5.8076923076923 338.884 C 5.8076923076923 338.884 12.77692307692312 336.14 17.423076923077 334.76800000000003 C 22.069230769231 333.396 24.392307692308 333.39600000000013 29.038461538462 332.024 C 33.6846153846156 330.65200000000004 36.0076923076924 329.82879999999994 40.653846153846 327.908 C 45.3 325.9872 47.623076923076994 325.4384000000002 52.269230769231 322.42 C 56.915384615384596 319.40160000000014 59.2384615384614 315.5599999999999 63.884615384615 312.81600000000003 C 68.530769230769 310.0719999999999 70.853846153846 310.6208 75.5 308.7 C 80.146153846154 306.77919999999995 82.469230769231 305.13280000000003 87.115384615385 303.212 C 91.7615384615386 301.29120000000006 94.0846153846154 300.7424000000002 98.730769230769 299.096 C 103.37692307692141 297.44960000000026 105.69999999999759 296.90079999999926 110.34615384615 294.98 C 114.99230769230601 293.0591999999993 117.315384615384 291.687200000001 121.96153846154 289.492 C 126.60769230769202 287.296800000001 128.930769230768 285.9247999999992 133.57692307692 284.004 C 138.223076923076 282.0831999999992 140.546153846154 281.8088000000009 145.19230769231 279.88800000000003 C 149.838461538462 277.96720000000084 152.161538461538 276.59519999999907 156.80769230769 274.4 C 161.453846153846 272.204799999999 163.776923076924 270.8328000000009 168.42307692308 268.91200000000003 C 173.069230769232 266.99120000000084 175.39230769230798 266.7167999999992 180.03846153846 264.796 C 184.684615384616 262.87519999999915 187.007692307694 261.50320000000096 191.65384615385 259.308 C 196.300000000002 257.11280000000096 198.623076923078 255.74079999999918 203.26923076923 253.82 C 207.915384615386 251.89919999999918 210.23846153846398 251.35040000000072 214.88461538462 249.704 C 219.53076923077197 248.05760000000075 221.853846153848 247.23440000000002 226.5 245.58800000000002 C 231.146153846152 243.94160000000005 233.46923076922803 243.39279999999917 238.11538461538 241.472 C 242.76153846153602 239.55119999999917 245.084615384614 237.6304000000007 249.73076923077 235.984 C 254.376923076922 234.33760000000072 256.699999999998 234.61199999999943 261.34615384615 233.24 C 265.992307692306 231.86799999999943 268.315384615384 231.04480000000083 272.96153846154 229.12400000000002 C 277.607692307692 227.20320000000086 279.930769230768 225.2823999999993 284.57692307692 223.63600000000002 C 289.22307692307606 221.98959999999929 291.54615384615397 222.81280000000083 296.19230769231 220.892 C 300.83846153846196 218.97120000000086 303.16153846153804 215.95279999999917 307.80769230769 214.032 C 312.45384615384603 212.11119999999917 314.77692307692394 212.3856000000005 319.42307692308 211.288 C 324.069230769232 210.1904000000005 326.392307692308 209.64159999999956 331.03846153846 208.544 C 335.684615384616 207.44639999999953 338.007692307694 207.1720000000006 342.65384615385 205.8 C 347.300000000002 204.4280000000006 349.623076923078 203.05599999999941 354.26923076923 201.684 C 358.91538461538596 200.31199999999941 361.238461538464 200.03760000000045 365.88461538462 198.94 C 370.530769230772 197.84240000000048 372.853846153848 197.2936 377.5 196.196 C 382.146153846152 195.0984 384.469230769228 194.54959999999951 389.11538461538 193.452 C 393.761538461536 192.35439999999952 396.08461538461404 192.08000000000058 400.73076923077 190.708 C 405.376923076922 189.33600000000058 407.699999999998 187.96399999999943 412.34615384615 186.592 C 416.992307692306 185.21999999999943 419.315384615384 184.94560000000047 423.96153846154 183.848 C 428.607692307692 182.7504000000005 430.93076923076796 182.20159999999953 435.57692307692 181.104 C 440.22307692307606 180.00639999999953 442.5461538461541 179.18320000000037 447.19230769231 178.36 C 451.83846153846207 177.53680000000037 454.16153846153793 176.98800000000003 458.80769230769 176.98800000000003 C 463.4538461538459 176.98800000000003 465.77692307692394 176.98800000000003 470.42307692308 176.98800000000003 C 475.06923076923204 176.98800000000003 477.392307692308 175.34159999999954 482.03846153846 174.24400000000003 C 486.684615384616 173.14639999999955 489.007692307694 172.32320000000033 493.65384615385 171.5 C 498.300000000002 170.67680000000036 500.623076923078 170.6767999999998 505.26923076923 170.12800000000001 C 509.915384615386 169.57919999999976 512.238461538464 169.30480000000026 516.88461538462 168.756 C 521.5307692307721 168.20720000000023 523.853846153848 167.93280000000001 528.5 167.38400000000001 C 533.146153846152 166.83520000000004 535.4692307692279 166.56079999999977 540.11538461538 166.012 C 544.7615384615359 165.46319999999974 547.0846153846139 165.46320000000037 551.73076923077 164.64000000000001 C 556.3769230769219 163.81680000000037 558.699999999998 162.99359999999953 563.34615384615 161.89600000000002 C 567.992307692306 160.79839999999953 570.315384615384 159.97520000000037 574.96153846154 159.15200000000002 C 579.607692307692 158.32880000000037 581.9307692307681 158.32879999999975 586.57692307692 157.78 C 591.2230769230761 157.23119999999975 593.5461538461541 157.23120000000037 598.19230769231 156.40800000000002 C 602.8384615384621 155.58480000000037 605.1615384615379 154.7615999999996 609.80769230769 153.66400000000004 C 614.4538461538459 152.56639999999956 616.7769230769239 151.7432000000004 621.42307692308 150.92000000000002 C 626.0692307692319 150.09680000000037 628.392307692308 150.09679999999977 633.03846153846 149.54800000000003 C 637.684615384616 148.99919999999977 640.007692307694 148.72480000000027 644.65384615385 148.17600000000002 C 649.300000000002 147.62720000000027 651.6230769230781 147.3527999999998 656.26923076923 146.80400000000003 C 660.9153846153861 146.25519999999977 663.2384615384641 146.25520000000037 667.88461538462 145.43200000000002 C 672.5307692307721 144.60880000000037 674.853846153848 143.5112 679.5 142.68800000000002 C 684.146153846152 141.8648 686.4692307692279 141.86479999999978 691.11538461538 141.31600000000003 C 695.7615384615359 140.7671999999998 698.0846153846139 140.49280000000027 702.73076923077 139.94400000000002 C 707.3769230769219 139.39520000000024 709.699999999998 139.39519999999968 714.34615384615 138.57200000000003 C 718.992307692306 137.74879999999965 721.315384615384 136.6512000000004 725.96153846154 135.828 C 730.607692307692 135.00480000000036 732.9307692307681 135.27919999999966 737.57692307692 134.45600000000002 C 742.2230769230761 133.63279999999966 744.5461538461541 132.53520000000037 749.19230769231 131.71200000000002 C 753.8384615384621 130.88880000000037 756.1615384615379 130.88879999999978 760.80769230769 130.34 C 765.4538461538459 129.79119999999978 767.7769230769239 129.51680000000027 772.42307692308 128.96800000000002 C 777.0692307692319 128.41920000000025 779.392307692308 128.41919999999965 784.03846153846 127.596 C 788.684615384616 126.77279999999963 791.007692307694 125.67520000000036 795.65384615385 124.852 C 800.300000000002 124.02880000000037 802.6230769230781 124.02879999999976 807.26923076923 123.48000000000002 C 811.9153846153861 122.93119999999978 814.2384615384641 122.65680000000025 818.88461538462 122.108 C 823.5307692307721 121.55920000000023 825.8538461538479 121.28479999999998 830.5 120.73599999999999 C 835.1461538461521 120.18719999999999 837.4692307692279 119.91279999999979 842.11538461538 119.36400000000003 C 846.7615384615359 118.8151999999998 849.0846153846139 119.36400000000003 853.73076923077 117.99200000000002 C 858.3769230769219 116.62 860.699999999998 114.42479999999917 865.34615384615 112.50400000000002 C 869.992307692306 110.58319999999917 872.315384615384 109.76000000000063 876.96153846154 108.38800000000003 C 881.607692307692 107.01600000000064 883.9307692307681 106.74159999999955 888.57692307692 105.64400000000003 C 893.2230769230761 104.54639999999955 895.5461538461541 103.9976000000005 900.19230769231 102.90000000000003 C 904.8384615384621 101.80240000000049 907.1615384615379 100.97919999999966 911.80769230769 100.156 C 916.4538461538459 99.33279999999965 918.7769230769239 99.33280000000026 923.42307692308 98.78400000000002 C 928.0692307692319 98.23520000000026 930.392307692308 98.23519999999964 935.03846153846 97.412 C 939.684615384616 96.58879999999964 942.007692307694 95.49120000000038 946.65384615385 94.668 C 951.300000000002 93.84480000000038 953.6230769230781 94.11919999999965 958.26923076923 93.29600000000002 C 962.9153846153861 92.47279999999967 965.2384615384641 91.37520000000036 969.88461538462 90.55200000000002 C 974.5307692307721 89.72880000000036 976.8538461538479 90.0032 981.5 89.18 C 986.1461538461521 88.35679999999999 988.4692307692279 87.25919999999857 993.11538461538 86.43599999999998 C 997.761538461548 85.61279999999857 1000.084615384632 85.61280000000049 1004.7307692308 85.06400000000002 C 1009.37692307696 84.5152000000005 1011.7000000000401 84.24080000000235 1016.3461538462 83.69200000000001 C 1020.99230769232 83.14320000000237 1023.31538461538 83.14319999999651 1027.9615384615 82.32000000000005 C 1032.6076923076598 81.49679999999651 1034.9307692307398 80.39920000000005 1039.5769230769 79.57600000000002 C 1044.2230769230598 78.75280000000004 1051.1923076923 78.20400000000001 1051.1923076923 78.20400000000001 L 1061.1923076923 78.20400000000001" stroke-linejoin="round" visibility="visible" stroke="rgba(192,192,192,0.0001)" stroke-width="22" class="highcharts-tracker"></path>
                           </g>
                           <g class="highcharts-markers highcharts-series-3 highcharts-spline-series highcharts-color-undefined highcharts-tracker" transform="translate(79,20) scale(1 1)"></g>
                        </g>
                        <text x="592" text-anchor="middle" class="highcharts-title" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                        <text x="592" text-anchor="middle" class="highcharts-subtitle" style="color:#666666;fill:#666666;" y="24"></text>
                        <g class="highcharts-axis-labels highcharts-xaxis-labels ">
                           <text x="84.8076923076923" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>17/10/2017</title>
                           </text>
                           <text x="96.42307692307692" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>18/10/2017</title>
                           </text>
                           <text x="108.03846153846153" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>19/10/2017</title>
                           </text>
                           <text x="119.65384615384615" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>20/10/2017</title>
                           </text>
                           <text x="131.26923076923075" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>21/10/2017</title>
                           </text>
                           <text x="142.88461538461536" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>22/10/2017</title>
                           </text>
                           <text x="154.5" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>23/10/2017</title>
                           </text>
                           <text x="166.11538461538458" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>24/10/2017</title>
                           </text>
                           <text x="177.73076923076923" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>25/10/2017</title>
                           </text>
                           <text x="189.3461538461538" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>26/10/2017</title>
                           </text>
                           <text x="200.96153846153845" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>27/10/2017</title>
                           </text>
                           <text x="212.57692307692307" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>28/10/2017</title>
                           </text>
                           <text x="224.19230769230768" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>29/10/2017</title>
                           </text>
                           <text x="235.8076923076923" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>30/10/2017</title>
                           </text>
                           <text x="247.4230769230769" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>31/10/2017</title>
                           </text>
                           <text x="259.0384615384615" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>01/11/2017</title>
                           </text>
                           <text x="270.65384615384613" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>02/11/2017</title>
                           </text>
                           <text x="282.2692307692308" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>03/11/2017</title>
                           </text>
                           <text x="293.8846153846154" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>04/11/2017</title>
                           </text>
                           <text x="305.5" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>05/11/2017</title>
                           </text>
                           <text x="317.1153846153846" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>06/11/2017</title>
                           </text>
                           <text x="328.7307692307692" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>07/11/2017</title>
                           </text>
                           <text x="340.3461538461538" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>08/11/2017</title>
                           </text>
                           <text x="351.96153846153845" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>09/11/2017</title>
                           </text>
                           <text x="363.5769230769231" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>10/11/2017</title>
                           </text>
                           <text x="375.1923076923077" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>11/11/2017</title>
                           </text>
                           <text x="386.8076923076923" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>12/11/2017</title>
                           </text>
                           <text x="398.4230769230769" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>13/11/2017</title>
                           </text>
                           <text x="410.03846153846155" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>14/11/2017</title>
                           </text>
                           <text x="421.65384615384613" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>15/11/2017</title>
                           </text>
                           <text x="433.2692307692308" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>16/11/2017</title>
                           </text>
                           <text x="444.88461538461536" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>17/11/2017</title>
                           </text>
                           <text x="456.5" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>18/11/2017</title>
                           </text>
                           <text x="468.1153846153846" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>19/11/2017</title>
                           </text>
                           <text x="479.7307692307692" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>20/11/2017</title>
                           </text>
                           <text x="491.3461538461538" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>21/11/2017</title>
                           </text>
                           <text x="502.96153846153845" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>22/11/2017</title>
                           </text>
                           <text x="514.5769230769231" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>23/11/2017</title>
                           </text>
                           <text x="526.1923076923077" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>24/11/2017</title>
                           </text>
                           <text x="537.8076923076923" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>25/11/2017</title>
                           </text>
                           <text x="549.423076923077" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>26/11/2017</title>
                           </text>
                           <text x="561.0384615384615" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>27/11/2017</title>
                           </text>
                           <text x="572.6538461538462" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>28/11/2017</title>
                           </text>
                           <text x="584.2692307692308" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>29/11/2017</title>
                           </text>
                           <text x="595.8846153846154" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>30/11/2017</title>
                           </text>
                           <text x="607.5" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>01/12/2017</title>
                           </text>
                           <text x="619.1153846153846" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>02/12/2017</title>
                           </text>
                           <text x="630.7307692307692" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>03/12/2017</title>
                           </text>
                           <text x="642.3461538461538" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>04/12/2017</title>
                           </text>
                           <text x="653.9615384615385" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>05/12/2017</title>
                           </text>
                           <text x="665.5769230769231" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>06/12/2017</title>
                           </text>
                           <text x="677.1923076923076" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>07/12/2017</title>
                           </text>
                           <text x="688.8076923076923" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>08/12/2017</title>
                           </text>
                           <text x="700.4230769230769" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>09/12/2017</title>
                           </text>
                           <text x="712.0384615384615" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>10/12/2017</title>
                           </text>
                           <text x="723.6538461538461" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>11/12/2017</title>
                           </text>
                           <text x="735.2692307692307" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>12/12/2017</title>
                           </text>
                           <text x="746.8846153846154" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>13/12/2017</title>
                           </text>
                           <text x="758.5" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>14/12/2017</title>
                           </text>
                           <text x="770.1153846153846" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>15/12/2017</title>
                           </text>
                           <text x="781.7307692307692" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>16/12/2017</title>
                           </text>
                           <text x="793.3461538461538" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>17/12/2017</title>
                           </text>
                           <text x="804.9615384615385" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>18/12/2017</title>
                           </text>
                           <text x="816.5769230769231" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>19/12/2017</title>
                           </text>
                           <text x="828.1923076923076" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>20/12/2017</title>
                           </text>
                           <text x="839.8076923076923" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>21/12/2017</title>
                           </text>
                           <text x="851.4230769230769" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>22/12/2017</title>
                           </text>
                           <text x="863.0384615384615" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>23/12/2017</title>
                           </text>
                           <text x="874.6538461538461" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>24/12/2017</title>
                           </text>
                           <text x="886.2692307692307" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>25/12/2017</title>
                           </text>
                           <text x="897.8846153846154" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>26/12/2017</title>
                           </text>
                           <text x="909.5" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>27/12/2017</title>
                           </text>
                           <text x="921.1153846153846" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>28/12/2017</title>
                           </text>
                           <text x="932.7307692307692" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>29/12/2017</title>
                           </text>
                           <text x="944.3461538461538" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>30/12/2017</title>
                           </text>
                           <text x="955.9615384615385" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>31/12/2017</title>
                           </text>
                           <text x="967.5769230769231" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>01/01/2018</title>
                           </text>
                           <text x="979.1923076923076" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>02/01/2018</title>
                           </text>
                           <text x="990.8076923076923" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>03/01/2018</title>
                           </text>
                           <text x="1002.4230769230769" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>04/01/2018</title>
                           </text>
                           <text x="1014.0384615384615" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>05/01/2018</title>
                           </text>
                           <text x="1025.653846153846" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>06/01/2018</title>
                           </text>
                           <text x="1037.2692307692305" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>07/01/2018</title>
                           </text>
                           <text x="1048.8846153846152" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>08/01/2018</title>
                           </text>
                           <text x="1060.5" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>09/01/2018</title>
                           </text>
                           <text x="1072.1153846153843" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>10/01/2018</title>
                           </text>
                           <text x="1083.730769230769" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>11/01/2018</title>
                           </text>
                           <text x="1095.3461538461538" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>12/01/2018</title>
                           </text>
                           <text x="1106.9615384615383" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>13/01/2018</title>
                           </text>
                           <text x="1118.576923076923" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>14/01/2018</title>
                           </text>
                           <text x="1130.1923076923076" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="382" opacity="1">
                              <tspan></tspan>
                              <title>15/01/2018</title>
                           </text>
                        </g>
                        <g class="highcharts-axis-labels highcharts-yaxis-labels ">
                           <text x="1151" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="start" transform="translate(0,0)" y="367" opacity="1">
                              <tspan>0</tspan>
                           </text>
                           <text x="1151" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="start" transform="translate(0,0)" y="298" opacity="1">
                              <tspan>0.5</tspan>
                           </text>
                           <text x="1151" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="start" transform="translate(0,0)" y="229" opacity="1">
                              <tspan>1</tspan>
                           </text>
                           <text x="1151" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="start" transform="translate(0,0)" y="161" opacity="1">
                              <tspan>1.5</tspan>
                           </text>
                           <text x="1151" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="start" transform="translate(0,0)" y="92" opacity="1">
                              <tspan>2</tspan>
                           </text>
                           <text x="1151" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="start" transform="translate(0,0)" y="24" opacity="1">
                              <tspan>2.5</tspan>
                           </text>
                        </g>
                        <g class="highcharts-axis-labels highcharts-yaxis-labels ">
                           <text x="64" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="end" transform="translate(0,0)" y="367" opacity="1">
                              <tspan>0</tspan>
                           </text>
                           <text x="64" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="end" transform="translate(0,0)" y="298" opacity="1">
                              <tspan>2000</tspan>
                           </text>
                           <text x="64" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="end" transform="translate(0,0)" y="229" opacity="1">
                              <tspan>4000</tspan>
                           </text>
                           <text x="64" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="end" transform="translate(0,0)" y="161" opacity="1">
                              <tspan>6000</tspan>
                           </text>
                           <text x="64" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="end" transform="translate(0,0)" y="92" opacity="1">
                              <tspan>8000</tspan>
                           </text>
                           <text x="64" style="color:#999;cursor:default;font-size:14px;fill:#999;" text-anchor="end" transform="translate(0,0)" y="24" opacity="1">
                              <tspan>10000</tspan>
                           </text>
                        </g>
                        <g class="highcharts-axis-labels highcharts-yaxis-labels "></g>
                     </svg>
                  </div>
               </div>
               <div class="legend">
                  <span><i class="balls-icon balls-icon__fs"></i> Daily Draw</span>
                  <span><i class="balls-icon balls-icon__free"></i> Free tickets</span>
                  <span><i class="balls-icon balls-icon__rapid"></i> Rapid to the Moon</span>
                  <span><i class="balls-icon balls-icon__success"></i> Token holder's fund, BTC</span>
               </div>
               <p>20/12/2017 - added an additional option to buy tickets with TFL</p>
            </div>
            <script type="text/javascript">
               var TAB_QUARTER_INFO = {
                   categories: ["17/10/2017","18/10/2017","19/10/2017","20/10/2017","21/10/2017","22/10/2017","23/10/2017","24/10/2017","25/10/2017","26/10/2017","27/10/2017","28/10/2017","29/10/2017","30/10/2017","31/10/2017","01/11/2017","02/11/2017","03/11/2017","04/11/2017","05/11/2017","06/11/2017","07/11/2017","08/11/2017","09/11/2017","10/11/2017","11/11/2017","12/11/2017","13/11/2017","14/11/2017","15/11/2017","16/11/2017","17/11/2017","18/11/2017","19/11/2017","20/11/2017","21/11/2017","22/11/2017","23/11/2017","24/11/2017","25/11/2017","26/11/2017","27/11/2017","28/11/2017","29/11/2017","30/11/2017","01/12/2017","02/12/2017","03/12/2017","04/12/2017","05/12/2017","06/12/2017","07/12/2017","08/12/2017","09/12/2017","10/12/2017","11/12/2017","12/12/2017","13/12/2017","14/12/2017","15/12/2017","16/12/2017","17/12/2017","18/12/2017","19/12/2017","20/12/2017","21/12/2017","22/12/2017","23/12/2017","24/12/2017","25/12/2017","26/12/2017","27/12/2017","28/12/2017","29/12/2017","30/12/2017","31/12/2017","01/01/2018","02/01/2018","03/01/2018","04/01/2018","05/01/2018","06/01/2018","07/01/2018","08/01/2018","09/01/2018","10/01/2018","11/01/2018","12/01/2018","13/01/2018","14/01/2018","15/01/2018"],
                   star: [ 517,418,518,636,727,573,573,538,494,493,628,645,611,571,575,535,509,598,460,499,407,364,331,477,397,423,338,375,320,359,312,338,301,389,315,322,329,346,305,127,234,245,276,223,229,187,229,265,228,230,231,326,271,248,210,219,386,389,275,271,309,517,449,425,529,417,501,785,725,568,513,546,499,1093,2471,1542,796,784,1237,1098,1135,686,828,976,948,708,763,814,1044,1012,654],
                   free: [ 52,27,31,29,28,49,43,49,40,22,39,28,28,34,32,37,54,34,20,64,78,57,69,53,58,39,22,17,37,44,35,45,25,29,30,33,20,21,25,4,45,55,32,40,34,24,18,26,85,53,55,60,64,56,43,71,87,96,12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                   fund: [ 0.03,0.06,0.08,0.11,0.15,0.22,0.25,0.29,0.32,0.35,0.39,0.43,0.46,0.5,0.54,0.57,0.61,0.65,0.68,0.71,0.74,0.78,0.8,0.83,0.87,0.89,0.94,0.96,0.98,1,1.03,1.05,1.07,1.09,1.11,1.14,1.16,1.18,1.2,1.21,1.21,1.23,1.25,1.26,1.27,1.28,1.29,1.3,1.32,1.34,1.35,1.36,1.38,1.4,1.41,1.42,1.43,1.44,1.46,1.47,1.48,1.49,1.51,1.52,1.54,1.55,1.56,1.57,1.59,1.6,1.61,1.62,1.63,1.64,1.68,1.71,1.73,1.75,1.77,1.78,1.79,1.81,1.82,1.84,1.85,1.87,1.88,1.89,1.9,1.92,1.93],
                   rapid: [ 0,0,0,2,3,2,38,130,1262,5530,6847,4345,7404,3012,960,524,4278,3181,903,1297,1085,2058,2734,2985,933,933,734,1223,714,584,333,600,606,695,854,982,564,681,449,438,315,293,274,239,295,239,379,300,237,292,188,180,198,264,180,144,251,159,282,206,238,167,201,6680,639,716,771,670,1184,870,311,632,137,1207,952,917,2758,5053,2189,1142,554,475,1854,723,580,982,405,566,680,1015,473]
               };
            </script>
         </div>
      </div>
   </div>
   <div class="socail_media_section">
      <div class="socail_media_section_wrapper row-eq-heigh">
         <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24">
            <div class="soc_wr">
               <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FDaily Draw%2F&amp;tabs=timeline&amp;width=500&amp;height=360&amp;small_header=true&amp;adapt_container_width=true&amp;hide_cover=false&amp;show_facepile=true&amp;appId=2286402351584880" width="100%" height="360" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true"></iframe>
            </div>
         </div>
         <div class="col-lg-12 col-md-12 col-sm-24 col-xs-24">
            <div class="soc_wr js-custom-scroll mCustomScrollbar _mCS_2">
               <div id="mCSB_2" class="mCustomScrollBox mCS-orange mCSB_vertical mCSB_inside" tabindex="0" style="max-height: none;">
                  <div id="mCSB_2_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y" style="position:relative; top:0; left:0;" dir="ltr">
                     <a class="twitter-timeline" data-link-color="#E95F28" href="#" data-twitter-extracted-i1516017645126688766="true">Tweets by Daily DrawLoto</a> <script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                  </div>
                  <div id="mCSB_2_scrollbar_vertical" class="mCSB_scrollTools mCSB_2_scrollbar mCS-orange mCSB_scrollTools_vertical" style="display: none;">
                     <div class="mCSB_draggerContainer">
                        <div id="mCSB_2_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 30px; height: 0px; top: 0px;">
                           <div class="mCSB_dragger_bar" style="line-height: 30px;"></div>
                        </div>
                        <div class="mCSB_draggerRail"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   @include('layouts/footer')
</div>
@endsection
