<div id="setWithdrawalModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="setWithdrawalModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
          <form action="{{ url('admin/setWithdrawalStatus') }}" method="post">
            <div class="modal-header">
                <h4 class="modal-title" id="setWithdrawalModalLabel">Set withdrawal status</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" id="divModalWithdrawalContent">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
            </div>
          </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
