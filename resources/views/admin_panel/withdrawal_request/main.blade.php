@extends('admin_panel/layouts/app')
@section('content')
  <!-- ============================================================== -->
  <!-- Start right Content here -->
  <!-- ============================================================== -->
  <div class="content-page">
      <!-- Start content -->
      <div class="content">
          <div class="container-fluid">

              <div class="row">
                  <div class="col-lg-12">
                      <div class="card-box">
                          <div class="dropdown pull-right">
                              <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
                                  <i class="mdi mdi-dots-vertical"></i>
                              </a>
                              <div class="dropdown-menu dropdown-menu-right">
                                  <!-- item-->
                                  <a href="javascript:void(0);" class="dropdown-item">Action</a>
                                  <!-- item-->
                                  <a href="javascript:void(0);" class="dropdown-item">Another action</a>
                                  <!-- item-->
                                  <a href="javascript:void(0);" class="dropdown-item">Something else</a>
                                  <!-- item-->
                                  <a href="javascript:void(0);" class="dropdown-item">Separated link</a>
                              </div>
                          </div>

                          <h4 class="header-title m-t-0 m-b-30">Withdrawal Requests</h4>

                          <p class="text-muted font-13 m-b-25">
                            After you manually send btc to the user's request adress. You should check current transaction status and save it.
                          </p>

                          <div class="table-responsive">
                              <table class="table m-0">
                                  <thead>
                                  <tr>
                                      <th>#</th>
                                      <th>User Email</th>
                                      <th>Coin</th>
                                      <th>Amount</th>
                                      <th>Address</th>
                                      <th>Transaction Id</th>
                                      <th>Status</th>
                                      <th>#</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                    @foreach ($transactions as $transaction)
                                      <tr>
                                          <th scope="row">{{ $transaction->id }}</th>
                                          <td>{{ $transaction->user->email }}</td>
                                          <td>{{ $transaction->currency }}</td>
                                          <td>{{ $transaction->amount }}</td>
                                          <td>{{ $transaction->address }}</td>
                                          <td>{{ $transaction->tx_id }}</td>
                                          <td>
                                            @if($transaction->status==0)
                                              Waiting
                                            @elseif ($transaction->status==1)
                                              Transfer Confirmed
                                            @elseif ($transaction->status==2)
                                              Approved for Transfer
                                            @endif
                                            </td>
                                          <td>
                                            <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#setWithdrawalModal" onclick="openWithdrawalStatusModal({{ $transaction->id }});">Set Withdrawal Status</button>
                                          </td>
                                      </tr>
                                    @endforeach
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div><!-- end col -->

              </div>
              <!-- end row -->

          </div> <!-- container -->

      </div> <!-- content -->
  </div>
  <script type="text/javascript">
    function openWithdrawalStatusModal(transactionId) {
      $.ajax({
        type:'POST',
        data:{
          transactionId:transactionId
        },
        url:'getWithdrawalStatusModalContent',
        success: function (result) {
          $('#divModalWithdrawalContent').html(result);
        }
      });
    }
  </script>
  @include('admin_panel/withdrawal_request/modal')
@endsection
