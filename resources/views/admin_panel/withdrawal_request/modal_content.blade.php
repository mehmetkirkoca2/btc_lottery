{{ csrf_field() }}
<input type="hidden" name="transactionId" value="{{ $transaction->id }}">
<label for="txId">Tx Id</label>
<input type="text" class="form-control" id="txId" name="txId" value="{{ $transaction->tx_id }}">
<label for="selectTransactionStatus">Tx Status</label>
<select class="form-control" name="selectTransactionStatus">
  <option>Please Select</option>
  @foreach ($transactionStatuses as $key=>$transactionStatus)
    @php
      if($key==$transaction->status){
        $selected='selected';
      }else{
        $selected='';
      }
    @endphp
    <option value="{{ $key }}" {{ $selected }}>{{ $transactionStatus }}</option>
  @endforeach
</select>
