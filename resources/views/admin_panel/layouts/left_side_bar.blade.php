<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
   <div class="sidebar-inner slimscrollleft">
      <!-- User -->
      <div class="user-box">
         <h5><a href="#">Admin</a> </h5>
         <ul class="list-inline">
            <li class="list-inline-item">
               <a href="#" >
               <i class="mdi mdi-settings"></i>
               </a>
            </li>
            <li class="list-inline-item">
               <a href="#" class="text-custom">
               <i class="mdi mdi-power"></i>
               </a>
            </li>
         </ul>
      </div>
      <!-- End User -->
      <!--- Sidemenu -->
      <div id="sidebar-menu">
         <ul>
            <li class="text-muted menu-title">Navigation</li>
            <li>
               <a href="{{ url('admin') }}" class="waves-effect">
                 <i class="mdi mdi-view-dashboard"></i> <span> Dashboard </span>
               </a>
            </li>
            <li>
               <a href="{{ url('admin/withdrawal-requests') }}" class="waves-effect">
                 <i class="fa fa-money"></i> <span> Withdrawal Requests </span>
               </a>
            </li>
         </ul>
         <div class="clearfix"></div>
      </div>
      <!-- Sidebar -->
      <div class="clearfix"></div>
   </div>
</div>
<!-- Left Sidebar End -->
