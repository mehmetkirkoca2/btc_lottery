<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">
    <link rel="shortcut icon" href="{{asset('public/admin_panel/images/favicon.ico')}}">
    <title>Amin Paneli</title>
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="{{asset('public/admin_panel/plugins/morris/morris.css')}}">
    <!-- App css -->
    <link href="{{asset('public/admin_panel/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('public/admin_panel/css/icons.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('public/admin_panel/css/style.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{asset('public/admin_panel/js/modernizr.min.js')}}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
  </head>
    <body class="fixed-left">
      <!-- Begin wrapper -->
      <div id="wrapper">
        @include('admin_panel/layouts/top_bar')
        @include('admin_panel/layouts/left_side_bar')
        @yield('content')
        @include('admin_panel/layouts/right_bar')
        @include('admin_panel/layouts/footer')
        <!-- jQuery  -->
        <script src="{{asset('public/admin_panel/js/jquery.min.js')}}"></script>
        <script src="{{asset('public/admin_panel/js/popper.min.js')}}"></script>
        <script src="{{asset('public/admin_panel/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('public/admin_panel/js/detect.js')}}"></script>
        <script src="{{asset('public/admin_panel/js/fastclick.js')}}"></script>
        <script src="{{asset('public/admin_panel/js/jquery.blockUI.js')}}"></script>
        <script src="{{asset('public/admin_panel/js/waves.js')}}"></script>
        <script src="{{asset('public/admin_panel/js/jquery.nicescroll.js')}}"></script>
        <script src="{{asset('public/admin_panel/js/jquery.slimscroll.js')}}"></script>
        <script src="{{asset('public/admin_panel/js/jquery.scrollTo.min.js')}}"></script>

        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="{{asset('public/admin_panel/plugins/jquery-knob/excanvas.js')}}"></script>
        <![endif]-->
        <script src="{{asset('public/admin_panel/plugins/jquery-knob/jquery.knob.js')}}"></script>

        <!--Morris Chart-->
        <script src="{{asset('public/admin_panel/plugins/morris/morris.min.js')}}"></script>
        <script src="{{asset('public/admin_panel/plugins/raphael/raphael-min.js')}}"></script>

        <!-- Dashboard init -->
        <script src="{{asset('public/admin_panel/pages/jquery.dashboard.js')}}"></script>

        <!-- App js -->
        <script src="{{asset('public/admin_panel/js/jquery.core.js')}}"></script>
        <script src="{{asset('public/admin_panel/js/jquery.app.js')}}"></script>
        <script type="text/javascript">
           $(function () { $.ajaxSetup({headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }});});
        </script>
    </div>
    <!-- END wrapper -->
    </body>
</html>
