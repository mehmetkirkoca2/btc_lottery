<?php
namespace App\Models;

use Zizaco\Entrust\EntrustPermission;
use Spatie\Activitylog\Traits\LogsActivity;

class Permission extends EntrustPermission
{
  use LogsActivity;
  protected static $logAttributes = [
    'id',
    'name',
    'display_name',
    'description',
    'created_at',
    'updated_at'
  ];
  protected $table = 'permissions';
}
