<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class DailyDraw extends Model
{
  use LogsActivity;

  protected $table='daily_draw';
  public $timestamps = false;
  protected static $logAttributes = [
    'block_hash_id',
    'n1',
    'n2',
    'n3',
    'n4',
    'n5',
    'n6',
    'total_won',
    'jackpot_prize'
  ];
  protected $dates = ['created_at'];
  public function hash() {
    return $this->belongsTo('App\Models\BlockHash','block_hash_id');
  }
}
