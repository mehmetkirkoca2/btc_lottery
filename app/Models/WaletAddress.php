<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class WaletAddress extends Model
{
  use LogsActivity;
  protected $table='walet_address';
  protected static $logAttributes = [
    'user_id',
    'currency',
    'address',
    'balance'
  ];
  public $timestamps = false;
}
