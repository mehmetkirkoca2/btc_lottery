<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Transaction extends Model
{
  protected $table='transactions';
  use LogsActivity;
  protected $dates = ['created_at','updated_at'];
  protected static $logAttributes = [
    'user_id',
    'currency',
    'amount',
    'address',
    'tx_id',
    'status',
    'type'
  ];
  public function user() {
    return $this->belongsTo('App\Models\User','user_id');
  }
}
