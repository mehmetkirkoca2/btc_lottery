<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class DailyDrawTicket extends Model
{
  protected $table='daily_draw_ticket';
  use LogsActivity;

  protected static $logAttributes = [
    'user_id',
    'draw_id',
    'n1',
    'n2',
    'n3',
    'n4',
    'n5',
    'n6',
    'created_at'
  ];
  public $timestamps = false;
  protected $dates = ['created_at'];
  public function user() {
    return $this->belongsTo('App\Models\User','user_id');
  }

}
