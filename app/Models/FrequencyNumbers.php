<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FrequencyNumbers extends Model
{
  protected $table='frequency_of_numbers';
  public $timestamps = false;
}
