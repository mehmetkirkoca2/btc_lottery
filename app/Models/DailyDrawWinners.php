<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class DailyDrawWinners extends Model
{
  protected $table='daily_draw_winners';
  use LogsActivity;

  protected static $logAttributes = [
    'user_id',
    'block_hash_id',
    'daily_draw_id',
    'daily_draw_ticket_id',
    'prize_id',
    'matched_numbers',
    'status',
    'created_at'
  ];
  protected $dates = ['created_at'];
  public $timestamps = false;
  public function user() {
    return $this->belongsTo('App\Models\User','user_id');
  }
}
