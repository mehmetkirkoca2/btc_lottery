<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HelpCenterCategory extends Model
{
  protected $table='help_center_category';
  public $timestamps = false;
}
