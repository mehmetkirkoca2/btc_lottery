<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HelpCenterQuestion extends Model
{
  protected $table='help_center_question';
  public $timestamps = false;
}
