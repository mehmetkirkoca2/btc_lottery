<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class DailyDrawWinnerPrize extends Model
{
  protected $table='daily_draw_winner_prizes';
  use LogsActivity;

  protected static $logAttributes = [
    'numbers',
    'plus_one',
    'price_percent',
    'probability'
  ];

  public $timestamps = false;
}
