<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlockHash extends Model
{
  protected $table='block_hash';
  public $timestamps = false;
}
