<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class DailyDrawFeePool extends Model
{
  protected $table='daily_draw_fee_pool';
  use LogsActivity;

  protected static $logAttributes = [
    'daily_draw_id',
    'amount',
    'status'
  ];

  public $timestamps = false;
}
