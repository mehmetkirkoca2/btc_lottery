<?php
namespace App\Models;

use Zizaco\Entrust\EntrustRole;
use Spatie\Activitylog\Traits\LogsActivity;

class Role extends EntrustRole
{
  use LogsActivity;
  protected static $logAttributes = [
    'id',
    'name',
    'display_name',
    'description',
    'created_at',
    'updated_at',
  ];
  protected $table = 'roles';
}
