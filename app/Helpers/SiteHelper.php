<?php
  function isActiveRouteName($routeName,$trueReturn,$falseReturn) {
    $activeRouteName=Illuminate\Support\Facades\Route::currentRouteName();
    return $routeName==$activeRouteName ? $trueReturn:$falseReturn;
  }
  function randomCharecters($len = 5){
    $charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    $base = strlen($charset);
    $result = '';

    $now = explode(' ', microtime())[1];
    while ($now >= $base){
      $i = $now % $base;
      $result = $charset[$i] . $result;
      $now /= $base;
    }
    return substr($result, -5);
  }
  function intervalToDateTime($interval) {
    $date = \Carbon\Carbon::createFromTimestamp($interval);
    return $date;
  }
?>
