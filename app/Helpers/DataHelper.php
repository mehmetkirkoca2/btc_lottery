<?php
  function getUserBalance($currency='BTC') {
    if(\Auth::User()){
      $userId=\Auth::User()->id;
      $wallet=\App\Models\WaletAddress::where('user_id',$userId)->where('currency',$currency)->first();
      if(isset($wallet->id)) {
        return number_format($wallet->balance,8);
      }
    }
  }
  function getUserAddress($currency='BTC') {
    if(\Auth::User()){
      $userId=\Auth::User()->id;
      $wallet=\App\Models\WaletAddress::where('user_id',$userId)->where('currency',$currency)->first();
      if(isset($wallet->id)) {
        return $wallet->address;
      }
    }
  }
  function createWalletAddress() {
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_PORT => "9978",
      CURLOPT_URL => "http://31.186.25.31:9978/create_address",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_HTTPHEADER => array(
        "authorization: 38169b3eb3c4900fa008f7ab4bb1777594193bcf4bca637843abbf3b82b75197",
        "cache-control: no-cache"
      ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
      return $err;
    } else {
      return $response;
    }
  }
  function getBtcToUsd($currency='USD'){
    $prize = \Curl::to('https://api.coindesk.com/v1/bpi/currentprice.json')->get();
    $prize=json_decode($prize);
    if( isset($prize->bpi->{$currency}->rate_float) ){
      return $prize->bpi->{$currency}->rate_float;
    }else{
      return false;
    }
  }
  function getRaffledPrize() {
    $prize= \App\Models\DailyDrawWinners::where('status',1)->sum('prize_amount');
    return substr($prize,0,7);
  }
?>
