<?php
  function getNumberBlocks($hash) {
    for ($i=strlen($hash); $i>0 ; $i-=4) {
      if($i<>strlen($hash)){
        $numberBlocks[]=substr($hash,$i,4);
      }
    }
    return $numberBlocks;
  }

  function convertIntoNumbers($hash) {
    $numberBlocks=getNumberBlocks($hash);
    foreach ($numberBlocks as $block) {
      $firstNumber=hexdec($block);
      $secondNumber=$firstNumber;
      if($firstNumber!=0){
        while ( $firstNumber > 49 ) {
          $firstNumber=$firstNumber % 49;
        }
        $firstNumbers[]=$firstNumber;
      }
      if($secondNumber!=0){
        while ( $secondNumber > 26 ) {
          $secondNumber=$secondNumber % 26;
        }
        $secondNumbers[]=$secondNumber;
      }
      if(isset($firstNumbers)){
        foreach ($firstNumbers as $key=>$firstNumber) {
          if($firstNumber==0){
            unset($firstNumbers[$key]);
          }
        }
      }
      if(isset($secondNumbers)){
        foreach ($secondNumbers as $key=>$secondNumber) {
          if($secondNumber==0){
            unset($secondNumbers[$key]);
          }
        }
      }
    }
    return [
      'firstNumbers'=>$firstNumbers,
      'secondNumbers'=>$secondNumbers
    ];
  }

  function getLuckyNumbers($hash) {
    $numbers=convertIntoNumbers($hash);
    $firstNumbers=array_unique($numbers['firstNumbers']);
    $secondNumbers=array_unique($numbers['secondNumbers']);
    $firstNumbers = array_values($firstNumbers);
    $secondNumbers = array_values($secondNumbers);
    if(count($firstNumbers)>=5 and count($secondNumbers)>=1){
      return [
        'firstNumbers'=>$firstNumbers,
        'secondNumber'=>$secondNumbers[0]
      ];
    }else{
      return false;
    }
  }

  function getDrawNumberDate($format=''){
    $today=\Carbon\Carbon::now()->today();
    $drawTimeAndDate=$today->addHours(18)->format('m/d/Y H:i:s');
    return $drawTimeAndDate;
  }

  function getNextDrawNumber() {
    $drawId=\App\Models\DailyDraw::max('id');
    $drawId++;
    return $drawId;
  }

  function getPrizes($drawId=0) {
    if($drawId==0){
      $drawId=getNextDrawNumber();
    }
    $prizes=\App\Models\DailyDrawWinnerPrize::orderBy('id','desc')->get();
    $ticketCount=\App\Models\DailyDrawTicket::where('draw_id',$drawId)->count();
    $totalAmountOfBtc=bcmul($ticketCount,0.0002,8);
    $totalPoolAmount=\App\Models\DailyDrawPool::where('status',1)->sum('amount');

    foreach ($prizes as $prize) {
      $range=bcdiv($totalAmountOfBtc,100,8);
      $calculatedPrize=bcmul($prize->price_percent,$range,8);
      $calculatedPrizes[]=[
        'numbers'=>$prize->numbers,
        'plus_one'=>$prize->plus_one,
        'prize'=>$calculatedPrize,
        'probability'=>$prize->probability
      ];
    }
    $datas=[
      'totalPoolAmount'=>$totalPoolAmount,
      'prizes'=>$calculatedPrizes
    ];
    return $datas;
  }
  function getJackPotPrize($drawId=0) {
    if($drawId==0){
      $drawId=getNextDrawNumber();
    }
    $ticketCount=\App\Models\DailyDrawTicket::where('draw_id',$drawId)->count();
    $totalAmountOfBtc=bcmul($ticketCount,0.0002,8);
    $totalPoolAmount=\App\Models\DailyDrawPool::where('status',1)->sum('amount');

    $prize=\App\Models\DailyDrawWinnerPrize::where('id',10)->first();
    $range=bcdiv($totalAmountOfBtc,100,8);
    $calculatedPrize=bcmul($prize->price_percent,$range,8);
    $jackpotPrize=bcadd($totalPoolAmount,$calculatedPrize,8);
    return $jackpotPrize;
  }

  function getDrawNumbers($drawId) {
    $draw=\App\Models\DailyDraw::where('id',$drawId)->first();
    if(! is_null($draw)){
      $drawNumbers[]=$draw->n1;
      $drawNumbers[]=$draw->n2;
      $drawNumbers[]=$draw->n3;
      $drawNumbers[]=$draw->n4;
      $drawNumbers[]=$draw->n5;
      $drawNumberPlusOne=$draw->n6;
      return [
        'numbers'=>$drawNumbers,
        'plusOne'=>$drawNumberPlusOne
      ];
    }
    return false;
  }
  function getLastDrawNumbers() {
    $drawId=\App\Models\DailyDraw::max('id');
    $numbers=getDrawNumbers($drawId);
    if($numbers===false){
      return false;
    }
    $numbers['drawId']=$drawId;
    return $numbers;
  }
?>
