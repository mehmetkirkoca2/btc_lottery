<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
      Commands\DrawLottery::class,
      Commands\GetBlockHash::class,
      Commands\CheckWinners::class,
      Commands\GivePrize::class,
      Commands\PlayDailyDraw::class,
      Commands\CalculateFrequencyOfNumbers::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
      $schedule->command('GetBlockHash')->everyMinute();
      $schedule->command('DrawLottery')->everyMinute();
      $schedule->command('CheckWinners')->everyMinute();
      $schedule->command('GivePrize')->hourly();

      $schedule->command('PlayDailyDraw')->dailyAt('21:00');
      $schedule->command('CalculateFrequencyOfNumbers')->dailyAt('23:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
