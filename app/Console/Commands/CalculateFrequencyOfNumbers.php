<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Artisan;
use App\Models\DailyDraw;
use App\Models\FrequencyNumbers;
class CalculateFrequencyOfNumbers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CalculateFrequencyOfNumbers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate Frequency Of Numbers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle() {
      $dailyDraws=DailyDraw::all();
      foreach ($dailyDraws as $dailyDraw) {
        $drawNumbers[]=$dailyDraw->n1;
        $drawNumbers[]=$dailyDraw->n2;
        $drawNumbers[]=$dailyDraw->n3;
        $drawNumbers[]=$dailyDraw->n4;
        $drawNumbers[]=$dailyDraw->n5;
        $drawNumbers[]=$dailyDraw->n6;
      }
      $drawNumbers=array_count_values($drawNumbers);
      $playedNumberCount=count($dailyDraws)*6;
      $percentRange=100/$playedNumberCount;
      $numbers=FrequencyNumbers::all();
      foreach ($numbers as $number) {
        if(isset($drawNumbers[$number->number])){
          $number->frequency=$percentRange*$drawNumbers[$number->number];
          $number->save();
        }
      }
    }
}
