<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Curl;
use App\Models\DailyDrawTicket;
use App\Models\BlockHash;
use App\Models\DailyDraw;
use App\Models\DailyDrawWinners;
use App\Models\DailyDrawWinnerPrize;
use App\Models\DailyDrawPool;

use DB;
use Carbon\Carbon;
use Hash;
class PlayDailyDraw extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'PlayDailyDraw {userId?} {drawId?} {ticketCount?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command For test purpose it generate random ticket for specific draw id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle() {
      $userId=$this->argument('userId');
      $drawId=$this->argument('drawId');
      $ticketCount=$this->argument('ticketCount');
      if(is_null($userId)){
        $userId=1;
      }
      if(is_null($drawId)){
        $drawId=getNextDrawNumber();
      }
      if(is_null($ticketCount)){
        $ticketCount=50000;
      }
      $systemTickets=0;
      while ($ticketCount>=$systemTickets) {
        $systemTickets=DailyDrawTicket::where('draw_id',$drawId)->where('user_id',$userId)->count('id');
        $rndNumber=rand(1,100000000000);
        $hash=Hash::make(date('H:i:s').$rndNumber.date('H:i:s'));
        $numbers=getLuckyNumbers($hash);
        if($numbers!==false){
          $firstNumbers=$numbers["firstNumbers"];
          $secondNumber=$numbers["secondNumber"];

          $dailyDrawTicket=new DailyDrawTicket;
          $dailyDrawTicket->user_id=$userId;
          $dailyDrawTicket->draw_id=$drawId;
          $dailyDrawTicket->n1=$firstNumbers[0];
          $dailyDrawTicket->n2=$firstNumbers[1];
          $dailyDrawTicket->n3=$firstNumbers[2];
          $dailyDrawTicket->n4=$firstNumbers[3];
          $dailyDrawTicket->n5=$firstNumbers[4];
          $dailyDrawTicket->n6=$secondNumber;
          $dailyDrawTicket->save();
        }
        echo $systemTickets."\n";
      }
    }
}
