<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\DailyDraw;
use App\Models\DailyDrawTicket;
use App\Models\DailyDrawWinners;
use App\Models\DailyDrawWinnerPrize;
use Carbon\Carbon;

class CheckWinners extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkWinners {drawId?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Give Prize';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    private function getPrizeId($matchedNumbers,$plusOne) {
      $numbers=count($matchedNumbers);
      $prize=DailyDrawWinnerPrize::where('numbers',$numbers);
      $prize=$prize->where('plus_one',$plusOne)->first();
      if(isset($prize->id)){
        return $prize->id;
      }
      return 0;
    }
    public function handle() {

      $today=Carbon::now()->today();
      $lotteryDrawTime=$today->addHours(18);
      $utcNow=Carbon::now();
      $nowInterval=strtotime($utcNow);
      $lotteryDrawTimeInterval=strtotime($lotteryDrawTime);

      if($nowInterval>=$lotteryDrawTimeInterval) {
        $drawId=$this->argument('drawId');

        $winners=DailyDrawWinners::where('status',0);
        if(is_null($drawId)){
          $drawId=DailyDraw::whereNull('total_won')->first();
          if(isset($drawId->id)){
            $drawId=$drawId->id;
          }else{
            echo "\n givable prize not found\n";
            return false;
          }
        }

        $winners=$winners->where('daily_draw_id',$drawId);
        $winners=$winners->get();

        // check all ticket if they are all checked
        $ticket=DailyDrawTicket::where('draw_id',$drawId)->where('prize_id',-1)->first();
        if(! is_null($ticket)) {
          $draw=DailyDraw::find($drawId);
          $tickets=DailyDrawTicket::where('draw_id',$draw->id)->where('prize_id',-1)->get();

          $drawNumbers[]=$draw->n1;
          $drawNumbers[]=$draw->n2;
          $drawNumbers[]=$draw->n3;
          $drawNumbers[]=$draw->n4;
          $drawNumbers[]=$draw->n5;
          $drawNumberPlusOne=$draw->n6;

          foreach ($tickets as $ticket) {
            $ticketNumbers[]=$ticket->n1;
            $ticketNumbers[]=$ticket->n2;
            $ticketNumbers[]=$ticket->n3;
            $ticketNumbers[]=$ticket->n4;
            $ticketNumbers[]=$ticket->n5;
            $ticketNumberPlusOne=$ticket->n6;
            $matchedNumbers=array_intersect($ticketNumbers,$drawNumbers);
            unset($ticketNumbers);
            $plusOne=0;
            if($drawNumberPlusOne==$ticketNumberPlusOne){
              $plusOne=$drawNumberPlusOne;
            }
            $prizeId=$this->getPrizeId($matchedNumbers,$plusOne);
            if( $prizeId!=0 ) { //user won some of the prize
              $dailyDrawWinner=DailyDrawWinners::where('daily_draw_ticket_id',$ticket->id)->first();
              if(is_null($dailyDrawWinner)){
                $dailyDrawWinner=new DailyDrawWinners;
                $dailyDrawWinner->user_id=$ticket->user_id;
                $dailyDrawWinner->block_hash_id=$draw->block_hash_id;
                $dailyDrawWinner->daily_draw_id=$draw->id;
                $dailyDrawWinner->prize_id=$prizeId;
                $dailyDrawWinner->daily_draw_ticket_id=$ticket->id;
                $dailyDrawWinner->matched_numbers=json_encode([
                    'numbers'=>array_values($matchedNumbers),
                    'plusOne'=>$plusOne
                  ]
                );
                $ticket->prize_id=$prizeId;
                $dailyDrawWinner->save();
              }
            }else{
              $ticket->prize_id=0;
            }
            $ticket->save();
          }
        }
      }
    }
}
