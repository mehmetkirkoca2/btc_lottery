<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Curl;
use App\Models\BlockHash;
use App\Models\DailyDraw;
use App\Models\DailyDrawTicket;
use App\Models\DailyDrawWinners;
use App\Models\DailyDrawWinnerPrize;
use App\Models\DailyDrawPool;
use App\Models\DailyDrawFeePool;

use DB;
use Carbon\Carbon;
use Symfony\Component\Console\Helper\ProgressBar;
class GivePrize extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'giveprize {drawId?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Give Prize';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    private function calculatePrize($prizeId,$drawId) {

      //how many user won the prize
      $prizeWinner = DB::table('daily_draw_winners')
                       ->select(DB::raw('count(*) as count'))
                       ->where('prize_id',$prizeId)
                       ->where('daily_draw_id',$drawId)
                       ->groupBy('prize_id')
                       ->groupBy('daily_draw_id')
                       ->first();

      $totalTicketCount=\App\Models\DailyDrawTicket::where('draw_id',$drawId)->count();
      $totalAmountOfBtc=bcmul($totalTicketCount,0.0002,8);
      $range=bcdiv($totalAmountOfBtc,100,8);

      $prize=\App\Models\DailyDrawWinnerPrize::where('id',$prizeId)->first();
      $totalCalculatedPrize=bcmul($prize->price_percent,$range,8);
      $calculatedPrize=bcdiv($totalCalculatedPrize,$prizeWinner->count,8);

      if($prizeId==10){ //jackpot
        $totalPoolAmount=\App\Models\DailyDrawPool::where('status',1)->sum('amount');
        $calculatedPrize=bcadd($calculatedPrize,$totalPoolAmount,8);
      }
      return $calculatedPrize;
    }
    private function getTotalBtcAmoud($drawId) {
      $totalTicketCount=\App\Models\DailyDrawTicket::where('draw_id',$drawId)->count();
      $totalAmountOfBtc=bcmul($totalTicketCount,0.0002,8);
      return $totalAmountOfBtc;
    }
    private function getGivedPrizeAmount($drawId) {
      $givedPrizeAmount=\App\Models\DailyDrawWinners::where('daily_draw_id',$drawId)->sum('prize_amount');
      return $givedPrizeAmount;
    }
    private function getTotalGivingAmount($totalBtcAmountRange) {
      $totalPrizePercent=\App\Models\DailyDrawWinnerPrize::sum('price_percent');
      $totalGivingAmount=bcmul($totalPrizePercent,$totalBtcAmountRange,8);
      return $totalGivingAmount;
    }
    private function calculateLeftedJackPotPrize($drawId) {
      $totalAmountOfBtc=$this->getTotalBtcAmoud($drawId);
      $totalBtcAmountRange=bcdiv($totalAmountOfBtc,100,8);

      $givedPrizeAmount=$this->getGivedPrizeAmount($drawId);

      $totalGivingAmount=$this->getTotalGivingAmount($totalBtcAmountRange);

      $leftedAmound=bcsub($totalGivingAmount,$givedPrizeAmount,8);
      return $leftedAmound;

      // dd('Total BTC:'.$totalAmountOfBtc
      //   ,' Total Giving Amount: '.$totalGivingAmount
      //   ,' already gived: :'.$givedPrizeAmount
      //   ,' lefted Amound: '.$leftedAmound);
    }
    private function calculateJackPotPrize($drawId) {
      $totalAmountOfBtc=$this->getTotalBtcAmoud($drawId);
      $totalBtcAmountRange=bcdiv($totalAmountOfBtc,100,8);
      $prizePercent=\App\Models\DailyDrawWinnerPrize::where('id',10)->first()->price_percent;
      $jackpotPrize=bcmul($totalBtcAmountRange,$prizePercent,8);
      $poolPrize=\App\Models\DailyDrawPool::where('daily_draw_id',$drawId)->first();
      if(isset($poolPrize->amount)){
        $poolPrize=$poolPrize->amount;
      }else{
        $poolPrize=0;
      }
      $jackpotPrize=bcadd($jackpotPrize,$poolPrize,8);
      return $jackpotPrize;
    }
    private function calculateTotalFee($drawId) {
      $totalAmountOfBtc=$this->getTotalBtcAmoud($drawId);
      $totalBtcAmountRange=bcdiv($totalAmountOfBtc,100,8);

      $totalGivingAmount=$this->getTotalGivingAmount($totalBtcAmountRange);
      $totalFeeAmound=bcsub($totalAmountOfBtc,$totalGivingAmount,8);
      return $totalFeeAmound;

      // dd('Total BTC:'.$totalAmountOfBtc
      //   ,' Total Fee Amound: '.$totalFeeAmound);
    }
    private function setDrawPrizes($drawId) {
      $dailyDraw=DailyDraw::where('id',$drawId)->first();
      $dailyDraw->total_won=$this->getGivedPrizeAmount($drawId);
      $dailyDraw->jackpot_prize=$this->calculateJackPotPrize($drawId);
      $dailyDraw->save();
    }
    private function getPrizeId($matchedNumbers,$plusOne) {
      $numbers=count($matchedNumbers);
      $prize=DailyDrawWinnerPrize::where('numbers',$numbers);
      $prize=$prize->where('plus_one',$plusOne)->first();
      if(isset($prize->id)){
        return $prize->id;
      }
      return 0;
    }
    public function handle() {

      $today=Carbon::now()->today();
      $lotteryDrawTime=1;
      // $today->addHours(20);
      $utcNow=Carbon::now();
      $nowInterval=strtotime($utcNow);
      $lotteryDrawTimeInterval=strtotime($lotteryDrawTime);

      if($nowInterval>=$lotteryDrawTimeInterval) {
        $drawId=$this->argument('drawId');
        $winners=DailyDrawWinners::where('status',0);
        if(is_null($drawId)){
          $drawId=DailyDraw::whereNull('total_won')->first();
          if(isset($drawId->id)){
            $drawId=$drawId->id;
          }else{
            echo "\n givable prize not found\n";
            return false;
          }
        }

        $winners=$winners->where('daily_draw_id',$drawId);
        $winners=$winners->get();

        // if(count($winners)==0){
        //   return false;
        // }

        // check all ticket if they are all checked
        $ticket=DailyDrawTicket::where('draw_id',$drawId)->where('prize_id',-1)->first();
        if(! is_null($ticket)) {
          return false;
        }
        $jackpotPrize=false;
        $this->info('Winners Calculating');
        $this->output->progressStart(count($winners));
        foreach ($winners as $winner) {
          $prizeAmount=$this->calculatePrize($winner->prize_id,$drawId);
          $winner->prize_amount=$prizeAmount;
          $winner->status=1;
          $winner->save();
          if($winner->prize_id==10){ //jackpot prize
            $jackpotPrize=true;
          }
          $this->output->progressAdvance();
        }
        $this->output->progressFinish();

        $this->info('Calculating lefted jackpot prize');
        if($jackpotPrize===false){ //jackpot prize will add to the pool
          $prizeAmount=$this->calculateLeftedJackPotPrize($drawId);
          $pool=DailyDrawPool::where('daily_draw_id',$drawId)->first();
          if(is_null($pool)){
            $pool= new DailyDrawPool;
            $pool->daily_draw_id=$drawId;
          }
          $pool->amount=$prizeAmount;
          $pool->save();
        }
        $this->info('lefted jackpot prize calculated');

        $this->info('Calculating total fee');
        $totalFee=$this->calculateTotalFee($drawId);
        $feePool=DailyDrawFeePool::where('daily_draw_id',$drawId)->first();
        if(is_null($feePool)){
          $feePool= new DailyDrawFeePool;
          $feePool->daily_draw_id=$drawId;
        }
        $feePool->amount=$totalFee;
        $feePool->save();
        $this->info('Total fee calculated');
        $this->setDrawPrizes($drawId);
      }
    }
}
