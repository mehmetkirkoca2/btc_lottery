<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Curl;
use App\Models\BlockHash;
use Carbon\Carbon;

class GetBlockHash extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GetBlockHash';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get the lastest block hash and block number';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
      $today=Carbon::now()->today();
      $lotteryDrawTime=$today->addHours(18);
      $utcNow=Carbon::now();
      $nowInterval=strtotime($utcNow);
      $lotteryDrawTimeInterval=strtotime($lotteryDrawTime);

      if($nowInterval>=$lotteryDrawTimeInterval) {
        //insert block untill successfull
        $blockHashExist=BlockHash::where('created_at','>',$today)->where('draw_type',2)->first();
        if(is_null($blockHashExist)){
          $currentBlockCount = Curl::to('https://blockchain.info/latestblock')->get();
          $blockData=json_decode($currentBlockCount);
          $height=BlockHash::max('height');
          if($blockData->height>$height and $blockData->time>=$lotteryDrawTimeInterval){
            $blockHash= new BlockHash;
            $blockHash->hash=$blockData->hash;
            $blockHash->time=$blockData->time;
            $blockHash->block_index=$blockData->block_index;
            $blockHash->height=$blockData->height;
            $blockHash->save();
          }
        }

      }
    }
}
