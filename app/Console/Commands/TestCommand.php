<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Helper\ProgressBar;

use Artisan;
class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'TestCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test Command for new features';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle() {
      $this->output->progressStart(10);

          for ($i = 0; $i < 10; $i++) {
              sleep(1);

              $this->output->progressAdvance();
          }

          $this->output->progressFinish();

    }
}
