<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Curl;
use App\Models\BlockHash;
use App\Models\DailyDraw;
use App\Models\DailyDrawTicket;
use App\Models\DailyDrawWinners;
use App\Models\DailyDrawWinnerPrize;
use Artisan;
use Carbon\Carbon;
class DrawLottery extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DrawLottery';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Draw Lottery';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

      $today=Carbon::now()->today();
      $lotteryDrawTime=$today->addHours(18);

      $utcNow=Carbon::now();
      $nowInterval=strtotime($utcNow);
      $lotteryDrawTimeInterval=strtotime($lotteryDrawTime);

      if($nowInterval>=$lotteryDrawTimeInterval){
        //if block hash not drawed
        $blockHash=BlockHash::where('time','>=',$lotteryDrawTimeInterval)->whereIn('draw_type',[0,2])->orderBy('id','asc')->first();
        if( ! is_null($blockHash) and $blockHash->draw_type!=2 ) {
          $luckyNumbers=getLuckyNumbers($blockHash->hash);
          if($luckyNumbers!==false){
            $firstNumbers=$luckyNumbers['firstNumbers'];
            $seconNumber=$luckyNumbers['secondNumber'];
            $dailyDraw=DailyDraw::where('block_hash_id',$blockHash->id)->first();
            if(is_null($dailyDraw)){
              $DailyDraw=new DailyDraw;
              $DailyDraw->block_hash_id=$blockHash->id;
              $DailyDraw->n1=$firstNumbers[0];
              $DailyDraw->n2=$firstNumbers[1];
              $DailyDraw->n3=$firstNumbers[2];
              $DailyDraw->n4=$firstNumbers[3];
              $DailyDraw->n5=$firstNumbers[4];
              $DailyDraw->n6=$seconNumber;
              $DailyDraw->save();
              $blockHash->draw_type=2; //it is a successfull draw
            }
          }else{
            $blockHash->draw_type=1; //is not correct number formats
          }
          $blockHash->save();
        }
      }

    }
}
