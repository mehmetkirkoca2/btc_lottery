<?php

namespace App\Http\Controllers;

use Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ValidateSecretRequest;
use PragmaRX\Google2FA\Google2FA;

class TwoFAController extends Controller
{
   public function authenticate2fa(Request $request)
   {
      $user=Auth::User();
      if($user->google2fa_secret){
        $secret = $request->input('secret');
        $google2fa= new Google2FA();
        $valid = $google2fa->verifyKey($user->google2fa_secret, $secret);
        if ($valid === false) {
          $request->session()->put('error',"Wrong 2FA code");
        }else{
          $request->session()->put('2fa:user:id', $user->id);
        }
      }
      return redirect(session('lastPage'));
   }
   /**
   *
   * @return \Illuminate\Http\Response
   */
  public function getValidateToken()
  {
    if(! Auth::User()){
      return redirect('login');
    }
    if (! session('2fa:user:id')) {
      return redirect('2fa-validation');
    }
  }
}
