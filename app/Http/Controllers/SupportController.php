<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Models\HelpCenterSupportMessage;

class SupportController extends Controller
{
  public function sendSupportMessage(Request $request){
    $nameSurname  = $request->input('nameSurname');
    $email        = $request->input('email');
    $subject      = $request->input('subject');
    $message      = $request->input('message');
    $supportMessage=new HelpCenterSupportMessage;
    if(isset(Auth::User()->id)){
      $supportMessage->user_id=Auth::User()->id;
    }
    $supportMessage->name_surname=$nameSurname;
    $supportMessage->email=$email;
    $supportMessage->subject=$subject;
    $supportMessage->message=$message;
    $supportMessage->save();
    return back();
  }
}
