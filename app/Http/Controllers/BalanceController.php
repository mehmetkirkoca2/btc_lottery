<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;
use Auth;
use Hash;
use DB;
use Carbon\Carbon;

use App\Models\WaletAddress;
use App\Models\Transaction;

class BalanceController extends Controller
{
  public function refillBalance() {
    return view('refill_balance/main');
  }
  public function withdrawal(){
    return view('fund_withdrawal/main');
  }
  private function returnMessage($status,$title,$message) {
    $datas=[
      'status'=>$status,
      'title'=>$title,
      'message'=>$message,
    ];
    return json_encode($datas);
  }
  public function saveWithdrawalRequest(Request $request) {
    $userBalance=getUserBalance('BTC');
    $widthdrawalWalletAddress=$request->input('widthdrawalWallet');
    $widthdrawalAmount=$request->input('widthdrawalAmount');
    $widthdrawalPass=$request->input('widthdrawalPass');

    $widthdrawalAmountWithFee=bcadd($widthdrawalAmount,0.001,8); //added fee
    $compareResult=bccomp($userBalance,$widthdrawalAmountWithFee,8);
    if($compareResult==-1) { //insuffucent blance
      return $this->returnMessage('error','insuffucent balance','Please check your blance');
    }
    $user = Auth::User();
    if (! Hash::check($widthdrawalPass, $user->password)) {
      return $this->returnMessage('error','Wrong Password','Please check your password again');
    }
    $wallet=WaletAddress::where('user_id',$user->id)->where('currency','BTC')->first();
    $leftedAmound=bcsub($wallet->balance,$widthdrawalAmountWithFee);
    $wallet->balance=$leftedAmound;
    $wallet->save();
    $transaction=new Transaction;

    $transaction->user_id=$user->id;
    $transaction->currency='BTC';
    $transaction->amount=$widthdrawalAmount;
    $transaction->address=$widthdrawalWalletAddress;
    $transaction->tx_id='';
    $transaction->status=0;
    $transaction->type=2;
    $transaction->save();

    return $this->returnMessage('success','Your withdrawal request has been send','You will redirect to see your transaction history and withdrawal status.');
  }
}
