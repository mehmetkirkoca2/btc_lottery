<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Models\DailyDraw;
use App\Models\DailyDrawTicket;
use App\Models\DailyDrawWinners;
use App\Models\Transaction;
use App\Models\FrequencyNumbers;
use App\Models\HelpCenterCategory;
use App\Models\HelpCenterQuestion;

use Carbon\Carbon;
use Mail;
class PageController extends Controller
{
  public function mainPage($openModal=0) {
    $currentDate=Carbon::now();
    $lastMounthDate=Carbon::now()->subMonth(1);

    $recentBets=DailyDrawTicket::orderBy('draw_id','desc')->orderBy('id','desc')->limit(10)->get();
    $recentWins=DailyDrawWinners::orderBy('prize_amount','desc')->limit(10)->get();
    $recentTransactions=Transaction::orderBy('id','desc')->where('status',1)->where('type',1)->limit(10)->get();
    $recentWithdrawals=Transaction::orderBy('id','desc')->where('status',1)->where('type',2)->limit(10)->get();
    $ticketSoldLastMounth=DailyDrawTicket::whereBetween('created_at',[$lastMounthDate,$currentDate])->count();
    $winnersLastMounth=DailyDrawWinners::whereBetween('created_at',[$lastMounthDate,$currentDate])->count();
    $paidToUserLastMounth=DailyDrawWinners::whereBetween('created_at',[$lastMounthDate,$currentDate])->sum('prize_amount');
    $frequencyOfNumbers=FrequencyNumbers::all();

    $datas=[
      'lastDrawNumbers'=>getLastDrawNumbers(),
      'recentBets'=>$recentBets,
      'recentWins'=>$recentWins,
      'recentTransactions'=>$recentTransactions,
      'recentWithdrawals'=>$recentWithdrawals,
      'ticketSoldLastMounth'=>$ticketSoldLastMounth,
      'winnersLastMounth'=>$winnersLastMounth,
      'paidToUserLastMounth'=>$paidToUserLastMounth,
      'frequencyOfNumbers'=>$frequencyOfNumbers,
      'openModal'=>$openModal
    ];
    return view('index/main',$datas);
  }
  public function dailyDraw(){
    $luciestWinners=DailyDrawWinners::orderBy('prize_amount','desc')->limit(4)->get();
    $datas=[
      'luciestWinners'=>$luciestWinners
    ];
    return view('daily_draw/main',$datas);
  }
  public function drawArchive(Request $request) {
    $dailyDraws=DailyDraw::orderBy('id','desc')->limit(10)->get();
    if(isset($dailyDraws[count($dailyDraws)-1]->id)){
      $minDrawId=$dailyDraws[count($dailyDraws)-1]->id;
    }
    $request->session()->put('minDrawId', $minDrawId);

    $datas=[
      'dailyDraws'=>$dailyDraws
    ];
    return view('draw_archive/main',$datas);
  }
  public function showMoreDraws(Request $request) {
    $minDrawId=$request->session()->get('minDrawId');
    $request->session()->put('minDrawId', $minDrawId-10);
    $dailyDraws=DailyDraw::where('id','<',$minDrawId)->orderBy('id','desc')->limit(10)->get();
    $datas=[
      'dailyDraws'=>$dailyDraws
    ];
    return view('draw_archive/next_draws',$datas);
  }
  public function myAccount(Request $request) {
    return view('my_account/main');
  }
  public function myTransactions(Request $request) {
    $userId=Auth::User()->id;
    $myTransactions=Transaction::where('user_id',$userId)->orderBy('id','desc')->limit(10)->get();
    $datas=[
      'myTransactions'=>$myTransactions
    ];
    return view('my_transactions/main',$datas);
  }
  public function resentConfirmationMail(Request $request) {
    $user=Auth::User();
    $confirmationCode=$user->confirmation_code;
    $email=$user->email;

    Mail::send('email/verify',[ 'confirmationCode'=>$confirmationCode], function($message) use($email) {
      $message->to($email, $email)->subject('Verify your email address');
    });
    $request->session()->flash('message','Confirmation link has been send to your mail address. You can click and confirm your account.');
    return back();
  }
  public function loginModal() {
    return $this->mainPage(1);
  }
  public function twoFaValidationModal() {
    return $this->mainPage(2);
  }
  public function helpCenter(Request $request,$categoryId=0) {
    $categories=HelpCenterCategory::all();
    $categoryName='The most common questions are';
    $questions=HelpCenterQuestion::where('order',1)->get();

    if( $categoryId != 0 ){
      $category=HelpCenterCategory::where('id',$categoryId)->first();
      if(isset($category->name)){
        $categoryName=$category->name;
        $questions=HelpCenterQuestion::where('category_id',$categoryId)->get();
      }else{
        $questions=null;
      }
    }
    $datas=[
      'categories'=>$categories,
      'categoryName'=>$categoryName,
      'questions'=>$questions
    ];
    return view('help_center/main',$datas);
  }
}
