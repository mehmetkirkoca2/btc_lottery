<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Models\Transaction;
use Carbon\Carbon;
class AdminController extends Controller
{
  public function mainPage() {
    $datas=[

    ];
    return view('admin_panel/main_page/main',$datas);
  }
  public function withdrawalRequests(Request $request) {
    $transactions=Transaction::where('type',2)->paginate(50);
    $datas=[
      'transactions'=>$transactions
    ];
    return view('admin_panel/withdrawal_request/main',$datas);
  }
  public function getWithdrawalStatusModalContent(Request $request) {
    $transactionId=$request->input('transactionId');
    $transaction=Transaction::find($transactionId);
    $transactionStatuses=[
      2=>'Approved for transfer',
      0=>'Waiting',
      1=>'Transfer Confirmed',
    ];
    $datas=[
      'transaction'=>$transaction,
      'transactionStatuses'=>$transactionStatuses
    ];
    return view('admin_panel/withdrawal_request/modal_content',$datas);
  }
  public function setWithdrawalStatus(Request $request) {
    $transactionId      =  $request->input('transactionId');
    $txId               =  $request->input('txId');
    $transactionStatus  =  $request->input('selectTransactionStatus');
    $transaction=Transaction::find($transactionId);
    $transaction->tx_id=$txId;
    $transaction->status=$transactionStatus;
    $transaction->save();
    return back();
  }
}
