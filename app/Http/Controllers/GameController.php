<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;
use DB;
use Session;
use Curl;
use Carbon\Carbon;
use App\Models\User;
use App\Models\DailyDraw;
use App\Models\DailyDrawTicket;
use App\Models\WaletAddress;
use App\Models\FrequencyNumbers;

class GameController extends Controller
{
  public function dailyDraw() {
    $mostCommonNumbers=FrequencyNumbers::orderBy('frequency','desc')->where('frequency','>',1)->get();
    $combos=null;
    $i=0;
    foreach ($mostCommonNumbers as $mostCommonNumber) {
      $i++;
      $numbers[]=$mostCommonNumber->number;
      if($i==6){
        $i=0;
        $combos[]=$numbers;
        unset($numbers);
      }
    }
    $datas=[
      'drawNo'=>getNextDrawNumber(),
      'combos'=>$combos
    ];
    return view('play_daily_draw/main',$datas);
  }

  public function buyTicket(Request $request) {
    $userId=Auth::User()->id;

    $tickets=$request->input('tickets');
    $currency=$request->input('currency');
    $userBalance=getUserBalance($currency);
    $ticketAmount=count($tickets);
    $totalPrice=bcmul($ticketAmount,0.00020,8);

    //  0  -> Equal
    //  1  -> left greater than right
    // -1  -> left lower than right
    $compareResult=bccomp($userBalance,$totalPrice,8);
    if($compareResult==1 or $compareResult==0) { //user blance low or equal
      $userWallet=WaletAddress::where('currency',$currency)->where('user_id',$userId)->first();
      $leftedBalance=bcsub($userWallet->balance,$totalPrice,8);
      $userWallet->balance=$leftedBalance;
      $userWallet->save();
      foreach ($tickets as $ticket) {
        $ticketNumbers=$ticket['combo'];
        $dailyDrawTicket=new DailyDrawTicket;
        $dailyDrawTicket->user_id=$userId;
        $dailyDrawTicket->draw_id=getNextDrawNumber();
        $dailyDrawTicket->n1=$ticketNumbers[0];
        $dailyDrawTicket->n2=$ticketNumbers[1];
        $dailyDrawTicket->n3=$ticketNumbers[2];
        $dailyDrawTicket->n4=$ticketNumbers[3];
        $dailyDrawTicket->n5=$ticketNumbers[4];
        $dailyDrawTicket->n6=$ticketNumbers[5];
        $dailyDrawTicket->save();
      }
      $datas=[
              'type'=>'success',
              'message'=>'Your ticket purchased. Good luck'
      ];
    }else{
      $datas=[
              'type'=>'error',
              'message'=>'Insufficient funds Please, refill your balance'
      ];
    }
    return  json_encode($datas);
  }

  public function getLuckyNumbers() {
    $result=getPrizes();
    dd($result);

    // $numbers=convertIntoNumbers('000000000000000000125e9285bce772c300d0ed9de33a43cb83b39f8098e953');
    // $luckyNumbers=getLuckyNumbers('000000000000000000125e9285bce772c300d0ed9de33a43cb83b39f8098e953');
    // dd($numbers,$luckyNumbers);
  }
}
