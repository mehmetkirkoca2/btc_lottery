<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Models\User as User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
  use AuthenticatesUsers;
  public function __construct() {
    $this->middleware('guest')->except('logout');
  }
  public function login(Request $request) {
    $loginAtemptCount = session('loginAtemptCount');
    if(is_null($loginAtemptCount)){
      session(['loginAtemptCount' =>1]);
    }else{
      $loginAtemptCount++;
      session(['loginAtemptCount' =>$loginAtemptCount]);
    }
    if($loginAtemptCount<=10) {
      $validator = \Validator::make($request->all(), [
        'g-recaptcha-response' => 'required|recaptcha',
      ]);

      if ($validator->fails()) {
        $request->session()->flash('errorLogin','Recaptcha is failed.');
        return back();
      }

      $email=$request->input('email');
      $password=$request->input('password');
      if (Auth::attempt(['email' => $email, 'password' => $password])) {
        $lastPage='/';
        if( Auth::user()->hasRole('admin') ){
          $lastPage='admin';
        }
        $request->session()->flash('successLogin','You logged in.');
        if(Auth::user()->google2fa_secret and is_null(session('2fa:user:id'))){
          $request->session()->put('lastPage',$lastPage);
          return redirect('2fa-validation');
        }
      }else{
        $request->session()->flash('errorLogin','Login failed. Your mail address or password is wrong. Please check again');
      }
    }else{
      $request->session()->flash('errorLogin','You have tried many login attempt please wait 5 minutes');
    }
    if($lastPage=='admin'){
      return redirect('admin');
    }
    return back();
  }
}
