<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Hash;
use DB;
use Session;
use App\Models\User;
use App\Models\Role;
use App\Models\WaletAddress;
use Mail;
use App\Exceptions\InvalidConfirmationCodeException;
class RegisterController extends Controller
{
  public function createUserAddress() {
    $respond=createWalletAddress();
    $respond=json_decode($respond);
    if(isset($respond->status) and $respond->status==1){
      return $respond->result;
    }else{
      return false;
    }
  }
  private function checkPassword($pwd) {
    if (strlen($pwd) < 5) {
      return "Your password has to be more than 5 charecters ! </br>";
    }

    if ( ! preg_match("#[0-9]+#", $pwd)) {
      return "Your password has to contain numbers ! </br>";
    }

    if ( ! preg_match("#[a-zA-Z]+#", $pwd)) {
      return "Your password has to contain letters</br>";
    }
    return true;
  }
  public function register(Request $request) {
    $email         = $request['email'];
    $password      = $request['password'];
    $errorMessages='';

    if($email==''){
      $errorMessages.='Email address can\'t be empty</br>';
    }

    if($password==''){
      $errorMessages.='Password can\'t be empty</br>';
    }

    $passCheck=$this->checkPassword($password);
    if($passCheck!==true){
      $errorMessages.= $passCheck;
    }

    $validator = \Validator::make($request->all(), [
      'recaptchaResponse' => 'required|recaptcha',
    ]);

    if ($validator->fails()) {
      $errorMessages.= 'Captcha couldn\'t pass please check again</br>';
    }

    if($errorMessages!=''){
      return $errorMessages;
    }

    $user = DB::table('users')->where('email', $email)->value('email');
    if($user){
      return 'This user already exist.</br>';
    }
    $confirmationCode=str_random(30);
    $user = new User();
    $user->email = $email;
    $user->password=bcrypt($password);
    $user->confirmation_code= $confirmationCode;
    $user->save();

    $newRole = Role::where('name', '=', 'user')->first();
    $user->attachRole($newRole);
    $user->save();
    $wallet=$this->createUserAddress();
    if($wallet!==false){
      $walletAddress=new WaletAddress;
      $walletAddress->user_id=$user->id;
      $walletAddress->currency=$wallet->currency;
      $walletAddress->address=$wallet->address;
      $walletAddress->save();
    }

    Mail::send('email/verify', $confirmationCode, function($message) {
      $message->to($email, $email)->subject('Verify your email address');
    });

    return 'Your registration is complete. You can sign in now.</br>';
  }
  public function sendPasswordResetLink() {
    $email=$request->input('email');
    $user=User::where('email','=',$email)->first();
    if( ! is_null($user) ) { //send password reset link mail to the user
      $user->password_reset_key=randomCharecters();
      $user->save();
      return "Your password reset link has been sent to your mail address.";
      sendPasswordResetMail($user->email,$password);
    }else{//böyle bir mail yok
      return "Your password couldt send your mail address. Please check your mail address again.";
    }
  }
  public function confirmUser($confirmation_code) {
    if( ! $confirmation_code) {
        // throw new InvalidConfirmationCodeException;
        return 'Verifaction code is not correct';
    }

    $user = User::whereConfirmationCode($confirmation_code)->first();

    if ( ! $user)
    {
        // throw new InvalidConfirmationCodeException;
        return 'Verifaction code is not correct';
    }

    $user->status = 1;
    $user->confirmation_code = null;
    $user->save();
    return 'Your account succesfully verified.';
  }
  public function changePassword(Request $request) {
    $yeniSifre       = $request->input('newpassword1');
    $yeniSifreTekrar = $request->input('newpassword2');

    $mevcutSifre     = $request->input('password');
    $user            = Auth::user();

    $errorMessage='';

    if($yeniSifre != $yeniSifreTekrar){
      $errorMessage.=' Şifreleriniz uyuşmuyor. <br>';
    }

    if( Hash::check($mevcutSifre,$user->password) ){
      $user->password=Hash::make($yeniSifre);
      $user->save();
      Session::flash("message","Şifreniz başarılı bir şekilde değiştirilmiştir.");
    }else{
      $errorMessage.=' Eski şifreniz yanlış <br>';
    }

    $passCheck=$this->checkPassword($yeniSifre);
    if($passCheck!==true){
      $errorMessage.= $passCheck;
    }

    if($errorMessage!=''){
      Session::flash("error",$errorMessage);
    }
    return back();
  }
}
