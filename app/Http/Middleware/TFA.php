<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class TFA
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
     if( Auth::User() ) {
       if(Auth::user()->google2fa_secret and is_null(session('2fa:user:id'))){
         $request->session()->put('lastPage', $request->url());
         return redirect('2fa-validation');
       }
       return $next($request);
     }
     return redirect('/');
   }
}
