if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(searchElement, fromIndex) {
      var k;
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      var O = Object(this);
      var len = O.length >>> 0;

      if (len === 0) {
        return -1;
      }
      var n = +fromIndex || 0;
      if (Math.abs(n) === Infinity) {
        n = 0;
      }
      if (n >= len) {
        return -1;
      }
      k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);
      while (k < len) {
        if (k in O && O[k] === searchElement) {
          return k;
        }
        k++;
      }
      return -1;
    };
  }

  function Methods() {

      if(typeof window._methods !== 'undefined'){
          return window._methods;
      }

      var Methods = {
          processing_post: false,
          actions : {},
          protection : false,

          SHOW_MODAL : function(content, __callback, _class){
              var $container = $('#autoModals');
              _class = _class || '';
              if($container.length === 0){
                  $container = $('<div>').attr('id', 'autoModals');
                  $('body').append($container);
              }
              var $modal = $('<div>').addClass('modal in').attr('tabindex', -1).attr('role', 'dialog').attr('aria-labelledby', 'myModalLabel').append($('<div>').addClass('modal-dialog ' + _class).append($('<div>').addClass('modal-content'))).hide();
              $modal.find('.modal-content').append(content);
              $container.empty().append($modal);
              if(__callback){
                  $modal.on('hide.bs.modal', function(){
                      __callback.apply(this);
                  });
              }
              $($modal).modal();
          },
          GET_MODAL : function(){
              var $container = $('#autoModals');
              return $container.children('.modal');
          },
          CLOSE_MODAL : function(){
              if ($(document).find('.modal').length) {
                  $(document).find('.modal').modal('hide');
              }
              $('body').children('.modal-backdrop').stop(1,1).fadeOut(250, function(){
                  $('body').removeClass('modal-open');
                  $(this).remove();
              });
          },
          SHOW_TOASTR: function (message, type) {
              if (toastr) {
                  type = type || 'success';
                  toastr.options = {
                      "closeButton": true,
                      "debug": false,
                      "progressBar": false,
                      "positionClass": "toast-top-center",
                      "onclick": null,
                      "showDuration": "150",
                      "hideDuration": "500",
                      "timeOut": "5000",
                      "extendedTimeOut": "1000",
                      "showEasing": "swing",
                      "hideEasing": "linear",
                      "showMethod": "fadeIn",
                      "hideMethod": "fadeOut"
                  };
                  toastr[type](message);
              }
          },
          SHOW_REFILL_POPUP : function(){
              Methods.POST('/env/refill', {}, function(response){
                  if(response.modal){
                      Methods.SHOW_MODAL(response.modal);
                  }
              });
          }
      };

      window._methods = Methods;

      return Methods;
  }(jQuery);


var sgame = {};
sgame.manual_bets = [];
sgame.automatic_bets = [];
sgame.bets_edited = [];
sgame.total_price = 0;
sgame.init = function(){
    set_watchers = function() {
        $(document).ready(function(){
            sgame.fn.addTicket(2);
            sgame.fn.manual_bets_fillout();
            sgame.fn.eventHandlers();
            sgame.fn.checkFlash();

            $(document).find('li').each(function(){
                if ($(this).html()=='-') {
                    $(this).remove();
                }
            });
        });

        $(document).on('change','#form_draws_quantity_manual', function(){
            var dval = $(this).val();
            var dmax = $(this).data('max');
            if (dval > dmax ) {
                $(this).val(dmax);
            }else if (dval < 1) {
                $(this).val(1);
            }
            sgame.fn.update_total();
        }).on('change','#multiplier_value', function(){
            sgame.fn.update_total();
        }).on('change','#frm_multiplier',function(){
            sgame.fn.update_total();
        }).on('change','input[name="unique"]',function(){
            sgame.fn.switch_multibet_type();
            setTimeout(function(){
            sgame.fn.generateBets();
        },300);
        }).on('click','.receipts_r',function(){
            if (typeof($(this).attr('data-needed')) != "undefined" && $(this).attr('data-needed').length!=0) {

                $(this).parent().siblings().removeClass('active');
                $(this).parent().addClass('active');

            }
            return false;
        }).on('click', '.rapido__timeline', function(e){
            e.preventDefault();
            var _ = $(this);
            sgame.fn.timeline(_.data('drawid'));
        }).on('click', '.tabs-list > a', function(e){
            e.preventDefault();
            var _ = $(this);
            if( ! _.hasClass('active')){
                var $tab = $(_.attr('href'));
                if($tab.length){
                    $tab.closest('.tabs-container').children('div').stop(1,1).hide().removeClass('active');
                    $tab.fadeIn(200, function(){
                        $(this).addClass('active');
                    });
                    _.closest('.tabs-list').children('a.active').removeClass('active');
                    _.addClass('active');
                }
            }
        }).on('click','#paybtn', function(e){
            e.preventDefault();
            sgame.fn.pay('BTC');
        });

        $(window).resize(function() {

        });
    };
    set_watchers();
};
sgame.fn = {

    pay: function(currency) {
        var $btn = $('#paybtn');

        if(true === $btn.data('processing')){
            Methods().SHOW_TOASTR('Please, wait!','error');
            return false;
        }
        if (window.multibet_enabled === true) {
            var tickets = sgame.fn.collectTickets('automatic');
        }else {
            var tickets = sgame.fn.collectTickets('manual');
        }
        if (!tickets.length) {
            Methods().SHOW_TOASTR('Please, fill out your tickets','error');
            return false;
        }
        var total = parseFloat(sgame.total_price);
        if (!isNaN(total)) {
          if (total > myBtcBalance){
            Methods().SHOW_TOASTR('Insufficient funds Please, refill your balance','error');
          }
        }

        if (tickets.length && !isNaN(total)) {
          $.ajax({
              url: "buyTicket",
              type: 'POST',
              data:{
                tickets:tickets,
                currency:currency
              },
              success: function(data) {
                var data=jQuery.parseJSON(data);
                Methods().SHOW_TOASTR(data.message,data.type);
              },
              error: function (request, status, error) {
                Methods().SHOW_TOASTR('Some error occurred please try again.','error');
              }
          });
        }
    },
    collectTickets: function(bettype) {
        var tickets = [];
        if (bettype == 'manual') {
            var draws_qty = parseInt($('#form_draws_quantity_manual').val());
            if (!isNaN(draws_qty) && draws_qty > 0) {
                for (var j = 0; j < draws_qty; j++) {
                    sgame.manual_bets.forEach(function(el,i) {
                        if (sgame.fn.validateBet(el.combo)) {
                            tickets.push({'combo':el.combo});
                        }
                    });
                }
            }
        }else {
            sgame.automatic_bets.forEach(function(el,i){
                if (sgame.fn.validateBet(el.combo)) {
                  tickets.push({'combo':el.combo});
                }
            });
        }
        return tickets;
    },
    eventHandlers: function() {
        var max_drs = parseInt($('#sgame_multy_draws_qty').data('max'));
        if (isNaN(max_drs)) {
            max_drs = 10;
        }
        $(document).on('slide','#ticketsSlider',function(e) {
            $('#sgame_multy_tickets_qty').val(e.value);
            setTimeout(function(){
                sgame.fn.generateBets();
            },100);
        }).on('slide','#drawsSlider',function(e) {
            $('#sgame_multy_draws_qty').val(e.value);
            setTimeout(function(){
                sgame.fn.generateBets();
            },100);
        }).on('change','#sgame_multy_tickets_qty',function() {
            var tval = parseInt($(this).val());
            var tmax = parseInt($('#ticketsSlider').data('max'));
            if (tval > tmax) {
                $(this).val(tmax);
                $('#ticketsSlider').slider('setValue',tmax);
            }else if (tval < 1) {
                $(this).val(1);
                $('#ticketsSlider').slider('setValue',1);
            }else {
                $('#ticketsSlider').slider('setValue',tval);
            }
            setTimeout(function(){
                sgame.fn.generateBets();
            },100);
        }).on('change','#sgame_multy_draws_qty',function(){
            var dval = parseInt($(this).val());
            var dmax = parseInt($('#drawsSlider').data('max'));
            if (dval > dmax) {
                $(this).val(dmax);
                $('#drawsSlider').slider('setValue',dmax);
            }else if (dval < 1) {
                $(this).val(1);
                $('#drawsSlider').slider('setValue',1);
            }else {
                $('#drawsSlider').slider('setValue',dval);
            }
            setTimeout(function(){
                sgame.fn.generateBets();

            },100);
        });
    },

    clearBets:function (type) {
        if (type == 'manual') {
            sgame.manual_bets = [];
        }else {
            sgame.automatic_bets = [];
        }
    },
    push_to_manual: function(uiId,combo) {
        sgame.manual_bets.forEach(function(el,i) {
            if (el.uiId == uiId) {
                el.combo = combo;


                var total_ticket_price = ticket_price;
                total_ticket_price = ((total_ticket_price * 10000)/10000).toFixed(4);
                $('#rapid_tkt_'+uiId).find('.self_total').html(total_ticket_price);
            }
        });
        sgame.fn.update_total();
    },
    clear_manual_bet: function(uiId) {
        sgame.manual_bets.forEach(function(el,i) {
            if (el.uiId == uiId) {
                el.combo = [];
                var total_ticket_price = ticket_price;
                total_ticket_price = ((total_ticket_price * 10000)/10000).toFixed(4);
            }
        });
        sgame.fn.update_total();
    },
    removeBet:function(bettype,uiId) {
        if (bettype == 'manual') {
            sgame.manual_bets.forEach(function(el,i) {
                if (el.uiId.toString() == uiId) {
                    sgame.manual_bets.splice(i,1);
                    $(document).find('#sgame_tkt_'+uiId).remove();
                }
            });
        }
    },
    check_if_bet_ready: function(uiId) {
        if ($(document).find('#sgame_tkt_'+uiId).length) {
            var _ = $(document).find('#sgame_tkt_'+uiId);
            var combo = [];
            var selectors_to_check = ['.first-49', '.last-26'];
            selectors_to_check.forEach(function(el,i) {
                _.find(el+' li').each(function(){
                    if ($(this).hasClass('active')) {
                        var number = parseInt($(this).html());
                        if (!isNaN(number)) {
                            combo.push(number);
                        }
                    }
                });
            });
            if (sgame.fn.validateBet(combo) == true) {
                sgame.fn.push_to_manual(uiId,combo);
            }else {
                sgame.fn.clear_manual_bet(uiId);
            }
        }
    },
    manual_bets_fillout: function() {
        $(document).on('click','.list_numbers li',function(){
            var _ = $(this);
            var full = 0;
            if (_.hasClass('active')) {
                _.removeClass('active');
                _.parent().removeClass('ready');
            }else {
                _.parent().find('li').each(function(){
                    if ($(this).hasClass('active')) {
                        full++;
                    }
                });
                if (full+1 <= _.parent().data('max')) {
                    _.addClass('active');
                }
                if (full+1 == _.parent().data('max')) {
                    _.parent().addClass('ready');
                }
            }
            sgame.fn.check_if_bet_ready(_.parent().data('id'));
        });
    },
    refreshForDraw : function(draw_id) {

        sgame.automatic_bets.forEach(function(i,ind){
            if (typeof (i.draw_id)!="undefined" && i.draw_id==(draw_id)) {
                sgame.automatic_bets.splice(ind,1);
            }
        });
        sgame.automatic_bets.forEach(function(i,ind){
            if (typeof (i.draw_id)!="undefined" && i.draw_id==(draw_id)) {
                sgame.automatic_bets.splice(ind,1);
            }
        });
        var ticketQuantity = parseInt($('#sgame_multy_tickets_qty').val());
        var bts = '';
        for (var j = 1; j<=ticketQuantity; j++) {
            var bet = sgame.fn.generateBet();
            var uiId = sgame.fn.getRandomString();
            if (typeof(draw_id) != "undefined") {
                var combo = {'uiId':uiId,'draw_id':(draw_id),'combo':bet};
                sgame.automatic_bets.push(combo);
                bts += sgame.fn.renderBet(combo);
            }
        }
        $(document).find('.cmb_draw[data-id="'+draw_id+'"]').find('.combos__bet').remove();
        $(document).find('.cmb_draw[data-id="'+draw_id+'"]').append(bts);
        sgame.fn.animateAppearance();
    },
    renderDrawsCombo : function(combos,draw_id) {
        var draw = '<div class="cmb_draw" data-id="'+draw_id+'">';
            draw += '<a href="#" onclick="sgame.fn.refreshForDraw('+draw_id+'); return false;" class="cmb_draw__refresh"><i class="tickets-icon tickets-icon-auto"></i></a>';
            draw += '<div class="cmb_draw__title">Draw â„–<span id="unique_combos_number_'+draw_id+'">'+(draw_id-56)+'</span></div>';
            draw += combos;
            draw += '</div>';
        return draw;
    },
    animateAppearance:function(){
        $('.combos__bet').each(function(i){
          var elm = $(this);
          setTimeout(function(){ elm.removeClass('bet_appearance'); },30*i);
        });
    },
    switch_multibet_type: function() {
        if (!$('input[name="unique"]').is(':checked')) {
            $('.equal_combos').hide(300);
            $('.unique_combos').show(300);
        }else {
            $('.equal_combos').show(300);
            $('.unique_combos').hide(300);
        }
    },
    generateBets: function(full) {
        if (typeof(full)=="undefined") {
            full = false;
        }else if (typeof(full) != "undefined" && full == true) {
            sgame.bets_edited = [];
        }
        sgame.fn.clearBets('multibets');
        var UniqueForEachDraw = $('input[name="unique"]').is(':checked');
        var ticketQuantity = parseInt($('#sgame_multy_tickets_qty').val());
        var drawsQuantity = parseInt($('#sgame_multy_draws_qty').val());
        if (!isNaN(drawsQuantity) && !isNaN(ticketQuantity)) {
            if (UniqueForEachDraw == false) {
                var drel = '';
                for(var i = 1; i<=drawsQuantity; i++) {
                    var bts = '';
                    for (var j = 1; j<=ticketQuantity; j++) {

                        var bet = sgame.fn.generateBet();

                        if (typeof(futureDraws[i-1]) != "undefined") {
                            var uiId = sgame.fn.getRandomString();
                            var combitanion = {'uiId': uiId, 'draw_id':futureDraws[i-1],'combo':bet};
                            sgame.automatic_bets.push(combitanion);
                            bts += sgame.fn.renderBet(combitanion);
                        }
                    }
                    if (bts != '') {
                        sgame.fn.switch_multibet_type();
                        drel += sgame.fn.renderDrawsCombo(bts,futureDraws[i-1]);
                    }
                }
                if (drel != '') {
                    $('.unique_combos').find('.cmb__list').html(drel);
                    sgame.fn.animateAppearance();
                }
            }else {
                var bts = '';
                var combo_list = [];
                var max_draw;
                for (var j = 1; j <= ticketQuantity; j++ ) {
                    var bet = sgame.fn.generateBet();
                    var uiId = sgame.fn.getRandomString();
                    var combination = {'uiId': uiId, 'draw_id':0,'combo':bet}
                    combo_list.push(combination);
                    bts += sgame.fn.renderBet(combination);
                }
                for (var i = 1; i <= drawsQuantity; i++) {
                    for(var k = 0; k < combo_list.length; k++) {
                        if (sgame.fn.validateBet(combo_list[k].combo)) {
                            var tmp_combo = {};
                            tmp_combo['combo'] = combo_list[k].combo;
                            tmp_combo['draw_id'] = futureDraws[i-1];
                            tmp_combo['uiId'] = sgame.fn.getRandomString();
                            sgame.automatic_bets.push(tmp_combo);
                        }
                    }
                    max_draw = futureDraws[i-1];
                }

                if (bts!='') {
                    sgame.fn.switch_multibet_type();
                    $('.equal_combos').find('.cmb__list').html(bts);

                    if (typeof(max_draw)!= "undefined") {
                        var numbers = '';
                        var min_draw = Math.min.apply(null, futureDraws)
                        if (max_draw == min_draw) {
                            numbers = max_draw - 56;
                        }else{
                            numbers = (min_draw - 56)+' - '+(max_draw - 56);
                        }
                        $('#equal_combos_number').html(numbers);
                    }
                    sgame.fn.animateAppearance();
                }
            }
        }
        sgame.fn.update_total();
    },
    multibets_disable : function(){
        if(window.multibet_enabled){
            window.multibet_enabled = false;
            $('#rapido_multy_draws_qty').val(1).trigger('change');
            sgame.fn.update_total();
            $('.multibet_form').addClass('hidden');
            $('.manual_form').removeClass('hidden');
            $('.common_numbers').toggle('medium');
        }
    },
    multibets_enable : function(){
        if(false === window.multibet_enabled){
            window.multibet_enabled = true;
            sgame.fn.generateBets();
            sgame.fn.update_total();
            $('.multibet_form').removeClass('hidden');
            $('.manual_form').addClass('hidden');
            $('.common_numbers').toggle('medium');
        }
    },
    autoFillBet: function(uiId) {
        sgame.fn.clearTicket(uiId);
        var combo = sgame.fn.generateBet();
        if (sgame.fn.validateBet(combo)) {
            combo.forEach(function(el,i) {
                if (i<=4) {
                    for (var j = 1; j<=el; j++) {
                            $('#sgame_tkt_'+uiId).find('.first-49 li').each(function(k){
                                if (k+1 == el) {
                                    $(this).addClass('active');
                                }
                            });
                    }
                }else {
                    $('#sgame_tkt_'+uiId).find('.last-26 li').each(function(k){
                        if (k+1 == el) {
                            $(this).addClass('active');
                        }
                    });
                }
            });
            $('#sgame_tkt_'+uiId).find('.last-26');
            $('#sgame_tkt_'+uiId).find('.first-49').addClass('ready');
            sgame.fn.check_if_bet_ready(uiId);
        }
    },
    autoFill: function(uiId){
        for(var i = 0; i<=5; i++) {
            setTimeout(function() {
                sgame.fn.autoFillBet(uiId);
            },50*i);
        }
    },
    clearTicket: function(uiId) {
        $('#sgame_tkt_'+uiId).find('li').removeClass('active');
        $('#sgame_tkt_'+uiId).find('.last-26').removeClass('ready');
        $('#sgame_tkt_'+uiId).find('.first-49').removeClass('ready');
        sgame.fn.check_if_bet_ready(uiId);
    },
    generateMax : function(max) {
        return Math.ceil(Math.random()*max);
    },
    generateBet:function() {
        var firstBlock = [];
        var secondBlock = [];
        while(firstBlock.length < 5){
            var randomnumber = sgame.fn.generateMax(49);
            if(firstBlock.indexOf(randomnumber) == -1 && firstBlock.length != 5) {
                firstBlock.push(randomnumber);
            }
        }
        while(secondBlock.length < 1){
            var randomnumber = sgame.fn.generateMax(26);
            if(secondBlock.indexOf(randomnumber) == -1 && secondBlock.length == 0 ) {
                secondBlock.push(randomnumber);
            }
        }
        firstBlock.push(secondBlock[0]);
        return firstBlock;
    },
    update_total: function() {
        if (window.multibet_enabled == false) {
            var count_filledout = 0;
            var count_draws = parseInt($('#form_draws_quantity_manual').val());

            if (isNaN(count_draws) || count_draws == 0) {
                count_draws = 1;
            }
            var total_price = 0;
            var tickets_sum = 0;
            sgame.manual_bets.forEach(function(el,i) {
                if (sgame.fn.validateBet(el.combo)) {
                    tickets_sum += ticket_price;
                    count_filledout++;


                    var total_ticket_price = ticket_price;
                    total_ticket_price = ((total_ticket_price * 10000)/10000).toFixed(4);
                }
            });
            $('#form_tickets_count').html(count_filledout);
            total_price =  tickets_sum * count_draws;
            total_price = ((total_price * 10000) / 10000).toFixed(5);
            sgame.total_price = total_price;
            $('#total_summ').html(total_price+' <span>BTC</span>');
            sgame.fn.discount(count_filledout *count_draws,total_price);
        }else {
            var ticketQuantity = parseInt($('#sgame_multy_tickets_qty').val());
            var drawsQuantity = parseInt($('#sgame_multy_draws_qty').val());

            $('#form_tickets_count').html(sgame.automatic_bets.length);
            $('#form_draws_quantity_multibet').html(drawsQuantity);
            var total_price = 0;
            var tickets_sum = 0;
            sgame.automatic_bets.forEach(function(el,i){
                if (sgame.fn.validateBet(el.combo)) {
                    tickets_sum += ticket_price;
                }
            });
            total_price =  tickets_sum ;
            total_price = ((total_price * 10000) / 10000).toFixed(5);
            sgame.total_price = total_price;
            $('#total_summ').html(total_price+' <span>BTC</span>');
            sgame.fn.discount(sgame.automatic_bets.length,total_price);
        }
    },
    discount:function(tickets,total_price) {
        if (tickets < 14 ) {
            $('.discount_label').addClass('hidden');
            $('.discount_block').addClass('hidden');
            $('.star-link__danger').addClass('hidden');
            $('.star-link__success').removeClass('hidden');
        }else if (tickets >=14 && tickets < 28) {

            $('.discount_label').removeClass('hidden').addClass('discount_label__success').removeClass('discount_label__danger').html('-5%');
            $('.discount_block').removeClass('hidden').find('.discount_small').addClass('discount_small_green').removeClass('discount_small_red').html('5%');
            var old = total_price;
            var ntotal = (total_price - (total_price/20)).toFixed(4);
            $('.discount_block').find('.pull-right').html(old+ 'BTC');
            $('#total_summ').html( ntotal +' <span>BTC</span>');
            sgame.total_price = ntotal;
            $('.star-link__danger').addClass('hidden');
            $('.star-link__success').removeClass('hidden');

        }else if (tickets >= 28 && tickets < 365 ) {

            $('.discount_label').removeClass('hidden').addClass('discount_label__success').removeClass('discount_label__danger').html('-15%');
            $('.discount_block').removeClass('hidden').find('.discount_small').addClass('discount_small_green').removeClass('discount_small_red').html('15%');
            var old = total_price;
            var ntotal = (total_price - (total_price*0.15)).toFixed(4);
            $('.discount_block').find('.pull-right').html(old+ 'BTC');
            $('#total_summ').html( ntotal +' <span>BTC</span>');
            sgame.total_price = ntotal;
            $('.star-link__danger').addClass('hidden');
            $('.star-link__success').removeClass('hidden');
        }else {

            $('.discount_label').removeClass('hidden').removeClass('discount_label__success').addClass('discount_label__danger').html('-30%');
            $('.discount_block').removeClass('hidden').find('.discount_small').removeClass('discount_small_green').addClass('discount_small_red').html('30%');
            var old = total_price;
            var ntotal = (total_price - (total_price*0.3)).toFixed(4);
            $('.discount_block').find('.pull-right').html(old+ 'BTC');
            $('#total_summ').html( ntotal +' <span>BTC</span>');
            sgame.total_price = ntotal;
            $('.star-link__danger').removeClass('hidden');
            $('.star-link__success').addClass('hidden');
        }
    },
    checkFlash: function() {
        if ($('#manual').attr('data-flash-combo')) {
            var combo = $('#manual').data('flash-combo').split(',');
            if (typeof(combo) == "object") {
                sgame.fn.pickCombo(combo);
            }
        }
    },
    fillPicked:function(combo,uiId) {
        sgame.fn.clearTicket(uiId);
        if (sgame.fn.validateBet(combo)) {
            combo.forEach(function(el,i) {
                if (i<=4) {
                    for (var j = 1; j<=el; j++) {
                            $('#sgame_tkt_'+uiId).find('.first-49 li').each(function(k){
                                if (k+1 == el) {
                                    $(this).addClass('active');
                                }
                            });
                    }
                }else {
                    $('#sgame_tkt_'+uiId).find('.last-26 li').each(function(k){
                        if (k+1 == el) {
                            $(this).addClass('active');
                        }
                    });
                }
            });
            $('#sgame_tkt_'+uiId).find('.last-26');
            $('#sgame_tkt_'+uiId).find('.first-49').addClass('ready');
            sgame.fn.check_if_bet_ready(uiId);
        }
    },
    pickCombo:function(combo) {
        if (sgame.manual_bets.length) {
            var done = false;
            sgame.manual_bets.forEach(function(el,i) {
                if (el.combo.length == 0 && done == false) {
                    sgame.fn.fillPicked(combo,el.uiId);
                    done = true;
                    return true;
                }
            });
            if (!done) {
                sgame.fn.addTicket(1);
                sgame.fn.fillPicked(combo,sgame.manual_bets[sgame.manual_bets.length-1].uiId);
            }
        }else {
            sgame.fn.addTicket(1);
            sgame.fn.fillPicked(combo,sgame.manual_bets[sgame.manual_bets.length-1].uiId);
        }
    },
    addTicket: function(count) {
        if ($(document).find('#manual .tickets').length) {
            if (count === "undefined") {
                var count = 2;
            }
            for (var i = 1; i<=count;i++) {
                $(document).find('#manual .tickets').append(sgame.fn.renderTicket());
            }

        }
    },
    clear_edited_ticket:function(){
        $('#original_combo').val('');
        $('#original_selector').val('');
        $(document).find('#modal_edit_ticket .num-list__item').removeClass('active');
        $('#modal_edit_ticket .first-49').removeClass('ready-number-set').attr('data-ready',false);
        $('#modal_edit_ticket .num-list_yellow').removeClass('ready-number-set').attr('data-ready',false);
    },
    edit_combo: function(bet,selector) {
        sgame.fn.clear_edited_ticket();
        $(document).find('#original_combo').val(bet.toString());
        $(document).find('#original_selector').val(selector.toString());
        if (typeof (bet) !="undefined") {
          $('#modal_edit_ticket .first-49 .num-list__item').each(function(i){
            var el = $(this);
            bet.forEach(function(val,ind){
              if (ind != 8) {
                if ((i+1) == val) {
                  el.addClass('active');
                }
              }
            });
          });
          $('#modal_edit_ticket .num-list_yellow .num-list__item').each(function(i){
            if ((i+1) == bet[9]) {
              $(this).addClass('active');
            }
          });
          $('#modal_edit_ticket .first-49').addClass('ready-number-set').attr('data-ready',true);
          $('#modal_edit_ticket .num-list_yellow').addClass('ready-number-set').attr('data-ready',true);
        }
    },
    renderBet:function(combo) {
        if (sgame.fn.validateBet(combo.combo)) {
            var template = '<div class="combos__bet bet_appearance" id="bet_'+combo.uiId+'">';
            combo.combo.forEach(function(i,ind) {
              if (ind < 5) {
                template += '<span class="ball white__ball">'+i+'</span>';
              }else {
                template += '<span class="ball yellow__ball">'+i+'</span>';
              }
            });

            template += '</div>';
            return template;
          }
          return '';
    },
    renderTicket: function() {
        var uiId = sgame.fn.getRandomString();
        var tkt = '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-24 padding-15" id="sgame_tkt_'+uiId+'">';
        tkt += '<div class="tickets__item">';
        tkt += '<div class="tickets__head">';
        tkt += '<button class="btn btn-outline-dark pull-left" onclick="sgame.fn.autoFill(\''+uiId+'\'); return false; "><i class="tickets-icon tickets-icon-auto"></i> '+ticketControls.AUTO+'</button>';
        tkt += '<div class="pull-right">';
        tkt += '<button class="btn btn-outline-dark" onclick="sgame.fn.clearTicket(\''+uiId+'\'); return false;">'+ticketControls.CLEAR+'</button>';
        tkt += '<button class="btn btn-outline-dark" onclick="sgame.fn.removeBet(\'manual\',\''+uiId+'\'); sgame.fn.update_total(); return false;"><i class="ticket-icon tickets-icon-close"></i></button>'
        tkt += '</div></div>';
        tkt += '<div class="tickets__body">';
        tkt += '<p>'+ticketControls.SELECT5+'</p>';
        tkt += '<ul class="list_numbers first-49" data-max="5" data-id="'+uiId+'">';
        for (var i = 1; i<=49; i++) {
            tkt += '<li>'+i+'</li>';
        }
        tkt += '</ul>';
        tkt += '<p>'+ticketControls.SELECT1+'</p>';
        tkt += '<ul class="list_numbers last-26" data-max="1" data-id="'+uiId+'">';
        for (var i = 1; i<=26; i++) {
            tkt += '<li>'+i+'</li>';
        }
        tkt += '</ul>';
        tkt += '</div></div></div>';
        sgame.manual_bets.push({'uiId':uiId,'combo':[]});
        return tkt;

    },
    showPrizeTable: function(uiId) {
        sgame.manual_bets.forEach(function(el,i) {
            if (el.uiId == uiId) {
                var id = 1;
                if (sgame.fn.validateBet(el.combo)) {
                    id = sgame.settings.combo_multiplier[el.combo.length];
                }
                sgame.fn.renderPrizeTable(id);
            }
        });
    },
    validateBet:function(combo) {
        if (Array.isArray(combo) && combo.length==6) {
          var ret = true;
          combo.forEach(function(i,ind) {
            var val = parseInt(i);
            if (ind < 5) {
              if (!(!isNaN(val) && val >= 1 && val <= 49)) {
                ret = false;
              }
            }else {
              if (!(!isNaN(val) && val >= 1 && val <= 26)) {
                ret = false;
              }
            }
          });
          return ret;
        }
        return false;
    },
    getRandomString: function() {
        var random_number = Math.random(1,100000).toString();
        return sgame.fn.djb2Code(random_number);
    },
    djb2Code: function(str){
        var hash = 5381;
        for (i = 0; i < str.length; i++) {
            char = str.charCodeAt(i);
            hash = ((hash << 5) + hash) + char; /* hash * 33 + c */
        }
        return hash;
    }
};
sgame.init();
