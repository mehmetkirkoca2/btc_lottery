<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transactions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id')->nullable();
			$table->string('currency', 11)->nullable();
			$table->float('amount', 16, 8)->nullable();
			$table->string('address')->nullable();
			$table->string('tx_id')->nullable()->unique('tx_idUnique')->comment('transaction id');
			$table->boolean('status')->nullable()->default(0)->comment('0->not confirmed
1->confirmed 2->Approved for transfer');
			$table->boolean('type')->nullable()->default(1)->comment('1->deposit
2->withdrawal');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transactions');
	}

}
