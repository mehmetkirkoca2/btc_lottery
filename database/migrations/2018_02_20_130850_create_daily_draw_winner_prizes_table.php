<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDailyDrawWinnerPrizesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('daily_draw_winner_prizes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->boolean('numbers')->nullable();
			$table->boolean('plus_one')->nullable();
			$table->float('price_percent', 16, 3)->nullable();
			$table->string('probability')->nullable();
			$table->timestamp('created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('daily_draw_winner_prizes');
	}

}
