<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFrequencyOfNumbersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('frequency_of_numbers', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->smallInteger('number')->nullable();
			$table->smallInteger('frequency')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('frequency_of_numbers');
	}

}
