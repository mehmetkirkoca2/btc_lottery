<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlockHashTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('block_hash', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('hash')->nullable()->unique('hashUnique');
			$table->integer('time')->nullable();
			$table->integer('block_index')->nullable();
			$table->integer('height')->nullable();
			$table->boolean('draw_type')->nullable()->default(0)->comment('0->not drawed yet
1->is not correct number formats
2->it is a successfull draw');
			$table->timestamp('created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('block_hash');
	}

}
