<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDailyDrawWinnersCopyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('daily_draw_winners_copy', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id')->nullable();
			$table->smallInteger('block_hash_id')->nullable();
			$table->integer('daily_draw_id')->nullable();
			$table->integer('daily_draw_ticket_id')->nullable()->unique('ticketIdUnique');
			$table->integer('prize_id')->nullable();
			$table->float('prize_amount', 16, 8)->nullable()->default(0.00000000)->comment('BTC coin');
			$table->text('matched_numbers', 65535)->nullable();
			$table->boolean('status')->nullable()->default(0)->comment('0-> not calculated yet 
1->calculated 
2->gived to the user balance');
			$table->timestamp('created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('daily_draw_winners_copy');
	}

}
