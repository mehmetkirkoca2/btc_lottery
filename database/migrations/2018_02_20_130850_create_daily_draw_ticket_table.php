<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDailyDrawTicketTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('daily_draw_ticket', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id')->nullable();
			$table->integer('draw_id')->nullable();
			$table->smallInteger('n1')->nullable();
			$table->smallInteger('n2')->nullable();
			$table->smallInteger('n3')->nullable();
			$table->smallInteger('n4')->nullable();
			$table->smallInteger('n5')->nullable();
			$table->smallInteger('n6')->nullable();
			$table->integer('prize_id')->nullable()->default(-1)->comment('-1 ->not checked
 0 ->checked
orhers price id');
			$table->timestamp('created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('daily_draw_ticket');
	}

}
