<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDailyDrawPoolTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('daily_draw_pool', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('daily_draw_id')->nullable()->unique('drawIdUnique');
			$table->float('amount', 16, 8)->nullable();
			$table->boolean('status')->nullable()->default(1)->comment('1->it is on pool, 0->it is already gave to winner');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('daily_draw_pool');
	}

}
