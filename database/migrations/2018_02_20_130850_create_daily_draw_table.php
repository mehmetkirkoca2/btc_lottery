<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDailyDrawTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('daily_draw', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('block_hash_id')->nullable()->unique('hashUnique');
			$table->smallInteger('n1')->nullable();
			$table->smallInteger('n2')->nullable();
			$table->smallInteger('n3')->nullable();
			$table->smallInteger('n4')->nullable();
			$table->smallInteger('n5')->nullable();
			$table->smallInteger('n6')->nullable();
			$table->float('total_won', 16, 8)->nullable();
			$table->float('jackpot_prize', 16, 8)->nullable();
			$table->timestamp('created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('daily_draw');
	}

}
