<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('email')->unique('emailUnique');
			$table->string('name_surname')->nullable();
			$table->string('password');
			$table->string('confirmation_code')->nullable();
			$table->string('remember_token')->nullable();
			$table->string('password_reset_key')->nullable();
			$table->timestamps();
			$table->boolean('status')->nullable()->default(0)->comment('0->Unconfirmed
1->Confirmed');
			$table->string('google2fa_secret')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
